/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeightTest {

    @Test
    @DisplayName("Weight()")
    void testEmptyConstructor() {

        Weight weight = new Weight();

        assertNotNull(weight);
        assertEquals(0.0, weight.getValue());
        assertEquals("kg", weight.getUnit());
    }

    @Test
    @DisplayName("Weight(3.5)")
    void testConstructorWithValue() {
        Weight weight = new Weight(3.5);

        assertNotNull(weight);
        assertEquals(3.5, weight.getValue());
        assertEquals("kg", weight.getUnit());
    }

    @Test
    @DisplayName("Weight(3.5, g)")
    void testConstructorWithAllValues() {
        Weight weight = new Weight(3.5, "G");

        assertNotNull(weight);
        assertEquals(3.5, weight.getValue());
        assertEquals("g", weight.getUnit());
    }

    @Test
    @DisplayName("Weight(3.2, kg) + Weight(1.2, kg)")
    void addTwoWeights() {
        Weight weight1 = new Weight(3.2);
        Weight weight2 = new Weight(1.2, "Kg");

        assertEquals(4.4, weight1.add(weight2).getValue());
    }

    @Test
    @DisplayName("Weight(3.2, g) + Weight(1.2, g)")
    void addTwoWeights2() {
        Weight weight1 = new Weight(3.2);
        Weight weight2 = new Weight(1.2, "g");

        assertEquals(4.4, weight1.add(weight2).getValue());
    }

}
