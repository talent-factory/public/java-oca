/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LengthTest {

    @Test
    @DisplayName("Length()")
    void testEmptyConstructor() {
        Length a = new Length();

        assertEquals(0.0, a.getLength());
        assertEquals(LengthUnit.METER, a.getUnit());
    }

    @Test
    @DisplayName("Length(5.0)")
    void testSimpleValue() {
        Length a = new Length(5.0);

        assertEquals(5.0, a.getLength());
        assertEquals(LengthUnit.METER, a.getUnit());
    }

    @Test
    @DisplayName("Length(5.0, LengthUnit.METER)")
    void testCompleteConstructor() {
        Length a = new Length(5.0, LengthUnit.METER);

        assertEquals(5.0, a.getLength());
        assertEquals(LengthUnit.METER, a.getUnit());
    }

    @Test
    @DisplayName("Length(6.5, LengthUnit.YARD)")
    void testAnotherUnit() {
        Length a = new Length(6.5, LengthUnit.YARD);

        assertEquals(6.5, a.getLength());
        assertEquals(LengthUnit.YARD, a.getUnit());
    }
}
