/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyTest {

    @Test
    @DisplayName("Money()")
    void testEmptyConstructor() {
        Money a = new Money();

        assertEquals(0.0, a.getAmount());
        assertEquals(Currency.CHF, a.getCurrency());
    }

    @Test
    @DisplayName("Money(5.0)")
    void testSimpleValue() {
        Money a = new Money(5.0);

        assertEquals(5.0, a.getAmount());
        assertEquals(Currency.CHF, a.getCurrency());
    }

    @Test
    @DisplayName("Money(5.0, Currency.EUR)")
    void testCompleteConstructor() {
        Money a = new Money(5.0, Currency.EUR);

        assertEquals(5.0, a.getAmount());
        assertEquals(Currency.EUR, a.getCurrency());
    }

    @Test
    @DisplayName("5.00 CHF + 6.00 CHF")
    void testAdd() {
        Money a = new Money(5.00);
        Money b = new Money(6.00);
        Money c = a.add(b);

        assertEquals(11.0, c.getAmount());
        assertEquals(Currency.CHF, c.getCurrency());
    }

    @Test
    @DisplayName("5.00 CHF + 6.00 EUR")
    void testAddDifferentCurrencies() {
        Money a = new Money(5.00);
        Money b = new Money(6.00, Currency.EUR);
        Money c = a.add(b);

        assertEquals(11.6, a.add(b).getAmount(), 0.01);
        assertEquals(Currency.CHF, c.getCurrency());
    }

    @Test
    @DisplayName("5.00 EUR + 6.00 EUR")
    void testAddEuros() {
        Money b = new Money(5.00, Currency.EUR);
        Money a = new Money(6.00, Currency.EUR);
        Money c = a.add(b);

        assertEquals(11.0, c.getAmount(), 0.01);
        assertEquals(Currency.EUR, c.getCurrency());
    }

    @Test
    @DisplayName("round(5.123)")
    void testRound() {
        Money b = new Money(5.123);
        assertEquals(5.12, b.getAmount());
    }

    @Test
    @DisplayName("round(5.125)")
    void testRound2() {
        Money b = new Money(5.125);
        assertEquals(5.13, b.getAmount());
    }

    @Test
    @DisplayName("5.00 CHF - 2.00 EUR")
    void testSubtract() {
        Money a = new Money(5.00);
        Money b = new Money(2.00, Currency.EUR);
        Money c = a.sub(b);

        assertEquals(2.8, c.getAmount(), 0.01);
        assertEquals(Currency.CHF, c.getCurrency());
    }

    @Test
    @DisplayName("Money(5.0, null)")
    void testNullValues() {
        Exception exception = assertThrows(NullPointerException.class,
                () -> new Money(5.00, null)
        );
        assertEquals("Currency must be defined.", exception.getMessage());
    }

    @Test
    @DisplayName("Money(-5.0)")
    void testNegativeAmount() {
        MoneyException exception = assertThrows(MoneyException.class,
                () -> new Money(-5)
        );

        assertEquals("Zero amount values not allowed.", exception.getMessage());
        assertEquals(0, exception.getMoney().getAmount());
        assertNull(exception.getMoney().getCurrency());
    }

}
