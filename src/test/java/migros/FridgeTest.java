package migros;

import _26class.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FridgeTest {

    // Deklarieren des zu testenden Objektes
    private Fridge fridge;
    private final Beer bud = new Beer(Brand.BUDWEISER);

    @BeforeEach
    void setUp() {
        fridge = new Fridge();
    }

    @Test
    void contains() {
        Beer beer = new Beer(Brand.BUDWEISER, new Money(2.5));
        fridge.add(beer);

        assertTrue(fridge.contains(bud));
    }

    @Test
    @DisplayName("1x3dl Budweiser, 1x3dl Feldschlösschen)")
    void count() {
        Beer beer1 = new Beer(Brand.BUDWEISER, new Money(2.5));
        beer1.setAmount(3);

        fridge.add(beer1);
        assertEquals(1, fridge.count(beer1));

        Beer beer2 = new Beer(Brand.FELDSCHLOESSCHEN, new Money());
        beer2.setAmount(3);
        fridge.add(beer2);
        assertEquals(1, fridge.count(beer2));
    }

    @Test
    @DisplayName("1x3dl Bier (Budweiser)")
    void count1() {
        Beer beer = new Beer(Brand.BUDWEISER, new Money());
        beer.setAmount(3);

        fridge.add(beer);
        assertEquals(1, fridge.count(beer));
    }

    @Test
    @DisplayName("2x3dl Budwiser")
    void count2() {
        Beer beer1 = new Beer(Brand.BUDWEISER, new Money());
        beer1.setAmount(3);

        Beer beer2 = new Beer(Brand.BUDWEISER, new Money());
        beer2.setAmount(3);

        fridge.add(beer1);
        fridge.add(beer2);

        assertEquals(2, fridge.count(beer1));
    }

    @Test
    @DisplayName("1x3dl Budweiser, 1x5dl Budweiser)")
    void count3() {
        Beer beer1 = new Beer(Brand.BUDWEISER, new Money(2.5));
        beer1.setAmount(3);

        fridge.add(beer1);
        assertEquals(1, fridge.count(beer1));

        Beer beer2 = new Beer(Brand.BUDWEISER, new Money());
        beer2.setAmount(5);
        fridge.add(beer2);
        assertEquals(1, fridge.count(beer2));
    }

    @Test
    @DisplayName("1x3dl Feldschlösschen/Hell, 1x3dl Feldschlösschen/Hell")
    void count4() {
        Beer beer1 = new Beer(Brand.FELDSCHLOESSCHEN, new Money(2.5));
        beer1.setAmount(3);

        fridge.add(beer1);
        assertEquals(1, fridge.count(beer1));

        Beer beer2 = new Beer(Brand.BUDWEISER, new Money());
        beer2.setAmount(5);
        fridge.add(beer2);
        assertEquals(1, fridge.count(beer2));
    }
}
