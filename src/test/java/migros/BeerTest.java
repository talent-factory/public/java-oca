package migros;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BeerTest {

    private Beer budweiser;

    @BeforeEach
    void setUp() {
        budweiser = new Beer(Brand.BUDWEISER);
        budweiser.setAmount(3);
    }

    @Test
    @DisplayName("2x3dl Budweiser")
    void equals1() {
        Beer beer = new Beer(Brand.BUDWEISER);
        beer.setAmount(3);

        assertEquals(budweiser, beer);
    }

    @Test
    @DisplayName("1x3dl Budweiser, 1x5dl Budweiser")
    void equals2() {
        Beer beer = new Beer(Brand.BUDWEISER);
        beer.setAmount(5);

        assertNotEquals(budweiser, beer);
    }
}
