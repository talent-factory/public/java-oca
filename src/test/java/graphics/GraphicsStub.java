package graphics;

import lombok.Getter;

@Getter
public class GraphicsStub extends GraphicsAdapter {

    // Hilfsobjekte, welche zur Überprüfung der Schnittstellen
    // benötigt werden.
    @Getter
    private static int numberOfCalls;
    protected int x, y;
    protected int width, height;

    @Override
    public void drawRect(int x, int y, int width, int height) {
        numberOfCalls++;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}
