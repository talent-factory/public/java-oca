package graphics;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RectangleTest {

    private static final int HEIGHT = 123;
    private static final int WIDTH = 456;
    private final GraphicsStub graphics = new GraphicsStub();
    private Rectangle rectangle;

    @BeforeEach
    void setUp() {
        rectangle = new Rectangle(HEIGHT, WIDTH);
    }

    @Test
    void draw() {
        rectangle.draw(graphics);

        assertEquals(1, GraphicsStub.getNumberOfCalls());
        assertEquals(0, graphics.getX());
        assertEquals(0, graphics.getY());
        assertEquals(HEIGHT, graphics.getHeight());
        assertEquals(WIDTH, graphics.getWidth());
    }

    @Test
    void testEquals() {
        Rectangle rectangle2 = new Rectangle(0, 0, HEIGHT, WIDTH);
        assertEquals(rectangle, rectangle2);
    }

}
