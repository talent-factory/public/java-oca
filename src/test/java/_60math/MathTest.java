/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _60math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MathTest {

    /**
     * Überprüfen der Fakultät im Bereich 0..8
     */
    @Test
    void factorial() {
        assertEquals(1, Math.factorial(0));
        assertEquals(1, Math.factorial(1));
        assertEquals(2, Math.factorial(2));
        assertEquals(6, Math.factorial(3));
        assertEquals(24, Math.factorial(4));
        assertEquals(120, Math.factorial(5));
        assertEquals(720, Math.factorial(6));
        assertEquals(5_040, Math.factorial(7));
        assertEquals(40_320, Math.factorial(8));
    }

    /**
     * Überprüfen verschiedener Binominalkoeffizienten. Bei diesem Test wird ein Überlauf des Datentyps
     * {@code long} nicht überprüft.
     */
    @Test
    void binominal() {
        assertEquals(1, Math.binominal(0, 0));
        assertEquals(1, Math.binominal(3, 0));
        assertEquals(1, Math.binominal(4, 0));

        assertEquals(1, Math.binominal(1, 1));
        assertEquals(1, Math.binominal(2, 2));
        assertEquals(1, Math.binominal(3, 3));

        assertEquals(20, Math.binominal(6, 3));

        assertEquals(2, Math.binominal(2, 1));
        assertEquals(3, Math.binominal(3, 1));
        assertEquals(10, Math.binominal(5, 2));

        assertEquals(184_756, Math.binominal(20, 10));

        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> Math.binominal(45, 6));
        assertEquals("Der Wertebereich ist überschritten.", exception.getMessage());
    }

    @Test
    void numberOfDigits() {
        assertEquals(2, Math.numberOfDigits(-15));
        assertEquals(1, Math.numberOfDigits(-1));
        assertEquals(1, Math.numberOfDigits(1));
        assertEquals(3, Math.numberOfDigits(123));
    }

}
