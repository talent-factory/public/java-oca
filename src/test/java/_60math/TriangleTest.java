/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _60math;

import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.UnrecognizedOptionException;
import org.junit.jupiter.api.Test;

import static com.github.stefanbirkner.systemlambda.SystemLambda.tapSystemOut;
import static org.junit.jupiter.api.Assertions.*;

class TriangleTest {

    @Test
    void testValidArguments() {
        String[] args = {
                "-a", "12", // Seitenlänge a
                "-b", "15", // Seitenlänge b
                "-c", "09", // Seitenlänge c
        };

        assertDoesNotThrow(() -> processArguments(args));
    }

    /**
     * Mit diesem Test stellen wir sicher, dass eine ungültige Option
     * (hier '{@code -x}') zu einem Fehler führt.
     */
    @Test
    void testInvalidArguments() {
        String[] args = {"-x", "15" };
        Exception exception = assertThrows(UnrecognizedOptionException.class,
                () -> processArguments(args));
        assertEquals("Unrecognized option: -x", exception.getLocalizedMessage());
    }

    /**
     * Mit diesem Test prüfen wir die {@code main()} Methode ohne Parameter
     * auf. In diesem Fall sollte der einleitende Text auf seine Vollständigkeit
     * überprüft werden.
     */
    @Test
    void testTriangleMain() throws Exception {
        String text = tapSystemOut(() -> Triangle.main(null));
        String expected = """
                Mit diesem Programm werden alle Berechnungen am Dreieck durchgeführt.
                Um ein Dreieck eindeutig berechnen zu können müssen drei Werte in
                beliebiger Reihenfolge eingegeben werden.
                                
                Falls diese Bedingung nicht erfüllt ist oder ungültige Werte eingegeben
                werden, dann findet keine Berechnung statt.
                                
                usage: java _60math.Triangle [options]
                 -a <arg>   Seitenlänge a
                 -A <arg>   Winkel α (Alpha)
                 -b <arg>   Seitenlänge b
                 -B <arg>   Winkel β (Beta)
                 -c <arg>   Seitenlänge c
                 -C <arg>   Winkel γ (Gamma)""";
        assertEquals(expected, text.trim());
    }

    /**
     * Verarbeiten der Argumentliste, so wie es dir Methode {@code main()}
     * machen würde.
     *
     * @param args Liste der Argumente
     * @throws ParseException bei der Eingabe eines ungültigen Argumentes
     */
    private void processArguments(String[] args) throws ParseException {
        CommandLineParser parser = new DefaultParser();
        parser.parse(Triangle.options, args);
    }
}
