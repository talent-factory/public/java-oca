package date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class YearTest {

    private Year year;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void newYear1900() {
        year = new Year(1900);
    }

    @Test
    public void newYear2020() {
        year = new Year(2020);
    }

    @Test
    public void newYear1899() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    year = new Year(1899);
                });
    }

    @Test
    public void newYear2021() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    year = new Year(2021);
                });
    }
}
