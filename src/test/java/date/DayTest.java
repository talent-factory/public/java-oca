package date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class DayTest {

    private Day day;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void day1() {
        day = new Day(1);
    }

    @Test
    public void day31() {
        day = new Day(31);
    }

    @Test
    public void day0() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    day = new Day(0);
                });
    }

    @Test
    public void day32() {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    day = new Day(32);
                });
    }
}
