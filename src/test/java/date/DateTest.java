package date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DateTest {

    Date date;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void test1() {
        date = new Date(new Day(1), Month.JAN, new Year(2020));
    }

    @Test
    public void test2() {
        date = new Date(new Day(29), Month.FEB, new Year(2020));
    }

    @Test
    public void test21() {
        date = new Date(29, Month.FEB, 2020);
    }

    @Test
    @DisplayName("Date(30, Month.FEB, 2020)")
    public void test3() {
        Exception exception = assertThrows(IllegalArgumentException.class,
                () -> date = new Date(30, Month.FEB, 2020));
        assertEquals("Illegal date provided.", exception.getMessage());
    }

    @Test
    public void testAdd() {
        Date date = new Date(1, Month.JAN, 2020);
        Date expect = new Date(2, Month.JAN, 2020);

        assertEquals(expect, date.add(1));
    }
}
