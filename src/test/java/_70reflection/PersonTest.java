/*
 * Released under MIT License
 *
 * Copyright (©) 2023 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _70reflection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Mit dieser Testklasse überprüfen wir unsere Klasse {@link _70reflection.Person}
 * mithilfe des Reflection APIs.
 * <p></p>
 *
 * @see <a href="https://www.baeldung.com/java-reflection">Guide to Java Reflection</a>
 */
class PersonTest {

    /**
     * Zu überprüfendes Objekt.
     */
    private Object person;

    /**
     * Gibt die Namen aller Felder des übergebenen {@link Field}-Arrays zurück.
     *
     * @param fields Array mit den Feldern
     * @return Liste mit den Namen der Felder
     */
    private List<String> getFieldNames(Field[] fields) {
        List<String> fieldNames = new ArrayList<>();
        for (Field field : fields)
            fieldNames.add(field.getName());
        return fieldNames;
    }

    @BeforeEach
    public void setUp() {
        person = new Person("peter", "MÜLLER", "47", "Hofmatte 3");
    }

    /**
     * Überprüfen aller Eigenschaften der Klasse {@link Person}.
     */
    @Test
    void checkProperties() {
        Field[] fields = person.getClass().getDeclaredFields();
        List<String> actualFieldNames = getFieldNames(fields);

        assertTrue(Arrays.asList("firstName", "lastName", "age", "address")
                .containsAll(actualFieldNames));
    }

}
