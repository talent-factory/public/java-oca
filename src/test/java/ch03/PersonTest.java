package ch03;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    private Person person;

    @BeforeEach
    void setUp() {
        person = new Person();
    }

    @Test
    void testEmptyConstructor() {
        assertEquals("Daniel", person.getFirstName());
        assertEquals("Senften", person.getLastName());
    }

    @Test
    void setFirstName() {
        person.setFirstName("Name");
        assertEquals("Name", person.getFirstName());
    }

}
