package adt;

import _40adt.RingBuffer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RingBufferTest {

    private RingBuffer<Integer> buffer;

    @BeforeEach
    public void setUp() {
        buffer = new RingBuffer<>();
    }

    @Test
    @DisplayName("Empty buffer")
    void isEmpty() {
        assertTrue(buffer.isEmpty());
    }

    @Test
    @DisplayName("Size of RingBuffer(3)")
    void sizeThree() {
        buffer = new RingBuffer<>(3);
        assertEquals(0, buffer.size());
        assertTrue(buffer.isEmpty());
    }

    @Test
    @DisplayName("Add one element to our queue")
    void addOne() {
        buffer.add(5);
        assertEquals(5, buffer.remove());
        assertTrue(buffer.isEmpty());
    }

    @Test
    @DisplayName("Add NULL element to our queue")
    void addNull() {
        assertThrows(NullPointerException.class,
                () -> buffer.add(null));
    }

    @Test
    @DisplayName("Remove from empty buffer")
    void removeEmpty() {
        buffer = new RingBuffer<>(3);
        assertNull(buffer.remove());
    }

}
