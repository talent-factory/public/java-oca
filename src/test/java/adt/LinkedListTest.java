package adt;

import _40adt.Buffer;
import _40adt.LinkedList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedListTest {

    private final Buffer<Integer> list = new LinkedList<>();

    @Test
    void isEmpty() {
        assertTrue(list.isEmpty());
    }

    @Test
    void addEmptyElement() {
        Exception exception = assertThrows(NullPointerException.class,
                () -> list.add(null));
        assertEquals("Null values are not allowed.", exception.getMessage());
    }

    @Test
    void addOneValue() {
        list.add(4711);
        assertFalse(list.isEmpty());
        assertTrue(list.contains(4711));
    }

    @Test
    void addSomeValues() {
        int[] numbers = {3, 7, 95, 28, 319, 1};
        for (int number : numbers)
            list.add(number);

        assertFalse(list.isEmpty());
        checkValues(numbers);
    }

    /**
     * Diese Funktion überprüft das Vorhandensein der übergebenen Werte.
     *
     * @param numbers zu überprüfende Werte
     */
    private void checkValues(int... numbers) {
        for (int number : numbers)
            assertTrue(list.contains(number));
    }
}
