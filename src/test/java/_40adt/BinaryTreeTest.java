/*
 * Released under MIT License
 *
 * Copyright (©) 2023 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _40adt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BinaryTreeTest {

    private BinaryTree<Integer> tree;

    @BeforeEach
    void setUp() {
        tree = new BinaryTree<>();
    }

    @Test
    @DisplayName("add(null)")
    void addNull() {

        Exception exception = assertThrows(NullPointerException.class, () -> {
            //noinspection DataFlowIssue
            tree.add(null);
        });

        String expectedMessage = "element is marked non-null but is null";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

    @Test
    @DisplayName("add(1)")
    void addOneValue() {
        List<Integer> values = List.of(1);
        tree.addAll(values);

        assertEquals(values.size(), tree.size());
        checkValues(values);
    }

    @Test
    @DisplayName("add(1, 2)")
    void addTwoValues() {
        List<Integer> values = List.of(1, 2);
        tree.addAll(values);

        assertEquals(values.size(), tree.size());
        checkValues(values);
    }

    @Test
    @DisplayName("add(<multiple values>)")
    void add() {
        List<Integer> values = List.of(6, 4, 8, 3, 5, 7, 9);
        tree.addAll(values);

        assertEquals(values.size(), tree.size());
        checkValues(values);
    }

    @Test
    @DisplayName("check correct sort order")
    void checkTreeStructure() {
        List<Integer> values = List.of(6, 4, 8, 3, 5, 7, 9);
        tree.addAll(values);

        List<Integer> actual = new ArrayList<>();
        for (int value : tree) {
            actual.add(value);
        }

        assertEquals(List.of(3, 4, 5, 6, 7, 8, 9), actual);
    }

    @Test
    @DisplayName("add(5, 2, 3)")
    void addThreeValues() {
        List<Integer> values = List.of(5, 2, 3);
        tree.addAll(values);

        assertEquals(values.size(), tree.size());
        checkValues(values);
    }

    @Test
    @DisplayName("add(5, 2, 3) - contains(4)")
    void searchNonexistentValue() {
        List<Integer> values = List.of(5, 2, 3);
        tree.addAll(values);

        assertEquals(values.size(), tree.size());
        assertFalse(tree.contains(4));
    }

    @Test
    @DisplayName("preorderIterator(1)")
    void preorderIterator() {
        List<Integer> values = List.of(1);
        tree.addAll(values);

        List<Integer> actual = new ArrayList<>();

        Iterator<Integer> iterator = tree.preorderIterator();
        while (iterator.hasNext()) {
            actual.add(iterator.next());
        }
        assertEquals(List.of(1), actual);

    }

    @Test
    @DisplayName("preorderIterator(6, 4, 8, 3, 5, 7, 9)")
    void preorderMultipleValues() {
        List<Integer> values = List.of(6, 4, 8, 3, 5, 7, 9);
        tree.addAll(values);

        List<Integer> actual = new ArrayList<>();

        Iterator<Integer> iterator = tree.preorderIterator();
        while (iterator.hasNext()) {
            actual.add(iterator.next());
        }
        assertEquals(List.of(6, 4, 3, 5, 8, 7, 9), actual);

    }

    /**
     * Überprüft das Vorhandensein aller Werte in der Liste.
     *
     * @param values zu prüfende Liste
     */
    private void checkValues(Collection<Integer> values) {
        values.forEach(value -> assertTrue(tree.contains(value)));
    }

}
