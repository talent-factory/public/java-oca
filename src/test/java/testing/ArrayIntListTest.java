package testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ArrayIntListTest {

    private final ArrayList<Integer> list = new ArrayList<>();

    @BeforeEach
    public void setUp() {
        list.clear();
    }

    @Test
    @DisplayName("add(-9)")
    public void testAdd1() {
        list.clear();
        list.add(-9);
        assertEquals(-9, list.get(0));
    }

    @Test
    public void test_add_get() {
        list.clear();
        list.add(42);
        list.add(-3);
        list.add(15);

        assertEquals(42, list.get(0));
        assertEquals(-3, list.get(1));
        assertEquals(15, list.get(2));
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    public void testIsEmpty() {
        list.clear();
        assertTrue(list.isEmpty());
        list.add(123);
        assertFalse(list.isEmpty());
        list.remove(0);
        assertTrue(list.isEmpty());
    }

}
