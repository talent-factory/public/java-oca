package q6;

public class Q6_07_MyClass {
}

class Satellite {
    void orbit() {
    }
}

class Moon extends Satellite {
    void orbit() {
    }
}

class ArtificialSatellite extends Satellite {
    void orbit() {
    }
}
