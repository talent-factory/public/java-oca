package q6;

public class Q6_02_MyClass {
}

class Flower {
    public void fragrance() {
        System.out.println("Flower");
    }
}

class Rose {
    public void fragrance() {
        System.out.println("Rose");
    }
}

class Lily {
    public void fragrance() {
        System.out.println("Lily");
    }
}

class Bouquet {
    public void arrangeFlowers() {
//        Flower f1 = new Rose();
//        Flower f2 = new Lily();
//        f1.fragrance();
    }
}
