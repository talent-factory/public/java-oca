package q6;

public class Q6_08_MyClass {
}

class Programmer {

    void print() {
        System.out.println("Programmer - Mala Gupta");
    }
}

class Author extends Programmer {
    void print() {
        System.out.println("Author - Mala Gupta");
    }
}

class TestEJava {
    public static void main(String[] args) {

        Programmer a = new Programmer();
        // TODO
        Author b = new Author();
        a.print();
        b.print();
    }
}
