package q6;

interface Movable {
    void move();
}

public class Q6_03_MyClass {
}

class Person implements Movable {
    public void move() {
        System.out.println("Person move");
    }
}

class Vehicle implements Movable {

    public void move() {
        System.out.println("Vehicle move");
    }
}

class Test {

    // TODO

//        movable.move();
//    }
}
