package q5;

public class Q5_08a_MyClass {
    public static void main(String[] args) {
        int i = 0;
        for (; i < 10; i++) ;
        for (i = 0; ; i++) break;
        for (i = 0; i < 10; ) i++;
        for (; ; ) ;
    }
}
