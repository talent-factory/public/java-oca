package q5;

public class Q5_05a_MyClass {
    public static void main(String[] args) {
        boolean b = false;
        int i = 1;
        do {
            i++;
            b = !b;
        } while (b);
        System.out.println(i);
    }
}
