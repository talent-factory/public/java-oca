package lists;

import java.util.ArrayList;
import java.util.List;

public class Sequence {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        list.add("apple");
        list.add("carrot");
        list.add("banana");
        list.add(1, "plum");
        System.out.println(list);
    }
}
