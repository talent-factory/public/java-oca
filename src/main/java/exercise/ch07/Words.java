package exercise.ch07;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;

class Words {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        PrintStream cons = System.out;
        cons.print("\n");

        cons.print(" Eingabe: ");
        String eingabe = keyboard.nextLine();

        StringTokenizer st = new StringTokenizer(eingabe);

        HashMap<String, Integer> hashTabelle = new HashMap<>();

        Integer ergebnis;
        int anzahl;
        String zeichenString;

        while (st.hasMoreTokens()) {
            zeichenString = st.nextToken();
            ergebnis = hashTabelle.get(zeichenString);

            if (ergebnis == null)
                hashTabelle.put(zeichenString, 1);
            else {
                anzahl = ergebnis + 1;
                hashTabelle.put(zeichenString, anzahl);
            }
        }

        cons.printf(" Anzahl verschiedene Wörter: %d \n", hashTabelle.size());

        cons.printf("\n %s \n", hashTabelle);
    }
}
