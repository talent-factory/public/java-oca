package exercise.ch07;

import java.io.IOException;

public class CharacterReader {

    public Character getNext() throws IOException {
        return (char) System.in.read();
    }

}
