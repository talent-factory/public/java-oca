package exercise.ch07;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;

public class WordCounter {

    public static void main(String[] args) {

        Map<String, Integer> hashTabelle = new HashMap<>();

        System.out.println("Your words: ");

        Scanner keyboard = new Scanner(System.in);
        String words = keyboard.nextLine();

        StringTokenizer stringTokenizer = new StringTokenizer(words);
        while (stringTokenizer.hasMoreTokens()) {
            String word = stringTokenizer.nextToken();
            hashTabelle.merge(word, 1, Integer::sum);
        }

        System.out.println("Result: " + hashTabelle);
    }
}
