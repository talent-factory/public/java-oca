package exercise.ch07;

import java.io.IOException;
import java.util.Stack;

public class TurnArount2 {

    public static void main(String[] args) throws IOException {

        System.out.print("Your word: ");

        CharacterReader reader = new CharacterReader();
        Stack<Character> stack = new Stack<>();

        Character ch;
        while ((ch = reader.getNext()) != '\n') {
            stack.push(ch);
        }

        while (!stack.empty()) {
            System.out.printf("'%s', ", stack.pop());
        }

        System.out.println();
    }
}
