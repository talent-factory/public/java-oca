package exercise.ch07;// Umdrehen der Buchstaben eines Wortes

import java.io.PrintStream;
import java.util.Scanner;
import java.util.Stack;

class TurnAround {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        PrintStream cons = System.out;
        cons.print("\n");

        cons.print(" Text eingeben: ");

        String eingabeWort = keyboard.nextLine();

        int anzahl = eingabeWort.length();

        // Die Buchstaben in einen Keller einfügen
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < anzahl; i++) {
            stack.push(eingabeWort.charAt(i));
        }

        // Elemente vom Stack entfernen und ausgeben
        cons.print(" Umgedreht    : ");
        for (int i = 0; i < anzahl; i++) {
            cons.printf("%s", stack.pop());
        }

        cons.print("\n");
    }
}
