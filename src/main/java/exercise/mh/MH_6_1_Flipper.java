package exercise.mh;

@SuppressWarnings("ConstantConditions")
public class MH_6_1_Flipper {

    public static void main(String[] args) {
        String o = "-";
        switch ("FRED".toLowerCase().substring(1, 3)) {
            case "yellow":
                o += "y";
            case "red":
                o += "r";
            case "green":
                o += "g";
        }
        System.out.println(o);
    }
}
