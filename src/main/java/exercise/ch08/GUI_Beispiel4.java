package exercise.ch08;// Das erste GUI-Programm mit Ereignisbehandlung

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GUI_Beispiel4 extends JFrame {

    // der Konstruktor legt drei Schaltflächen an
    GUI_Beispiel4(String titel) {
        super(titel);

        // Schaltflächen erzeugen
        JButton hansel = new JButton("Hänsel");
        JButton und = new JButton("und");
        JButton gretel = new JButton("Gretel");

        // Einen Layout-Manager zum Anordnen der Schalter festlegen
        setLayout(new FlowLayout());

        // Schaltflächen zum Frame hinzufügen
        add(hansel);
        add(und);
        add(gretel);

        // Anwendung schliessen, wenn Fenster geschlossen wird
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // ActionListener für die Schaltflächen registrieren.
        MouseListener mouseListener = new MyMouseListener();
        hansel.addMouseListener(mouseListener);
        und.addMouseListener(mouseListener);
        gretel.addMouseListener(mouseListener);
    }

    public static void main(String[] args) {
        GUI_Beispiel4 fenster = new GUI_Beispiel4("GUI mit Ereignisbehandlung");
        fenster.pack();
        fenster.setVisible(true);
    }

    private static class MyMouseListener implements MouseListener {

        public void mouseClicked(MouseEvent e) {
            System.out.println("Mouse clicked at [" + e.getPoint() + "]");
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }
    }
}
