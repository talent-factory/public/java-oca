package exercise.ch08;// Das erste GUI-Programm mit Ereignisbehandlung

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI_Beispiel3 extends JFrame {

    private static GUI_Beispiel3 fenster;
    private JButton hansel, and, gretel;

    // der Konstruktor legt drei Schaltflächen an
    GUI_Beispiel3(String titel) {
        super(titel);

        // Schaltflächen erzeugen
        hansel = new JButton("Hänsel");
        and = new JButton("und");
        gretel = new JButton("Gretel");


        // Einen Layout-Manager zum Anordnen der Schalter festlegen
        setLayout(new FlowLayout());

        // Schaltflächen zum Frame hinzufügen
        add(hansel);
        add(and);
        add(gretel);

        // Anwendung schliessen, wenn Fenster geschlossen wird
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        // ActionListener für die Schaltflächen registrieren.
        ActionListener actionListener = new MyActionListener();
        hansel.addActionListener(actionListener);
        and.addActionListener(actionListener);
        gretel.addActionListener(actionListener);
    }

    public static void main(String[] args) {
        fenster = new GUI_Beispiel3("GUI mit Ereignisbehandlung");
        fenster.pack();
        fenster.setVisible(true);
    }

    // Die eigenen Adapter- und Listener-Klassen als innere Klassen innerhalb der Klasse definieren
    class MyActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            // Titel ändern
            hansel.setText("verirrten");
            and.setText("sich");
            gretel.setText("im Wald");
            fenster.pack(); // Notwendig, damit das Layout angepasst wird
        }
    }
}
