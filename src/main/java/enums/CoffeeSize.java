package enums;

public enum CoffeeSize {

    BIG("gross"),
    HUGE("grösser"),
    VERY_HUGE("noch grösser") {

    };

    private String name;

    CoffeeSize(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
