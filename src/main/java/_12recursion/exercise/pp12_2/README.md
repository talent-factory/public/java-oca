# Programming Project

A `Bag` is a data structure that can be defined as: A `Bag` may 
be empty, or a `Bag` may contain an `Object` and a `Bag`. 
Identify the base class and the recursive case in this definition. Write a
Java class that implements the `Bag` structure.

Include a methode to add items to the `Bag`. Some helper methods may
include `size()`, which returns the number of elements in the `Bag`,
and `isEmpty()`, which returns `true` if the `Bag` is empty.

Use recursion to write a `toString()` method that returns a string in
the form `(1,(2,(3,EMPTY)))`, which describes a `Bag` containing the
integers 1, 2, and 3. Write a driver program to test the functionality of the
`Bag`.
