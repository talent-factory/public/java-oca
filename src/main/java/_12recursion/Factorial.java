package _12recursion;

import edu.princeton.cs.stdlib.StdOut;

/**
 * Berechnen der Fakultät.
 * <p>
 *
 * @author <a href="mailto:daniel@senften.org">Daniel Senften</a>
 * @see <a href="https://de.wikipedia.org/wiki/Fakult%C3%A4t_(Mathematik)">Wikipedia</a>
 */
public class Factorial {

    // Rekursive Funktion
    public static int fact(int number) {
        if (number == 0) return 1;
        return number * fact(number - 1);
    }

    // Nicht-rekursive Funktion
    public static int fact2(int number) {
        int result = 1;

        if (number == 0) return result;

        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }

    public static void main(String[] args) {
        int result = fact(5);
        StdOut.println(result);
    }
}
