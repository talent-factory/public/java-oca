package _25stream;

import java.util.List;

@SuppressWarnings("java:S106")
public class SumWithoutStream {

    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7);

        int sum = 0;
        for (int n : list) {
            if (n % 2 == 1 && n > 5) {
                sum += n * n;
            }
        }

        System.out.println(sum);
    }
}
