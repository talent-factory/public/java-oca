package _25stream;

import java.util.List;

@SuppressWarnings("java:S106")
public class SumWithStream {

    public static void main(String[] args) {

        List<Integer> list = List.of(1, 2, 3, 4, 5, 6, 7);

        int sum = list
                .stream()
                .filter(n -> n % 2 == 1)
                .filter(n -> n > 6)
                .map(n -> n * n)
                .reduce(0, Integer::sum);

        System.out.println(sum);
    }
}
