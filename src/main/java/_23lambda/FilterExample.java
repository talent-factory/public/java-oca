package _23lambda;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

@SuppressWarnings("java:S106")
public class FilterExample {

    public static <T> List<T> filter(List<T> list, Predicate<T> p) {
        List<T> result = new ArrayList<>();
        for (T element : list) {
           if (p.test(element)) {
               result.add(element);
           }
        }
        return result;
    }

    public static void main(String[] args) {

        List<String> list1 = List.of("Anna", "", "Daniel", "Raffaele");

        Predicate<String> p = s -> s.length() > 2;
        Predicate<String> q = p.and(s -> s.contains("el"));

        List<String> list2 = filter(list1, q);
        System.out.println(list2);

    }
}
