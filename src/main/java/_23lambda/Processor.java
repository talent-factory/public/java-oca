package _23lambda;

public interface Processor<T> {
    int getStringLength(T string);
}
