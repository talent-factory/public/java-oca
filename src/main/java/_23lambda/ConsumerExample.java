package _23lambda;

import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings("java:S106")
public class ConsumerExample {

    public static <T> void forEach(List<T> list, Consumer<T> c) {
       for (T element : list) {
           c.accept(element);
       }
    }

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3, 4, 5);
        forEach(list, System.out::println);
    }
}
