package uebung;

import lombok.ToString;

@ToString
public class Length {

    private final double value;
    private final Unit unit;

    public Length(double value, Unit unit) {
        // Plausi
        this.value = value;
        this.unit = unit;
    }

    public Length() {
        this(0, Unit.METER);
    }

    public Length add(Length other) {
        Length result;
        if (this.unit == other.unit) {
            result = new Length(this.value + other.value, this.unit);
        } else {
            result = new Length(
                    this.value * this.unit.getFactor() +
                            other.value * other.unit.getFactor(), Unit.METER);
        }
        return result;
    }

    public Length convert(Unit unit) {
        // TODO Must be implemented 😀
        return null;
    }
}
