package uebung;

import lombok.Getter;

public enum Unit {
    METER(1.0),
    CENTIMETER(0.01),
    INCH(0.3),
    MEILE(1.482);

    @Getter
    private final double factor;

    Unit(double factor) {
        this.factor = factor;
    }
}
