package uebung;

public class Main {

    public static void main(String[] args) {

        Length b = new Length(1.0, Unit.INCH); // 0, m
        Length a = new Length(1.0, Unit.MEILE);

        /*
         *    6m     = 3m + 3m
         *    6cm    = 3cm + 3cm
         *    3.03m  = 3m + 3cm
         *    3.03m  = 3cm + 3m
         *           = 3in + 2cm
         */
        Length c = a.add(b);

        System.out.println(c.convert(Unit.INCH));
    }

}
