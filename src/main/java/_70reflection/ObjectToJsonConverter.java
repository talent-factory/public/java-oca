/*
 * Released under MIT License
 *
 * Copyright (©) 2023 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _70reflection;

import java.io.NotSerializableException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @See <a href="https://www.baeldung.com/java-custom-annotation">Creating a Custom Annotation</a>
 */
@SuppressWarnings("all")
public class ObjectToJsonConverter {

    /**
     * Diese Funktion überprüft, ob die Annotation {@code JsonSerializable} gesetzt ist.
     *
     * @param object zu überprüfendes Objekt
     * @throws NotSerializableException wird geworfen, wenn die Annotation nicht gesetzt ist
     */
    private void checkIfSerializable(Object object) throws NotSerializableException {
        if (Objects.isNull(object)) {
            throw new NotSerializableException("The object to serialize is null");
        }

        Class<?> clazz = object.getClass();
        if (!clazz.isAnnotationPresent(JsonSerializable.class)) {
            throw new NotSerializableException("The class "
                    + clazz.getSimpleName()
                    + " is not annotated with JsonSerializable");
        }
    }

    /**
     * Diese Funktion überprüft, ob die Annotation {@code Init} gesetzt ist. Falls ja, wird die Methode
     * entsprechend aufgerufen.
     *
     * @param object zu überprüfendes Objekt
     * @throws Exception wird geworfen, wenn die Methode nicht korrekt terminiert.
     */
    private void initializeObject(Object object) throws Exception {
        Class<?> clazz = object.getClass();
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Init.class)) {
                method.setAccessible(true);
                method.invoke(object);
            }
        }
    }

    /**
     * Diese Funktion überprüft, ob die Annotation {@code JsonElement} gesetzt ist. Falls ja,
     * wird das entsprechende Element als JSON zurückgegeben.
     *
     * @param object zu überprüfendes Objekt
     * @return JSON-String aller Elemente
     */
    private String getJsonString(Object object) throws IllegalAccessException {
        Class<?> clazz = object.getClass();
        Map<String, String> jsonElementsMap = new HashMap<>();
        for (Field field : clazz.getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(JsonElement.class)) {
                jsonElementsMap.put(getKey(field), (String) field.get(object));
            }
        }

        String jsonString = jsonElementsMap.entrySet()
                .stream()
                .map(entry -> "\"" + entry.getKey() + "\":\""
                        + entry.getValue() + "\"")
                .collect(Collectors.joining(","));
        return "{" + jsonString + "}";
    }

    /**
     * Diese Funktion überprüft, ob die Annotation {@code JsonElement} die Eigenschaft
     * {@code key} gesetzt hat. Falls ja, wird der Inhalt dieses Schlüssels zurückgegeben.
     * Falls nicht, dann wird der Name des Feldes/der Eigenschaft zurückgegeben.
     *
     * @param field zu überprüfendes Feld
     * @return Name des Feldes/der Eigenschaft
     */
    private String getKey(Field field) {
        String value = field.getAnnotation(JsonElement.class).key();
        return value.isEmpty() ? field.getName() : value;
    }

    /**
     * Konvertiert alle eigenschaften des Objekts in JSON.
     *
     * @param object zu konvertierendes Objekt
     * @return JSON-String der Eigenschaften
     */
    public String convertToJson(Object object) {
        try {
            checkIfSerializable(object);
            initializeObject(object);
            return getJsonString(object);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

}
