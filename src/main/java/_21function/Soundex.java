package _21function;

import org.apache.commons.codec.language.ColognePhonetic;

/**
 * Soundex ist ein phonetischer Algorithmus zur Indizierung von Wörtern und
 * Phrasen nach ihrem Klang in der englischen Sprache. Gleichklingende Wörter
 * sollen dabei zu einer identischen Zeichenfolge kodiert werden.
 * <p>
 *
 * @author <a href="mailto:daniel.senften@talent-factory.ch">Daniel Senften</a>
 * @see <a href="https://de.wikipedia.org/wiki/Soundex">Wikipedia</a>
 * @see <a href="https://howtodoinjava.com/algorithm/implement-phonetic-search-using-soundex-algorithm/">Implement Phonetic search using Soundex algorithm</a>
 */
public class Soundex {

    public static final String[] words = {
            "Sänften",  // S513
            "Senften",  // S513
            "Müller",   // M460
            "müller",   // M460
            "muller",   // M460
            "Mueller",  // M460
    };

    public static String encode(String s) {

        char[] x = s.toUpperCase().toCharArray();
        char firstLetter = x[0];

        for (int i = 0; i < x.length; i++) {
            switch (x[i]) {
                case 'B', 'F', 'P', 'V', 'W' -> x[i] = '1';
                case 'C', 'G', 'K', 'Q', 'X', 'S', 'Z', 'ß' -> x[i] = '2';
                case 'D', 'T' -> x[i] = '3';
                case 'L' -> x[i] = '4';
                case 'M', 'N' -> x[i] = '5';
                case 'R' -> x[i] = '6';
                default -> x[i] = '0';
            }
        }

        StringBuilder output = new StringBuilder("" + firstLetter);
        for (int i = 1; i < x.length; i++)
            if (x[i] != x[i - 1] && x[i] != '0')
                output.append(x[i]);

        // Pad with 0's or truncate
        return output.append("0000").substring(0, 4);
    }

    public static void main(String[] args) {

        System.out.println("Eigene soundex() Funktion");
        for (String word : words) {
            System.out.format("%10s - %s\n", word, encode(word));
        }

        ColognePhonetic apache = new ColognePhonetic();

        System.out.println("\nUnd nun Kölner Phonetik");
        for (String word : words) {
            System.out.format("%10s - %s\n", word, apache.encode(word));
        }

    }
}
