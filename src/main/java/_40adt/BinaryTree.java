/*
 * Released under MIT License
 *
 * Copyright (©) 2023 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _40adt;

import lombok.NonNull;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class BinaryTree<T extends Comparable<T>> implements Buffer<T> {

    Node<T> root;
    private int size;

    /**
     * Hinzufügen eines neuen Elementes in unseren binären Baum.
     *
     * @param element hinzuzufügendes Element
     */
    @Override
    public void add(@NonNull T element) {
        root = add(element, root); // Rekursive Funktion aufrufen
    }

    private Node<T> add(T element, Node<T> node) {

        if (node == null) {
            node = new Node<>(element);
            size++;
            return node;
        }

        if (element.compareTo(node.payload) < 0) {
            node.left = add(element, node.left);
            node.left.parent = node;
        } else {
            node.right = add(element, node.right);
            node.right.parent = node;
        }

        return node;
    }

    /**
     * Wird aktuell noch nicht unterstützt.
     */
    @Override
    public T remove() {
        throw new UnsupportedOperationException("Remove operation is not supported");
    }

    /**
     * Liefert die Aktuelle Grösse resp. die Anzahl der Elemente in unserem Baum.
     *
     * @return Anzahl der Elemente in unserem Baum.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Gibt ein Iterator Objekt zurück, mit welchem die Datenstruktur traversiert werden kann.
     *
     * @return Iterator
     */
    @NotNull
    @Override
    public Iterator<T> iterator() {
        return new InorderIterator(root);
    }

    public Iterator<T> preorderIterator() {
        return new PreorderIterator(root);
    }

    /**
     * Repräsentiert einen einzelnen Knoten in unserem binären Baum.
     *
     * @param <T> konkreter Datentyp eines Elementes in unserem Knoten
     */
    static class Node<T> {

        T payload;
        Node<T> left;
        Node<T> right;
        Node<T> parent;

        public Node(@NonNull T value) {
            payload = value;
        }

    }

    private class PreorderIterator implements Iterator<T> {
        private Node<T> current;
        private Node<T> next;
        private boolean hasNext;

        public PreorderIterator(Node<T> root) {

            if (root == null) {
                throw new IllegalArgumentException("Root node cannot be null");
            }

            current = root;
            hasNext = true;
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public T next() {

            if (!hasNext) {
                throw new NoSuchElementException("No more elements in tree");
            }

            // Zuweisen des aktuellen Wertes. Bei jedem Aufruf von next() bewegen wir uns inorder
            // an eine andere Stelle im Baum.
            T value = current.payload;

            if (current.left != null) {
                next = current.left;
            } else if (current.parent != null) {
                if (current.parent.right != null) {
                    next = current.parent.right;
                } else hasNext = false;
            }

            current = next;
            return value;
        }
    }

    /**
     * Dieser Iterator traversiert unsere Baumstruktur mit dem
     * <a href="https://de.wikipedia.org/wiki/Bin%C3%A4rbaum#Traversierung">Inorder</a> Algorithmus.
     */
    private class InorderIterator implements Iterator<T> {

        private Node<T> current;
        private Node<T> next;
        private boolean hasNext;

        /**
         * In dieser Version des Konstruktors wird der aktuelle Baum zuerst komplett nach links
         * traversiert. Dort bleiben wir stehen, bis der Anwender mit dem Aufruf von {@link InorderIterator#next()}
         * das nächste Element anfordert.
         *
         * @param root Wurzelknoten
         */
        public InorderIterator(Node<T> root) {

            if (root == null) {
                throw new IllegalArgumentException("Root node cannot be null");
            }

            current = root;
            hasNext = true;
            while (current.left != null) {
                current = current.left;
            }
        }

        @Override
        public boolean hasNext() {
            return hasNext;
        }

        @Override
        public T next() {

            if (!hasNext) {
                throw new NoSuchElementException("No more elements in tree");
            }

            // Zuweisen des aktuellen Wertes. Bei jedem Aufruf von next() bewegen wir uns inorder
            // an eine andere Stelle im Baum.
            T value = current.payload;

            // Bestimmen des nächsten Knotens im Baum.
            if (current.right != null) {
                next = current.right;
                while (next.left != null) {
                    next = next.left;
                }

            } else if (current.parent != null) {
                if (current != current.parent.left) {
                    while (current.parent != null && current == current.parent.right) {
                        current = current.parent;
                    }
                }
                next = current.parent;
                if (next == null) hasNext = false;

            }

            current = next;
            return value;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove operation is not supported");
        }
    }
}
