package _40adt;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

@SuppressWarnings("unchecked")
public class LinkedList<T extends Comparable<T>> implements Buffer<T> {

    private Node<T> root;
    private int size;

    /**
     * Hinzufügen eins neuen Elementes in der Datenstruktur. Elemente mit dem
     * Wert {@code null} werden nicht akzeptiert.
     *
     * @param element hinzuzufügendes Element
     */
    @Override
    public void add(T element) {
        Objects.requireNonNull(element, "Null values are not allowed.");
        size++;

        Node<T> newNode = new Node<>(element);

        if (root == null || element.compareTo(root.payload) < 0) {
            newNode.next = root;
            root = newNode;
            return;
        }

        Node<T> current = root;
        while (current.next != null && current.next.payload.compareTo(element) < 0) {
            current = current.next;
        }

        newNode.next = current.next;
        current.next = newNode;
    }

    /**
     * Entfernen des ersten Elementes aus der Datenstruktur
     *
     * @return das soeben gelöschte Element aus der Datenstruktur
     */
    public T remove() {
        T element = null;
        if (root != null) {
            element = root.payload;
            root = root.next;
        }
        return element;
    }

    /**
     * Aktuelle Anzahl Elemente in der Datenstruktur.
     *
     * @return aktuelle Grösse der Datenstruktur.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Wir ausschliesslich für interne Tests verwendet
     */
    private ArrayList<T> toList() {

        ArrayList<T> list = new ArrayList<>();

        Node<T> current = root;    // Initialisierung der Schleife
        while (current != null) {  // Abbruchkriterium
            list.add(current.payload);
            current = current.next;
        }

        return list;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other)
            return true;

        if (!(other instanceof LinkedList))
            return false;

        if (size != ((LinkedList<T>) other).size())
            return false;

        return toList().equals(((LinkedList<T>) other).toList());
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<T> iterator() {
        return new LinkedListIterator();
    }

    /**
     * Repräsentiert einen einzelnen Knoten in unserer Datenstruktur.
     *
     * @param <T> konkreter Datentyp eines Elementes in unserem Knoten
     */
    private static class Node<T> {
        T payload;
        Node<T> next;

        public Node(@NonNull T value) {
            payload = value;
        }
    }

    private class LinkedListIterator implements Iterator<T> {
        private Node<T> current = root;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return current != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws java.util.NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next() {
            T element = current.payload;
            current = current.next;
            return element;
        }
    }

}
