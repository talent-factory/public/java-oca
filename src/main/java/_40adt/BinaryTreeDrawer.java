/*
 * Released under MIT License
 *
 * Copyright (©) 2023 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _40adt;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.List;

public class BinaryTreeDrawer extends Application {

    private final BinaryTree<Integer> tree = new BinaryTree<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        Pane pane = new Pane();

        // Erstellen des binären Baumes
        List<Integer> values = List.of(6, 4, 8, 3, 5, 7, 9);
        tree.addAll(values);

        drawBinaryTree(tree.root, pane, 400, 50, 200);

        Scene scene = new Scene(pane, 800, 600);
        primaryStage.setTitle("Binary Tree Drawer");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void drawBinaryTree(BinaryTree.Node<Integer> node, Pane pane, double x, double y, double xOffset) {

        if (node == null) {
            return;
        }

        // Erstelle den Kreis
        double circleRadius = 20;
        Circle circle = new Circle(x, y, circleRadius);
        circle.setFill(Color.WHITE);
        circle.setStroke(Color.BLACK);

        // Erstelle den Text
        Text text = new Text(String.valueOf(node.payload));
        text.setFont(Font.font("Verdana", FontWeight.BOLD, 14));

        // Zentriere den Text innerhalb des Kreises
        double textWidth = text.getLayoutBounds().getWidth();
        double textHeight = text.getLayoutBounds().getHeight();
        text.setLayoutX(circle.getCenterX() - textWidth / 2);
        text.setLayoutY(circle.getCenterY() + textHeight / 4);

        pane.getChildren().addAll(circle, text);

        // Zeichne den linken Teilbaum
        if (node.left != null) {
            double childX = x - xOffset;
            double childY = y + 80;
            drawBinaryTree(node.left, pane, childX, childY, xOffset / 2);

            // Zeichne eine Linie zum linken Kindknoten
            pane.getChildren().add(new javafx.scene.shape.Line(x, y + circleRadius, childX, childY - circleRadius));
        }

        // Zeichne den rechten Teilbaum
        if (node.right != null) {
            double childX = x + xOffset;
            double childY = y + 80;
            drawBinaryTree(node.right, pane, childX, childY, xOffset / 2);

            // Zeichne eine Linie zum rechten Kindknoten
            pane.getChildren().add(new javafx.scene.shape.Line(x, y + circleRadius, childX, childY - circleRadius));
        }
    }

}
