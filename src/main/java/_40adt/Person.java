package _40adt;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.Period;
import java.util.Objects;

/**
 * Diese Klasse, welche eine Person repräsentiert wird für die Demonstration
 * unterschiedlicher Funktionen der Eclipse Collection verwendet.
 */
@Data
@AllArgsConstructor
@Builder
public class Person implements Comparable<Person> {

    private final String firstName;
    private final String lastName;

    private final LocalDate birthDate;

    /**
     * Berechnet das Alter einer Person.
     *
     * @return Alter der Person
     */
    public int getAge() {
        return Period.between(birthDate, LocalDate.now()).getYears();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) && Objects.equals(lastName, person.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public int compareTo(@NotNull Person o) {

        if (lastName.compareTo(o.lastName) == 0) {
            return firstName.compareTo(o.firstName);
        }

        return lastName.compareTo(o.lastName);
    }
}
