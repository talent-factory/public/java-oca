package _40adt;

import java.util.Collection;
import java.util.Iterator;

/**
 * Generelle Beschreibung eines Datenspeichers.
 */
public interface Buffer<T> extends Iterable<T> {

    /**
     * Hinzufügen eins neuen Elementes in der Datenstruktur. Elemente mit dem
     * Wert {@code null} werden nicht akzeptiert.
     *
     * @param element hinzuzufügendes Element
     */
    void add(T element);

    /**
     * Entfernen eines Elements aus der Datenstruktur
     *
     * @return Element aus der Datenstruktur
     */
    T remove();

    /**
     * Hat es Elemente in der Datenstruktur
     *
     * @return {@code true}, falls die Datenstruktur leer ist
     */
    default boolean isEmpty() {
        Iterator<T> iterator = this.iterator();
        return !iterator.hasNext();
    }

    /**
     * Aktuelle Anzahl Elemente in der Datenstruktur.
     *
     * @return aktuelle Grösse der Datenstruktur.
     */
    int size();

    /**
     * Überprüft, ab der gesuchte Wert in der Datenstruktur vorhanden ist.
     *
     * @param element gesuchter Wert
     * @return {@code true}, falls der gesuchte Wert vorhanden ist
     */
    default boolean contains(T element) {
        for (T current : this) {
            if (current.equals(element))
                return true;
        }
        return false;
    }

    /**
     * Fügt alle Elemente der Sammlung {@code collection} sequentiell in
     * dieser Datenstruktur ein.
     *
     * @param collection Liste der einzufügenden Elemente
     */
    default void addAll(Collection<? extends T> collection) {
        collection.forEach(this::add);
    }
}
