package _40adt;

import java.util.Iterator;
import java.util.Objects;

/**
 * Implementation eines RungBuffers
 */
public class RingBuffer<T> implements Buffer<T> {

    private final int CAPACITY;

    private final T[] buffer;
    private int front = 0;
    private int rear = 0;

    /**
     * Aktuelle Anzahl gespeicherter Elemente
     */
    private int queueSize;

    /**
     * Erstellen eines neuen RinBuffer mit einer initialen Kapazität von {@code 1'000}
     * Elementen.
     */
    public RingBuffer() {
        this(1_000);
    }

    /**
     * Erstellen eines neuen RinBuffer mit der vorgegebenen Kapazität
     *
     * @param capacity gewünschte Grösse des RingBuffers
     */
   @SuppressWarnings("unchecked")
    public RingBuffer(final int capacity) {
        this.buffer = (T[]) new Object[capacity];
        this.CAPACITY = capacity;
    }

    /**
     * Hinzufügen eins neuen Elementes in der Datenstruktur. Elemente mit dem
     * Wert {@code null} werden nicht akzeptiert.
     *
     * @param element hinzuzufügendes Element
     */
    @Override
    public void add(T element) {
        Objects.requireNonNull(element);
        buffer[rear] = element;
        rear = increment(rear);
    }

    /**
     * Inkrementiert den angegebenen Wert unter Berücksichtigung der maximalen
     * Grösse der Datenstruktur.
     *
     * @param value zu inkrementierender Wert
     * @return inkrementierter Wert
     */
    private int increment(int value) {
        return (value + 1) % CAPACITY;
    }

    /**
     * Entfernen des nächsten Elementes aus unserer FIFO Queue.
     *
     * @return das soeben gelöschte Element
     */
    @Override
    public T remove() {
        T value = buffer[front];
        buffer[front] = null;
        front = increment(front);
        return value;
    }

    /**
     * Maximale Grösse unserer FIFO Queue
     *
     * @return Grösse der Queue
     */
    @Override
    public int size() {
        return rear - front;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<T> iterator() {
        return new RingBufferIterator();
    }

    private class RingBufferIterator implements Iterator<T> {
        private int current = front;

        /**
         * Returns {@code true} if the iteration has more elements.
         * (In other words, returns {@code true} if {@link #next} would
         * return an element rather than throwing an exception.)
         *
         * @return {@code true} if the iteration has more elements
         */
        @Override
        public boolean hasNext() {
            return current != rear;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         * @throws java.util.NoSuchElementException if the iteration has no more elements
         */
        @Override
        public T next() {
            return buffer[current++];
        }
    }
}
