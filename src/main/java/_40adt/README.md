# Abstrakte Datentypen
   
Die in diesem `Package` verwendeten Klassen und Interfaces sind im folgenden UML 
Klassendiagramm dargestellt. 

![UML](../../resources/adt-uml.png)
