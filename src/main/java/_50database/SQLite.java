package _50database;

import lombok.extern.java.Log;

import java.sql.*;

@Log
public class SQLite {

    /**
     * Statische Verbindung zu unserer Datenbank.
     */
    private static Connection connection;

    static {
        try {
            String url = "jdbc:sqlite:/Users/daniel/GitRepository/java-oca/sqlite.db";
            connection = DriverManager.getConnection(url);
        } catch (SQLException e) {
            log.severe(e.getMessage());
            System.exit(1);
        }
    }

    public SQLite() {
        createTable();
        insert("Daniel Senften");
        selectAll();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new SQLite();
    }

    public void selectAll() {
        String sql = "SELECT * FROM employee";

        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" +
                        rs.getString("name"));
            }
        } catch (SQLException e) {
            log.severe(e.getMessage());
        }
    }

    public void createTable() {
        String sql = """
                CREATE TABLE IF NOT EXISTS employee (
                  id integer PRIMARY KEY,
                  name text NOT NULL
                );
                """;
        try {
            Statement stmt = connection.createStatement();
            stmt.execute(sql);
        } catch (SQLException e) {
            log.severe(e.getMessage());
        }
    }

    public void insert(String name) {
        String sql = "INSERT INTO employee(name) VALUES(?)";

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, name);
            statement.executeUpdate();
        } catch (SQLException e) {
            log.severe(e.getMessage());
        }
    }
}
