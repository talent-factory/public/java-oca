package operators;

public class BinaryLiteralsExample {

    static long l1 = 0b0_000_011_111_100_001;
    static long l2 = 0b1_101_010_100_101_010;

    static int security = 0b111_111_000;

    static long l3 = l1 & l2;

    public static void main(String[] args) {
        // System.out.println(Long.toBinaryString(l3));

        int groupReadOnly = 0b000_100_000;

        if ((security & groupReadOnly) == groupReadOnly) {
            System.out.println("Leserechte");
        }


    }
}
