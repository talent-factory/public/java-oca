package date;

import lombok.Getter;

@Getter
public class Date {

    private final Day day;
    private final Month month;
    private final Year year;

    /**
     * Erstellen eines neuen Datumsobjektes
     *
     * @param day   Tag
     * @param month Monat
     * @param year  Jahr
     */
    public Date(Day day, Month month, Year year) {
        if (!isValid(day, month, year))
            throw new IllegalArgumentException("Illegal date provided.");

        this.day = day;
        this.year = year;
        this.month = month;
    }

    public Date(int day, Month month, int year) {
        this(new Day(day), month, new Year(year));
    }

    public Date add(int day) {

        Date result = null;
        if (day < 29)
            result = new Date(
                    this.getDay().getDay() + day,
                    this.getMonth(),
                    this.getYear().getYear());
        return result;
    }

    public Date add(Day day) {
        return this.add(day.getDay());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Date date = (Date) o;

        if (!day.equals(date.day)) return false;
        if (month != date.month) return false;
        return year.equals(date.year);
    }

    @Override
    public int hashCode() {
        int result = day.hashCode();
        result = 31 * result + month.hashCode();
        result = 31 * result + year.hashCode();
        return result;
    }

    private boolean isValid(Day day, Month month, Year year) {

        boolean valid = true;

        switch (month) {
            case APR:
            case JUN:
            case SEP:
            case NOV:
                if (day.getDay() == 31) return false;
                break;

            case FEB:
                if (day.getDay() <= 28) return true;
                if (day.getDay() == 29 && isLeapYear(year.getYear())) return true;
                valid = false;
        }

        return valid;
    }

    /**
     * Berechnen eines Schaltjahres.
     *
     * @param year ist dies ein Schaltjahr
     * @return TRUE, falls es sich um ein Schaltjahr handelt
     */
    public boolean isLeapYear(int year) {
        boolean leapYear = year % 4 == 0;

        if (year % 100 == 0) leapYear = false;

        if (year % 400 == 0)  leapYear = true;

        return leapYear;
    }
}
