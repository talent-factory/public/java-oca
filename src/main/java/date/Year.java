package date;

import lombok.Data;

@Data
public class Year {

    private int year;

    /**
     * Erstellen eines neuen Jahr Objektes, wobei ein Jahr im Bereich
     * 1900 - 2020 liegen muss.
     *
     * @param year Jahreszahl
     */
    public Year(int year) {
        if (year < 1900 || year > 2020)
            throw new IllegalArgumentException("Year must be in the range 1900 - 2020");
        this.year = year;
    }

    public static void main(String[] args) {

        Year first = new Year(2019);
        System.out.println(first);     // Impliziter Aufruf von toString()

        Year second = new Year(2019);
        System.out.println(second);

        if (first.equals(second)) {
            System.out.println("Identical");
        } else
            System.out.println("Not identical");
    }
}
