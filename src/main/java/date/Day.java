package date;

import lombok.Getter;

@Getter
public class Day {

    private int day;

    public Day(int day) {
        if (day < 1 || day > 31)
            throw new IllegalArgumentException("Day must be in the range 1 - 31");

        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Day day1 = (Day) o;

        return day == day1.day;
    }

    @Override
    public int hashCode() {
        return day;
    }
}
