/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * Mit dieser Klasse wollen wir ein beliebiges Längenmass
 * (s. <a href="https://de.wikipedia.org/wiki/L%C3%A4ngenma%C3%9F">Wikipedia</a> abbilden.
 */
@Getter
@ToString
public class Length {

    /**
     * Interne Repräsentation der Länge.
     */
    private final double length;

    /**
     * Einheit des Längenobjektes.
     */
    private final LengthUnit unit;

    /**
     * Erstellen eines neuen Längenmasses mit den gewünschten Parametern. Mit
     * diesem Null-Argument-Konstruktor wird ein neues Objekt der Länge `0 Meter`
     * instanziiert.
     */
    public Length() {
        this(0, LengthUnit.METER);
    }

    /**
     * Erstellen eines neuen Längenmasses mit den gewünschten Parametern. Ein
     * Längenmass kann sowohl negative, als auch positive Werte beinhalten. Als
     * Einheit wird in diesem Fall `Meter` angenommen;
     *
     * @param length Längenmass in der gewünschten Einheit
     */
    public Length(double length) {
        this(length, LengthUnit.METER);
    }

    /**
     * Erstellen eines neuen Längenmasses mit den gewünschten Parametern. Ein
     * Längenmass kann sowohl negative, als auch positive Werte beinhalten.
     *
     * @param length Längenmass in der gewünschten Einheit
     * @param unit   Einheit unseres Längenmasses
     */
    public Length(double length, @NonNull LengthUnit unit) {
        this.length = length;
        this.unit = unit;
    }

    public Length add(Length other) {
        if (length != other.getLength())
            return new Length(
                    length * unit.getConversionFactor() +
                            other.length * other.unit.getConversionFactor(),
                    LengthUnit.METER);
        return new Length(length + other.length, unit);
    }

}
