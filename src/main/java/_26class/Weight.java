/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import lombok.Getter;
import lombok.ToString;

/**
 * Mit dieser Klasse wollen wir Gewichtsobjekte repräsentieren.
 */
@ToString
@Getter
public class Weight {

    /**
     * Beinhaltet den absoluten Wert des Gewichts.
     */
    private final double value;

    private final String unit;

    /**
     * Initialisieren eines neuen Gewichts Objektes, wobei wir den Wert 0.0 und die
     * Einheit kg annehmen.
     */
    public Weight() {
        this(0.0, "kg");
    }

    public Weight(double value) {
        this(value, "kg");
    }

    public Weight(double value, String unit) {
        this.value = value;
        this.unit = unit.toLowerCase();
    }

    /**
     * Zusammenzählen zweier Gewichtsobjekte. Nach der Addition entspricht die Einheit der
     * Summe der Einheit der linken Summanden.
     * <pre
     * Weight(1 kg)  + Weight(2 kg)  = Weight(3 kg)
     * Weight(1 g)   + Weight(2 g)   = Weight(3 g)
     * Weight(1 kg)  + Weight(100 g) = Weight(1.1 kg)
     * Weight(100 g) + Weight(1 kg)  = Weight(1100 g)
     * </pre>
     *
     * @param weight zu addierendes Gewicht
     * @return Addition der beiden Gewichte
     */
    public Weight add(Weight weight) {
        return new Weight(value + weight.value, unit);
    }
}
