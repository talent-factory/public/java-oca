/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _26class;

import lombok.Getter;

import java.util.Objects;

/**
 * Abbild eines Geld-Objektes.
 */
@Getter
public class Money {

    /**
     * Interner Geldwert in Abhängigkeit unserer aktuellen Währung.
     */
    private final double amount;

    /**
     * Währung unseres Geldobjektes.
     */
    private final Currency currency;

    /**
     * Instanzieren eines neuen Geld-Objektes.
     */
    public Money() {
        this(0.0);
    }

    /**
     * Instanzieren eines neuen Geld-Objektes.
     *
     * @param amount Wert
     */
    public Money(double amount) {
        this(amount, Currency.CHF);
    }

    /**
     * Instanzieren eines neuen Geld-Objektes.
     *
     * @param amount   Wert
     * @param currency Währung
     */
    public Money(double amount, Currency currency) {
        Objects.requireNonNull(currency, "Currency must be defined.");
        if (amount < 0)
            throw new MoneyException("Zero amount values not allowed.", this);
        this.amount = round(amount, 2);
        this.currency = currency;
    }


    /**
     * Addieren zweier Money-Objekte. Falls die beiden Money-Objekte unterschiedliche
     * Währungen ausweisen, dann normalisieren wir in jedem Falls auf CHF.
     *
     * @param other zu addierende Objekt
     * @return Resultat der Addition
     */
    public Money add(Money other) {
        if (currency != other.getCurrency())
            return new Money(
                    amount * currency.getCurrencyRate() +
                            other.amount * other.getCurrency().getCurrencyRate(),
                    Currency.CHF);

        return new Money(amount + other.getAmount(), currency);
    }

    public Money sub(Money other) {
        if (currency != other.getCurrency())
            return new Money(
                    amount * currency.getCurrencyRate() -
                            other.amount * other.getCurrency().getCurrencyRate(),
                    Currency.CHF);

        return new Money(amount - other.getAmount(), currency);
    }

    public double round(double value, int scale) {
        // return Double.parseDouble(String.format("%." + scale + "f", value));
        return Math.round(value * Math.pow(10, scale)) / Math.pow(10, scale);
    }
}
