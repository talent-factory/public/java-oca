package ch07;

import ch03.Employee;

import java.util.*;

public class ArrayExample {

    public static void main(String[] args) {

        Map<Integer, Employee> map = new HashMap<>();

        List<Employee> list = new ArrayList<>();

//        list.add(5711);
//        list.add("Daniel");
        list.add(new Employee("Senften", "Daniel", 5000));

        for (Object object: list)
            System.out.println(object);
    }
}
