package ch07;

import ch03.Employee;
import lombok.extern.java.Log;

import java.util.Set;
import java.util.TreeSet;

@Log
public class SetExample {

    public static void main(String[] args) {

        Set<Employee> employees = new TreeSet<>();

        employees.add(new Employee("Müller", "Daniel", 5_000));
        employees.add(new Employee("Müller", "Peter", 6_000));

        for (Employee employee : employees) {
            log.info(employee.toString());
        }

    }
}
