package migros;

public enum BeerType {

    ALE,
    AMBER,
    BRIGHT,
    DARK,
    STOUT,
    WHEAT
}
