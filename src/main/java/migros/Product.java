package migros;

import lombok.Data;
import lombok.NonNull;
import _26class.Money;

import java.util.Objects;

/**
 * Allgemeine Definition eines Produktes.
 */
@Data
@SuppressWarnings("all")
public abstract class Product {

    // Eigenschaften

    // Marke des Produktes
    protected Brand brand;

    // Aktueller Preis des Produktes
    protected Money price;


    // Konstruktor

    /**
     * Erstellen eines neuen Produktes, wobei dies immer nur in einer Unterklasse
     * möglich sein wird.
     *
     * @param brand Hersteller des Produktes
     * @param price aktueller Preis
     */
    public Product(Brand brand, Money price) {
        this.brand = brand;
        this.price = price;
    }

    // Methoden

    /**
     * Zwei Produkte sind dann identisch, wenn sich auch vom selen Hersteller
     * stammen. Der Preis ist in diesem Sinne irrelevant.
     *
     * @param other zu vergleichendes Produkt
     * @return TRUE, falls der Hersteller des Produktes identisch ist.
     */
    @Override
    public boolean equals(@NonNull Object other) {
        Product otherProduct = (Product) other;
        return brand.equals(otherProduct.getBrand());
    }

    @Override
    public int hashCode() {
        return Objects.hash(brand, price);
    }
}
