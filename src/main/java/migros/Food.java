package migros;

import lombok.Getter;
import _26class.Money;

import java.util.Date;

public abstract class Food extends Product {

    // Verfallsdatum des Produktes
    @Getter
    private final Date expiryDate;

    public Food(Brand brand, Money price, Date expiryDate) {
        super(brand, price);
        this.expiryDate = expiryDate;
    }
}
