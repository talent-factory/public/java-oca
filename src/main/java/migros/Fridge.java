package migros;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class Fridge {

    // Eigenschaften

    private final List<Food> contents = new ArrayList<>();

    @Getter
    @Setter
    private double temperature;


    // Methoden

    /**
     * Hinzufügen eines neuen Produktes.
     *
     * @param food Hinzuzufügendes Produkt.
     */
    public void add(Food food) {
        contents.add(food);
    }

    /**
     * Ist das gesuchte Produkt noch vorhanden im Kühlschrank?
     *
     * @param food Gesuchtes Produkt im Kühlschrank
     * @return TRUE, falls das Produkt vorhanden ist.
     */
    public boolean contains(Food food) {
        for (Food current : contents) {
            if (current.equals(food))
                return true;
        }
        return false;
    }

    /**
     * Zählt die Anzhal eines bestimmten Produktes im Kühlschrank
     *
     * @param food das gesuchte Produkt
     * @return Anzahl des gesuchten Produktes
     */
    public int count(Food food) {
        int count = 0;
        for (Food current : contents) {
            if (current.equals(food))
                count++;
        }
        return count;
    }
}
