package migros;

import lombok.Getter;
import lombok.Setter;
import _26class.Money;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@SuppressWarnings("all")
public class Beer extends Food {

    @Getter
    @Setter
    private int amount;

    private final BeerType beerType;

    /**
     * Ein helles Standardbier.
     *
     * @param brand Hersteller des Bieres
     * @param price aktueller Preis
     */
    public Beer(Brand brand, Money price) {
        this(brand, BeerType.BRIGHT, price);
    }

    /**
     * Dieser Konstruktor wird in in den Testfällen benötigt.
     * @param brand Hersteller
     */
    protected Beer(Brand brand) {
        this(brand, BeerType.BRIGHT, new Money());
    }

    public Beer(Brand brand, BeerType beerType, Money price) {
        super(brand, price, new Date());
        this.beerType = beerType;
    }

    /**
     * Zwei Bier sind dann identisch, wenn sowohl der Hersteller, als auch die
     * Menge identisch ist.
     *
     * @param other zu vergleichendes Produkt
     * @return TRUE, falls beide Biere identisch (Hersteller/Menge) sind
     */
    @Override
    public boolean equals(@NotNull Object other) {
        Beer otherBeer = (Beer) other;
        return amount == otherBeer.amount &&
                brand == otherBeer.brand &&
                beerType == otherBeer.beerType;
    }

}
