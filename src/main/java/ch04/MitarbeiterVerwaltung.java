package ch04;

import ch03.Employee;

@SuppressWarnings("ForLoopReplaceableByForEach")
public class MitarbeiterVerwaltung {

    public static void main(String[] args) {

        Employee[] personalliste = new Employee[4];

        personalliste[0] = new Employee("Marx", "Groucho", 8000);
        personalliste[1] = new Employee("Marx", "hico", 7000);
        personalliste[2] = new Employee("Marx", "Harpo", 7000);
        personalliste[3] = new Employee("Marx", "Zeppo", 7000);

        // alle Mitarbeiter ausgeben
        for (int i = 0; i < personalliste.length; i++)
            System.out.println(personalliste[i]);
    }
}
