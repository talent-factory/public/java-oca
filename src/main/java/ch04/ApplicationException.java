package ch04;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ApplicationException extends RuntimeException {

    private int errorNumber;
    private String message;

    public ApplicationException() {
    }

    public ApplicationException(String s) {
        super(s);
    }
}
