package ch04;

public class GeradeZahlen {

    public static void main(String[] args) {
        int i;

        for (i = 100; i != 0; i -= 2) {
            // von 100 bis 2 alle geraden Zahlen ausgeben
            System.out.println(" i hat den Wert " + i + "\n");
        }
    }
}
