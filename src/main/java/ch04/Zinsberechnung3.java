package ch04;

@SuppressWarnings("WeakerAccess")
public class Zinsberechnung3 {

    public static double zinsBerechnen(double startkapital, double zinssatz, double laufzeit) {
        return startkapital * Math.pow((1 + zinssatz / 100), laufzeit);
    }

    public static void main(String[] args) {

        for (int laufzeit = 1; laufzeit < 8; laufzeit++) {
            System.out.println(" Nach "
                    + laufzeit + ". Jahr: "
                    + (long) zinsBerechnen(15_000, 3.5, laufzeit)
                    + " EUR");
        }
    }
}
