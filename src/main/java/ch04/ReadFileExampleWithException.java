package ch04;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class ReadFileExampleWithException {

    public static void main(String[] args) {
        ReadFileExampleWithException example = new ReadFileExampleWithException();
        try {
            example.readFile("example.txt");
        } catch (ApplicationException e) {
            System.out.println("ErrorNo: "
                    + e.getErrorNumber()
                    + ", " + e.getMessage());
        }
    }

    public void readFile(String fileName) throws ApplicationException {
        try {
            System.out.println("Opening file " + fileName);
            InputStream in = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            System.out.println("Exception: " + e.getMessage());

            ApplicationException exception = new ApplicationException();
            exception.setErrorNumber(5);
            exception.setMessage("Meine Meldung");

            throw exception;
        }

        System.out.println("Done.");
    }
}
