package _09inheritance;

public interface Trainable {

    void attendTraining();
}
