package _09inheritance;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.java.Log;

@Log
@ToString(callSuper = true)
public abstract class Employee extends Person {

    @Getter
    private static int counter = 0;

    /*
     * Dieser statische Initializer-block wird nur 1x ausgeführt, nachdem die
     * Klasse geladen wurde.
     */
    static {
        log.info("Initializing Employee");
    }

    @Getter
    private final int employeeNumber;

    @Getter
    @Setter
    protected double salary;

    public Employee(String name, int employeeNumber) {
        setName(name);
        this.employeeNumber = employeeNumber;
        counter++;
        log.info("Employee #" + counter + " initialized: " + name);
    }

    /**
     * Der Mitarbeiter bekommt eine Lohnerhöhung, wobei die Implementations selbst
     * in den jeweiligen Subklassen implementiert wird. Mit dieser Funktion zeigen wir
     * eine wichtige Funktionalität:
     * <a href="https://de.wikipedia.org/wiki/Polymorphie_(Programmierung)">Polimorphie</a>.
     */
    public abstract void raiseSalary();
}
