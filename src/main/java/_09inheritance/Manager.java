package _09inheritance;

import lombok.ToString;

import java.util.Set;
import java.util.TreeSet;

@SuppressWarnings("all")
@ToString(callSuper = true)
public class Manager extends Employee implements Interviewer, Trainable {

    private final Set<Employee> employees = new TreeSet<>();

    public Manager(String name, int employeeNumber) {
        super(name, employeeNumber);
    }

    public void reportProjectStatus() {
    }

    @Override
    public void conductInterview() {
        System.out.println("Manager: conductInterview()");
    }

    @Override
    public void attendTraining() {
        System.out.println("Manager: attendTraining()");
    }

    @Override
    public String print() {
        return ("I am " + this);
    }

    @Override
    public void raiseSalary() {
        salary *= 1.5;
    }

    /**
     * Hinzufügen eines neuen Mitarbeiters.
     *
     * @param employee neuer Mitarbeiter
     */
    public void add(Employee employee) {
        employees.add(employee);
    }

}
