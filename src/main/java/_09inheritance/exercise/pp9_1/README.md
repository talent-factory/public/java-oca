# Programming Project

Write a class called `MonetaryCoin` that is derived from the
[Coin](../../../_05conditionals/Coin.java) class. Store an integer in the `MonetaryCoin` that represents 
its value and add a method that returns its value. Create a main driver 
class to instantiate and compute the sum of several `MonetaryCoin` objects. 
Demonstrate that a monetary coin inherits its parent’s ability to be flipped.
