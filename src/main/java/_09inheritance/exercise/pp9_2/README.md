# Programming Project

Design and implement a set of classes that define the employees of
a hospital: doctor, nurse, administrator, surgeon, receptionist, janitor,
and so on. Include methods in each class that are named according to the
services provided by that person and that print an appropriate message.
Create a main driver class to instantiate and exercise several of
the classes.
