package _09inheritance;

import lombok.Data;

@Data
public abstract class Person implements Comparable<Person> {

    private String name;
    private String address;
    private String phone;

    @Override
    public int compareTo(Person other) {
        return name.compareTo(other.name);
    }


}
