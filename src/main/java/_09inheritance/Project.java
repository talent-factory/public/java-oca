package _09inheritance;

import java.time.LocalDateTime;

public class Project {

    public static void main(String[] args) {

        Interviewer interviewer = new Manager("Daniel", 4711);

        // Die Methodes bookConferenceRoom() kann nur in einem
        // statischen Kontext verwendet werden.
        //
        // interviewer.bookConferenceRoom(LocalDateTime.now(), 2);

        Interviewer.bookConferenceRoom(LocalDateTime.now(), 2);
    }
}
