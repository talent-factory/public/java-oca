package _09inheritance;

@SuppressWarnings("all")
public class Programmer extends Employee {

    private String[] programmingLanguages;

    static {
        System.out.println("Initializing Programmer");
    }

    public Programmer(String name, int employeeNumber) {
        super(name, employeeNumber);
        System.out.println("Programmer instantziated");
    }

    /**
     * Der Mitarbeiter bekommt eine Lohnerhöhung, wobei die Implementations selbst
     * in den jeweiligen Subklassen implementiert wird. Mit dieser Funktion zeigen wir
     * eine wichtige Funktionalität:
     * <a href="https://de.wikipedia.org/wiki/Polymorphie_(Programmierung)">Polimorphie</a>.
     */
    @Override
    public void raiseSalary() {
        salary *= 1.2;
    }

    public Programmer(String name, String[] programmingLanguages, int employeeNumber) {
        super(name, employeeNumber);
        this.programmingLanguages = programmingLanguages;
        System.out.println("Programmer instantziated");
    }

    public void writeCode() {
    }


}
