package _09inheritance;

import java.time.LocalDateTime;

public interface Interviewer {

    double MIN_SAL = 9_999;

    static void bookConferenceRoom(LocalDateTime dateTime, int duration) {
        System.out.println("Interview scheduled on:   " + dateTime);
        System.out.println("Book conference room for: " + duration + " hrs");
    }

    void conductInterview();

    default void submitInterviewStatus() {
        System.out.println(this);
        System.out.println(this.MIN_SAL);
        System.out.println(this.print());
    }

    String print();
}
