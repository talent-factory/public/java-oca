package _09inheritance;

public class Example {

    public void raiseSalary(Employee employee) {
        System.out.println(employee);

        employee.raiseSalary();
        System.out.println(employee);
    }

    public static void main(String[] args) {

        Example example = new Example();

        Employee e1 = new Engineer("C", 1);
        Employee e2 = new Engineer("A", 2);
        Employee e3 = new Engineer("B", 3);
        Employee e4 = new Engineer("C", 3);

        Manager m = new Manager("Joe", 0);
        m.add(e1);
        m.add(e2);
        m.add(e3);
        m.add(e3);

        System.out.println(m);
    }
}
