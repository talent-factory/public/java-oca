package _09inheritance;

import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString(callSuper = true)
public class Engineer extends Employee {

    private final List<String> skills = new ArrayList<>();

    public Engineer(String name, int employeeNumber) {
        super(name, employeeNumber);
    }

    /**
     * Der Mitarbeiter bekommt eine Lohnerhöhung, wobei die Implementations selbst
     * in den jeweiligen Subklassen implementiert wird. Mit dieser Funktion zeigen wir
     * eine wichtige Funktionalität:
     * <a href="https://de.wikipedia.org/wiki/Polymorphie_(Programmierung)">Polimorphie</a>.
     */
    @Override
    public void raiseSalary() {
        salary *= 1.1;
    }

    public void add(String skill) {
        skills.add(skill);
    }

}
