package _09inheritance;

public class InstanceOf {

    public static void main(String[] args) {

        InstanceOf instance = new InstanceOf();
        Manager human = new Manager("Daniel", 4711);

        instance.test(human);
    }

    public void test(Employee employee) {

        if (employee instanceof Programmer) {
            ((Programmer) employee).writeCode();
        }

        if (employee instanceof Manager) {
            ((Manager) employee).conductInterview();
        }

    }

}
