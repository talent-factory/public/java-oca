package testing;

/**
 * Kleine Mathemathik Bibliothek.
 */
public class Math {

    /**
     * Liefert in jedem Fall einen gültigen Wert. Falls ein NULL Wert als
     * Parameter übergeben wird, dann wird der Wert '0' zurück geliefert,
     */
    private static Integer getValue(Integer value) {
        return (value == null) ? 0 : value;
    }

    /**
     * Addition zweier Zahlen. Falls ein Argument den Wert NULL beinhaltet, dann
     * wird '0' angenommen.
     *
     * @param a erste Zahl
     * @param b zweite Zahl
     * @return Summe der beiden Zahlen
     */
    public static Integer add(Integer a, Integer b) {
        return getValue(a) + getValue(b);
    }

    public static void printString() {
        System.out.print("abc");
    }

}
