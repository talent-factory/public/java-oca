package ch02;

/**
 * Dies ist die Dokumentation meiner Klasse...
 */
public class HalloWelt {

    /**
     * Dies ist die Beschreibung meiner Methode. Es soll dem
     * Anwender meiner Funktion aufzeigen, wie diese verwendet
     * werden kann oder was diese macht.
     *
     * @param args Argumente, die beim Aufruf übergeben werden
     */
    public static void main(String[] args) {
        System.out.println("Hallo  " + args[0]);
    }
}
