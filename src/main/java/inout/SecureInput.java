package inout;

public class SecureInput {

    static double getInteger(String prompt) {

        double result = 0;

        while (true) {
            System.out.println(prompt);

            // [ ... ]
            break;

        }

        return result;
    }

    public static void main(String[] args) {

        double a = getInteger("Bitte Länge a: ");
        double b = getInteger("Bitte Länge b: ");
        double c = getInteger("Bitte Länge c: ");

    }
}
