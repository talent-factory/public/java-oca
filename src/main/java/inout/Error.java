package inout;

import java.io.PrintStream;

public class Error {

    // The "standard" error output stream.
    static PrintStream error = System.err;

    public static void main(String[] args) {
        error.println("Error message");
    }
}
