package inout;

import lombok.extern.java.Log;

@SuppressWarnings("divzero")
@Log
public class Logging {

    static {
        log.info("Gestartet");
    }

    public static void main(String[] args) {

        int a = 5;
        int b = 0;

        // [ ... ]

        try {
            System.out.println("Result: " + (a / b));
        } catch (Exception ex) {
            log.severe(ex.getMessage());
        }
    }
}
