package inout;

import java.io.PrintStream;

public class Output {

    // The "standard" output stream.
    static PrintStream console = System.out;

    public static void main(String[] args) {
        console.println("Hello");
    }
}
