/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _98utils;

/**
 * Hilfsmethoden rund um die Klasse {@code String}.
 * <p></p>
 * Referenz: <a href="https://stackoverflow.com/questions/8154366/how-to-center-a-string-using-string-format">stackoverflow</a>
 */
public class StringUtils {

    /**
     * Zentrieren einer Zeichenkette.
     *
     * @param string zu zentrierende Zeichenkette
     * @param size zu verwendende Breite der resultierenden Zeichenkette
     * @return zentrierte Zeichenkette
     */
    public static String center(String string, int size) {
        return center(string, size, ' ');
    }

    /**
     * Zentrieren einer Zeichenkette.
     *
     * @param string zu zentrierende Zeichenkette
     * @param size zu verwendende Breite der resultierenden Zeichenkette
     * @param pad Füllzeichen
     * @return zentrierte Zeichenkette
     */
    public static String center(String string, int size, char pad) {

        if (string == null || size <= string.length())
            return string;

        StringBuilder sb = new StringBuilder(size);
        sb.append(String.valueOf(pad).repeat((size - string.length()) / 2));

        sb.append(string);
        while (sb.length() < size) {
            sb.append(pad);
        }

        return sb.toString();
    }
}
