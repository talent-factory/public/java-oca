package games.mastermind;

/**
 * Allgemeine Schnittstelle für das Spiel Mastermind
 */
public interface Interface {

    /**
     * Eingabeaufforderung für den Spieler.
     */
    void showPrompt();

    /**
     * Einlesen der Benutzer (Spieler) Eingabe. Die Eingabe wird solange
     * wiederholt, bis diese die Erwartungen (Anzahl Farbstifte und erwartete
     * Farben entspricht. Ein Abbruch bei der Eingabe ist nicht vorgesehen.
     *
     * @return Eingabe des Spielers
     */
    Code getUserInput();

    /**
     * Gibt das Resultat eines Spielversuches wieder.
     *
     * @param attempt aktueller Spielversuch
     */
    void showResult(Attempt attempt);

    /**
     * Das Spiel is zu Ende.
     */
    void done();

    /**
     * Ausgabe eines Fehlertextes.
     *
     * @param error Auszugebender Fehler
     */
    default void showErrorText(String error) {
        System.err.println(
                "Fehler bei Eingabe: " + error +
                        ".\nGeben Sie den Versuchscode erneut ein:");
    }

}
