package games.mastermind;

import java.util.Random;

/**
 * Generator zu Erstellung einer zufälligen Farbkombination
 * für das Spiel.
 */
public class CodeGenerator {

    private static final Random randomGenerator = new Random();

    /**
     * Statische Methode zur Erstellung einer zufälligen Farbkombination
     * für das Spiel.
     *
     * @return Farbkombination
     */
    public static Code generateRandomCode() {
        Pin[] pins = new Pin[Code.LENGTH];
        for (int i = 0; i < pins.length; i++)
            pins[i] = randomPin();
        return new Code(pins);
    }

    /**
     * Statische Methode zur Erstellung eines zufälligen Farbstiftes.
     *
     * @return Farbstift
     */
    private static Pin randomPin() {
        int colorNumber = randomGenerator.nextInt(Color.values().length);
        return new Pin(Color.values()[colorNumber]);
    }
}
