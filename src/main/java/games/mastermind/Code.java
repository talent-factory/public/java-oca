package games.mastermind;

/**
 * Allgemeine Spielkombination. Diese kann sowohl vom Spieler, als
 * auch vom Computer erstellt werden.
 */
public class Code {

    static final int LENGTH = 4;
    private final Pin[] pins;

    /**
     * Initislisieren einer neuen Spielkombination basierend auf der
     * übergebenen Liste (Array) von Farbstiften,
     *
     * @param pins Liste der Farbstifte
     */
    public Code(Pin[] pins) {
        this.pins = pins;
    }

    /**
     * Initialisieren einer neuen Spielkombination basierend auf der
     * übergebenen Zeichenkette.
     *
     * @param input Spielkombination in Form einer Zeichenkette
     */
    public Code(String input) {
        pins = new Pin[LENGTH];
        for (int i = 0; i < LENGTH; i++)
            pins[i] = new Pin(input.charAt(i));
    }

    /**
     * Zählt die Anzahl korrekter Farbstifte (Pin) an korrekter Position.
     *
     * @param otherCode zu überprüfende Farbkombination
     * @return Anzahl korrekter Farbstifte an korrekter Position
     */
    public int comparePins(Code otherCode) {
        int numberOfIdenticalPins = 0;
        for (int i = 0; i < LENGTH; i++)
            if (pins[i].equals(otherCode.pins[i]))
                numberOfIdenticalPins++;
        return numberOfIdenticalPins;
    }

    /**
     * Zählt die Anzahl korrekt eingegebener Farben.
     *
     * @param otherCode zu überprüfende Farbkombination
     * @return Anzahl korrekter Farben
     */
    public int compareColors(Code otherCode) {
        int numberOfIdenticalColors = 0;
        for (Color color : Color.values()) {
            int countThisColor = countColors(color);
            int countOtherColor = otherCode.countColors(color);
            numberOfIdenticalColors += Math.min(countThisColor, countOtherColor);
        }
        return numberOfIdenticalColors;
    }

    /**
     * Zählt, wie oft die angegebene Farbe bei allen vorhandenen Farbstiften
     * vorkommt.
     *
     * @param color zu überprüfende Farbe
     * @return Anzahl der gefundenen (Farb-) Übereinstimmungen
     */
    private int countColors(Color color) {
        int colorCounter = 0;
        for (Pin pin : pins)
            if (pin.hasColor(color))
                colorCounter++;
        return colorCounter;
    }

}
