package games.mastermind;

/**
 * Diese Klasse überprüft die Eingabe des Spielers auf ihre Gültigkeit.
 */
public class InputValidator {

    private final String input;
    private boolean isValid = true;
    private String errorText;

    /**
     * Initialisierung eines neuen Überprüfers.
     *
     * @param input zu überprüfende Zeichenkette
     */
    public InputValidator(String input) {
        this.input = input;
        testLength();
        if (isValid)
            testColorCodes();
    }

    /**
     * Überprüft die Länge der Eingabe.
     */
    private void testLength() {
        if (input.length() != Code.LENGTH) {
            isValid = false;
            errorText = "Ungültige Anzahl Stifte";
        }
    }

    /**
     * Überprüft alle Farben der Eingabe. Falls ein einzelner Stift
     * nicht in der Liste der möglichen Farben aufgeführt ist, dann
     * wird der Fehlertext ('errorText') entsprechend gesetzt.
     */
    private void testColorCodes() {
        for (char farbCode : input.toCharArray()) {
            testColorCode(farbCode);
            if (!isValid) {
                errorText = "Ungültige Farbe";
                break;
            }
        }
    }

    /**
     * Überprüft einen einzelnen Farbcode, ob dieser in der Liste der
     * möglichen Farben definiert ist. Falls dies nicht der Fall ist,
     * dann wird die Variable 'isValid' entsprechend gesetzt.
     *
     * @param colorCode zu überprüfenden Farbcode
     */
    private void testColorCode(char colorCode) {
        for (Color color : Color.values()) {
            if (color.isDefined(colorCode)) {
                isValid = true;
                break;
            } else {
                isValid = false;
            }
        }
    }

    /**
     * Ist die Eingabe korrekt?
     *
     * @return TRUE, falls die Eingabe korrekt ist
     */
    public boolean isValid() {
        return isValid;
    }

    /**
     * Liefert den Aktuellen Fehlertext, falls die Eingabe nicht
     * korrekt ist.
     *
     * @return aktueller Fehlertext
     */
    public String getErrorText() {
        return errorText;
    }
}
