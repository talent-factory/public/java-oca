package games.mastermind;

import org.jetbrains.annotations.NotNull;

/**
 * Repräsentiert einen Farbstift des Spieles.
 */
public class Pin {
    
    private final Color color;

    /**
     * Initialisierung eines Farbstiftes.
     *
     * @param color Farbe des Stiftes
     */
    public Pin(Color color) {
        this.color = color;
    }

    /**
     * Initialisierung eines Farbstiftes.
     *
     * @param colorCode Farbe des Stiftes
     */
    public Pin(char colorCode) {
        color = Color.getColor(colorCode);
    }

    /**
     * Überprüft, ob der aktuelle Farbstift der Farbe 'color'
     * entspricht.
     *
     * @param color zu prüfende Farbe
     * @return TRUE, falls die Farbe übereinstimmt
     */
    public boolean hasColor(Color color) {
        return this.color.equals(color);
    }

    /**
     * Überprüft, ob der aktuelle Farbstift mit dem Stift 'pin'
     * übereinstimmt.
     *
     * @param pin zu prüfender Farbstift
     * @return TRUE, falls die beiden Farbstifte identisch sind
     */
    public boolean equals(@NotNull Pin pin) {
        return pin.color.equals(color);
    }
}
