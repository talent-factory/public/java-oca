package games.mastermind;

import java.util.Scanner;

/**
 * Benutzerschnittstelle. Sämtliche I/O Funktionalitäten zwischen
 * dem Spieler und dem Computer finden in dieser Klasse statt.
 */
public class ConsoleInterface implements Interface {

    private final Scanner scanner = new Scanner(System.in);

    /**
     * Eingabeaufforderung für den Spieler.
     */
    @Override
    public void showPrompt() {
        System.out.println(
                "Geben Sie einen Versuchscode mit " + Code.LENGTH +
                        " Buchstaben aus der Menge {" + Color.colorList() + "} ein.");
        System.out.print("> ");
    }

    /**
     * Einlesen der Benutzer (Spieler) Eingabe. Die Eingabe wird solange
     * wiederholt, bis diese die Erwartungen (Anzahl Farbstifte und erwartete
     * Farben entspricht. Ein Abbruch bei der Eingabe ist nicht vorgesehen.
     *
     * @return Eingabe des Spielers
     */
    @Override
    public Code getUserInput() {
        do {
            String input = scanner.next();
            InputValidator validator = new InputValidator(input);
            if (validator.isValid()) {
                return new Code(input);
            } else
                showErrorText(validator.getErrorText());
        } while (true);
    }

    /**
     * Gibt das Resultat eines Spielversuches wieder.
     *
     * @param attempt aktueller Spielversuch
     */
    @Override
    public void showResult(Attempt attempt) {
        System.out.println("\nAuswertung " + Attempt.getNumberOfAttempts() + ". Versuch:");
        System.out.println("Anzahl korrekte Stifte: " + attempt.getNumberOfCorrectPins());
        System.out.println("Anzahl korrekte Farben: " + attempt.getNumberOfCorrectColors());
    }

    /**
     * Das Spiel is zu Ende.
     */
    @Override
    public void done() {
        System.out.println("Sie haben den Geheimcode erraten, " +
                "das Spiel ist beendet.");
    }
}
