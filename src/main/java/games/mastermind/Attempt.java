package games.mastermind;

/**
 * Spielversuch.
 */
public class Attempt {

    private final Code trialCode;
    private final Code secretCode;

    private static int numberOfAttempts = 0;
    private int numberOfCorrectPins;
    private int numberOfCorrectColors;

    /**
     * Konstruktor eines Spielversuches.
     *
     * @param trialCode  Farbkombination dieses Spielversuches
     * @param secretCode zu suchende Farbkombination
     */
    public Attempt(Code trialCode, Code secretCode) {
        numberOfAttempts++;
        this.trialCode = trialCode;
        this.secretCode = secretCode;
        numberOfCorrectPins();
        numberOfCorrectColors();
    }

    /**
     * Berechnet die Anzahl der korrekt eingegebenen Farbstifte. Richtige
     * Farbe UND richtige Position.
     */
    private void numberOfCorrectPins() {
        numberOfCorrectPins = trialCode.comparePins(secretCode);
    }

    /**
     * Liefert die Anzahl der korrekt eingegebenen Farben.
     */
    private void numberOfCorrectColors() {
        numberOfCorrectColors = trialCode.compareColors(secretCode);
        numberOfCorrectColors -= numberOfCorrectPins;
    }

    /**
     * Liefert die Anzahl korrekter Stifte
     *
     * @return Anzahl korrekter Stifte
     */
    public int getNumberOfCorrectPins() {
        return numberOfCorrectPins;
    }

    /**
     * Liefert die Anzahl korrekter Farben.
     *
     * @return Anzahl korrekter Farben
     */
    public int getNumberOfCorrectColors() {
        return numberOfCorrectColors;
    }

    /**
     * Liefert die Anzahl der Spielversuche.
     *
     * @return Anzahl Spielversuche
     */
    public static int getNumberOfAttempts() {
        return numberOfAttempts;
    }

    /**
     * Der aktuelle Spielversuch ist nicht korrekt.
     *
     * @return TRUE, falls der aktuelle Spielversuch nicht korrekt ist
     */
    public boolean isNotCorrect() {
        return numberOfCorrectPins < Code.LENGTH;
    }

}
