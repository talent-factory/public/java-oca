package games.mastermind;

/**
 * Liste der möglichen Farben.
 */
public enum Color {
    RED('r'),
    GREEN('g'),
    BLUE('b'),
    YELLOW('y'),
    WHITE('w'),
    BLACK('s');

    private final char colorCode;

    /**
     * Initialisierung eines neuen Farbcodes.
     *
     * @param colorCode neuer Farbcode
     */
    Color(char colorCode) {
        this.colorCode = colorCode;
    }

    /**
     * Liefert eine Liste der möglichen Farbcodes.
     *
     * @return Liste der möglichen Farbcode
     */
    public static String colorList() {
        StringBuilder colorCodes = new StringBuilder();
        for (Color color : values())
            colorCodes.append(color.colorCode).append(", ");
        String colorCodeString = colorCodes.toString();
        colorCodeString = colorCodeString.substring(0, colorCodeString.length() - 2);
        return colorCodeString;
    }

    /**
     * Liefert die (interne) Farbe des gesuchten 'colorCodes', falls
     * dieser definiert ist.
     *
     * @param colorCode gesuchter Farcode
     * @return interne Farbe, falls diese vorhanden ist; ansonsten NULL
     */
    public static Color getColor(char colorCode) {
        for (Color color : values())
            if (color.isDefined(colorCode))
                return color;
        return null;
    }

    /**
     * Liefert den Wert TRUE, falls die gesuchte Farbe definiert ist
     *
     * @param colorCode gesuchter Farbcode
     * @return TRUE, falls der Farbcode definiert ist
     */
    public boolean isDefined(char colorCode) {
        return this.colorCode == colorCode;
    }
}
