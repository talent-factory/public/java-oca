package games.mastermind;

public class Mastermind {

    public Code secretCode;
    public Interface userInterface = new ConsoleInterface();

    /**
     * Startfunktion unseres Spiels.
     *
     * @param args wird nicht verwendet
     */
    public static void main(String[] args) {
        Mastermind mastermind = new Mastermind();
        mastermind.play();
    }

    /**
     * Eigentlicher Spielablauf.
     */
    private void play() {
        secretCode = CodeGenerator.generateRandomCode();
        Attempt attempt;
        do {
            userInterface.showPrompt();
            Code code = userInterface.getUserInput();
            attempt = new Attempt(code, secretCode);
            userInterface.showResult(attempt);
        } while(attempt.isNotCorrect());
        userInterface.done();
    }
}
