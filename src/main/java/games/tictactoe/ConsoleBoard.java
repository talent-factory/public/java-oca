package games.tictactoe;

import static java.lang.System.out;

public class ConsoleBoard extends AbstractBoard {

    /**
     * Initiales, leeres Spielfeld.
     *
     * @param type
     */
    public ConsoleBoard(UIType type) {
        super(type);
    }

    /**
     * Zeichnet das aktuelle Spielfeld.
     */
    public void paint() {
        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                cells[row][col].paint();
                if (col < COLS - 1)
                    out.print("|");
            }
            out.println();
            if (row < ROWS - 1)
                out.println("-----------");
        }
        out.println();
    }

}
