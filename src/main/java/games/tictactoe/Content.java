package games.tictactoe;

/**
 * Aufzählung aller möglichen Inhalte, welche eine Zelle aufnehmen
 * kann.
 */
public enum Content {

    EMPTY,
    CROSS,
    CIRCLE
}
