package games.tictactoe;

import lombok.Data;

@Data
public abstract class AbstractCell {

    // Zelleninhalt
    protected Content content;

    /**
     * Erstellen einer neuen Zelle.
     */
    public AbstractCell() {
        clear();
    }

    /**
     * Löscht den Inhalt dieser Zelle
     */
    public void clear() {
        content = Content.EMPTY;
    }

    public abstract void paint();
}
