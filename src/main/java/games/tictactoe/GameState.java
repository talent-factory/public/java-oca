package games.tictactoe;

/**
 * Das Spiel ist immer in einem der folgenden Zustäne.
 */
public enum GameState {

    PLAYING,
    DRAW,
    CROSS_WON,
    CIRCLE_WON
}
