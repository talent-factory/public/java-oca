package games.tictactoe;

public enum UIType {
    Console,
    Swing,
    FX;
}
