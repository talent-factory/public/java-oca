package games.tictactoe;

import lombok.Data;

/**
 * Abbild des Spielfeldes.
 */
@Data
public abstract class AbstractBoard {

    static final int ROWS = 3;
    static final int COLS = 3;

    protected AbstractCell[][] cells;
    private int currentRow, currentCol;

    /**
     * Initiales, leeres Spielfeld.
     */
    public AbstractBoard(UIType type) {
        cells = new AbstractCell[ROWS][COLS];
        for (int row = 0; row < ROWS; row++)
            for (int col = 0; col < COLS; col++)
                cells[row][col] = (type == UIType.Console)
                        ? new ConsoleCell()
                        : new SwingCell();
    }

    public abstract void paint();

    /**
     * Initialisieren des Spielfeldes.
     */
    public void init() {
        for (int row = 0; row < ROWS; row++)
            for (int col = 0; col < COLS; col++)
                cells[row][col].clear();
    }

    public boolean hasWon(Content currentPlayer) {
        return ((cells[currentRow][0].getContent() == currentPlayer
                && cells[currentRow][1].getContent() == currentPlayer
                && cells[currentRow][2].getContent() == currentPlayer)

                ||
                (cells[0][currentCol].getContent() == currentPlayer
                        && cells[1][currentCol].getContent() == currentPlayer
                        && cells[2][currentCol].getContent() == currentPlayer)

                ||
                (currentRow == currentCol
                        && cells[0][0].getContent() == currentPlayer
                        && cells[1][1].getContent() == currentPlayer
                        && cells[2][2].getContent() == currentPlayer)

                ||
                (currentRow + currentCol == 2
                        && cells[0][2].getContent() == currentPlayer
                        && cells[1][1].getContent() == currentPlayer
                        && cells[2][0].getContent() == currentPlayer));
    }

    /**
     * Überprüft, ob das Spiel unentschieden ausgegangen ist. Dies ist dann
     * der Fall, wenn keine leere Zelle mehr vorhanden ist.
     *
     * @return TRUE, wenn das Spiel unentschieden ausgegangen ist
     */
    public boolean isDraw() {
        for (int row = 0; row < ROWS; ++row) {
            for (int col = 0; col < COLS; ++col) {
                if (cells[row][col].getContent() == Content.EMPTY)
                    return false;
            }
        }
        return true;
    }
}
