package games.tictactoe;

import javax.swing.*;

public class SwingBoard extends AbstractBoard {

    private JFrame fenster = new JFrame();

    /**
     * Initiales, leeres Spielfeld.
     *
     * @param type
     */
    public SwingBoard(UIType type) {
        super(type);

        fenster.setTitle("TicTacToe");
        fenster.setSize(300, 300);
    }

    @Override
    public void paint() {

        // TODO Rainer's Code
        fenster.setVisible(true);

    }
}
