package games.tictactoe;

public class Main {

    /**
     * Starten des Spieles.
     *
     * @param args wird nicht verwendet.
     */
    public static void main(String[] args) {
        new TicTacToe(UIType.Swing);
    }
}
