package games.tictactoe;

import java.util.Random;
import java.util.Scanner;

import static java.lang.System.out;

public class TicTacToe {

    private static final Scanner in = new Scanner(System.in);

    private final AbstractBoard board;      // Das Spielfeld
    private GameState currentState; // Aktueller Zustand des Spiels
    private Content currentPlayer;  // Aktueller Spieler

    /**
     * Kunstruktore, welcher das komplette Spiel initialisiert.
     */
    public TicTacToe(UIType type) {

        board = (type == UIType.Console)
                ? new ConsoleBoard(type)
                : new SwingBoard(type);
        initGame();

        do {
            playerMove(currentPlayer);
            board.paint();
            updateGameState(currentPlayer);

            switch (currentState) {
                case CROSS_WON -> out.println("'X' won! Bye!");
                case CIRCLE_WON -> out.println("'O' won! Bye!");
                case DRAW -> out.println("It's Draw! Bye!");
            }

            currentPlayer = (currentPlayer == Content.CROSS)
                    ? Content.CIRCLE
                    : Content.CROSS;

        } while (currentState == GameState.PLAYING);

    }

    private void updateGameState(Content currentPlayer) {
        if (board.hasWon(currentPlayer)) {  // check for win
            currentState = (currentPlayer == Content.CROSS)
                    ? GameState.CROSS_WON
                    : GameState.CIRCLE_WON;
        } else if (board.isDraw()) {
            currentState = GameState.DRAW;
        }
    }

    /**
     * Der aktuelle Spiele macht einen Spielzug.
     */
    private void playerMove(Content currentPlayer) {
        boolean validInput = false;  // for validating input
        do {
            if (currentPlayer == Content.CROSS) {
                System.out.print("Enter your move (row[1-3] column[1-3]): ");
            } else {
                computerMove();
                return;
            }
            int row = in.nextInt() - 1;
            int col = in.nextInt() - 1;
            if (isValid(row, col)) {
                board.getCells()[row][col].setContent(currentPlayer);
                board.setCurrentRow(row);
                board.setCurrentCol(col);
                validInput = true;
            } else {
                out.printf("This move at (%d,%d) is not valid. Try again...",
                        row + 1, col + 1);
            }
        } while (!validInput);
    }

    /**
     * Überprüft, ob die eingegebene Position (x,y) gültig ist
     *
     * @param row zu überprüfende Zeile
     * @param col zu überprüfende Spale
     * @return TRUE, falls diese Position gültig ist
     */
    private boolean isValid(int row, int col) {
        return row >= 0 && row < AbstractBoard.ROWS && col >= 0 && col < AbstractBoard.COLS
                && board.getCells()[row][col].getContent() == Content.EMPTY;
    }

    /**
     * Initialisiert das Spielfeld und setzt den aktuellen Zustand.
     */
    private void initGame() {
        board.init();
        currentPlayer = Content.CROSS;
        currentState = GameState.PLAYING;
    }

    /**
     * Der Computer wählt eine rein zufällige Position.
     */
    private void computerMove() {
        int row, col;
        do {
            Random move = new Random();
            row = move.nextInt(3);
            col = move.nextInt(3);
        } while (!isValid(row, col));

        board.getCells()[row][col].setContent(Content.CIRCLE);
        board.setCurrentRow(row);
        board.setCurrentCol(col);
    }
}
