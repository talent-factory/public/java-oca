package games.tictactoe;

import lombok.Data;

import static java.lang.System.out;

/**
 * Abbild einer einzelnen Zelle, wobei die Ausgabe auf der Konsole
 * sein wird.
 */
public class ConsoleCell extends AbstractCell {

    /**
     * Ausgabe der aktuellen Zelle.
     */
    public void paint() {
        switch (content) {
            case CROSS -> out.print(" X ");
            case CIRCLE -> out.print(" O ");
            case EMPTY -> out.print("   ");
        }
    }

}
