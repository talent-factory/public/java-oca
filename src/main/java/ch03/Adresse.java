package ch03;

import lombok.Data;

/**
 * Diese Klasse realisiert eine Adresse einer natuerlichen Person
 */
@Data
public class Adresse {
    public String name;
    public String strasse;
    public int hausnummer;
    public int postleitzahl;
    public String wohnort;
    public String mail;
    public String kommentar;
}

