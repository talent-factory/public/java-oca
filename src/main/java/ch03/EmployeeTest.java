package ch03;

public class EmployeeTest {

    public static void main(String[] args) {

        Employee billy = new Employee("Gates", "Billy", 3000);
        Employee stevie = new Employee("Jobs", "Steve", 3500);

        Employee peter = new Employee();
        peter.setFirstName("Peter");
        peter.setLastName("Müller");

        System.out.println("Peter's lastname: " + peter.getLastName());

        // Daten ausgeben
        System.out.println(billy);
        System.out.println(stevie);
        System.out.println(peter);

        // Gehalt von a erhöhen
        billy.increaseSalary(500);

    }
}
