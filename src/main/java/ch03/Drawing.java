package ch03;

public class Drawing {

    public static void main(String[] args) {

        int[] array = {
                6, 9, 47, 0
        };


        Figure[] figures = {
                new Circle(5),
                new Triangle(4, 5),
                new Circle(5),
        };

        // Loop
        for (Figure figure : figures) {
            figure.draw();
        }

    }
}
