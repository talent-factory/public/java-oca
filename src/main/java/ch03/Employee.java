package ch03;

import lombok.Data;

@SuppressWarnings("all")
@Data
public class Employee implements Comparable<Employee> {

    private String lastName = "<not yet defined>";
    private String firstName = "<not yet defined>";
    private String address;
    private int salary;

    public Employee() {
    }

    public Employee(String lastName, String firstName, int salary) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.salary = salary;
    }

    public void increaseSalary(int increase) {
        salary += increase;
    }

    @Override
    public int compareTo(Employee o) {
        return (this.equals(o)) ? 0 : -1;
    }
}
