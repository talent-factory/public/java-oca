package ch03;

public class Operatoren {

    public static void main(String[] args) {
        int x, y, z;
        int ergebnis_1, ergebnis_2;

        x = 1;
        y = 2;
        z = 3;

        ergebnis_1 = x + y * z;
        ergebnis_2 = (5 - 3) * z;

        System.out.println("1: " + ergebnis_1);
        System.out.println("2: " + ergebnis_2);

        x = x + z;
        System.out.println("3: " + x);
        x += z;
        System.out.println("4: " + x);
        x += 1;
        System.out.println("5: " + x);

        x++;
        System.out.println("6: " + x);

        System.out.println("7: " + ++x);

        z = x + ++x - x++;
        System.out.println("8: " + z);
        System.out.println("9: " + x);

        System.out.println("10: " + 11.0 % 10.0);
    }
}
