package ch03;

public abstract class Figure {

    /**
     * Startkoordinaten der Figur.
     */
    private double x, y;

    /**
     * Deklaration einer Zeichnungsmethode, welche später
     * durch meine spezialisierten Klassen implementiert
     * werden muss.
     */
    public abstract void draw();
}
