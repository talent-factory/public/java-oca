package ch03;

import java.awt.*;

/**
 * Abbild/Vorlage für ein Auto.
 */
public class Car {

    // Anzahl Sitze des Autos
    private int seats;
    // Farbe des Auto
    private Color color;

    /**
     * Erstellen eines Autos.
     *
     * @param seats Anzahl Sitze
     * @param color Farbe
     */
    public Car(int seats, Color color) {
        setSeats(seats);
        this.color = color;
    }

    /**
     * Erstellen eines Autos, wobei der Anzah der Sitze der
     * <i>Standard</i> Wert von vier angenommen wird.
     *
     * @param color Farbe
     */
    public Car(Color color) {
        this(4, color);
    }

    /**
     * Erstellen eines Autos, wobei für die Anzahl Size (4) und die
     * Farbe (Weiss) ein <i>Standard</i> Wert angenommen wird.
     */
    public Car() {
        this(4, Color.WHITE);
    }

    /**
     * Starten des Autos.
     */
    public void startEngine() {
        System.out.println("Engine started.");
    }


    /*
     * Die folgenden Methoden werden für das Lesen und Setzen der einzelnen
     * Eigenschaften verwendet. Es handelt sich dabei um die sogenannten
     * Setter/Setter Methoden.
     *
     * Wir können die Erstellung dieser Methoden durch das Kontextmenu (Generate...)
     * von IntelliJ generieren lassen.
     */
    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    /*
     * Mit dieser Methode können wir definieren, welche Daten ausgegeben werden
     * sollen bei der Verwendung von 'System.out.println()'.
     */
    @Override
    public String toString() {
        return "Car{" +
                "seats=" + seats +
                ", color=" + color +
                '}';
    }
}
