package ch03;

import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class Person {

    // Eigenschaften
    @NonNull
    private String firstName = "Daniel";
    @NonNull
    private String lastName = "Senften";
    private Date birthDate;

    public Person() {
    }

    public Person(@NonNull String firstName, @NonNull String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
