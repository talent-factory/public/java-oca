package ch03;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Abbild eines Dreeieckes.
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Triangle extends Figure {

    // Eigenschaften der Klasse

    /**
     * Seitenlängen des Dreieckes.
     */
    private double a, b, c;

    // Konstruktor(en)

    /**
     * Erstellen eines recktwinkligen Dreieckes mit den Seitenlängen a und b,
     *
     * @param a Seitenlänge 'a'
     * @param b Seitenlänge 'b'
     */
    public Triangle(double a, double b) {
        this.a = a;
        this.b = b;
        berechneHypothenus();
    }


// Methoden der Klasse

    public static void main(String[] args) {

        Triangle triangle = new Triangle(3, 7);

        {
            double result = triangle.berechneHypothenus();
            System.out.println(triangle);
        }

        {
            triangle.setA(4);
            triangle.setB(19);
            double result = triangle.berechneHypothenus();
            System.out.println(triangle);
        }

    }

    // ========

    public double berechneHypothenus() {
        c = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        return c;
    }

    @Override
    public void draw() {
        System.out.println(this);
    }
}
