package ch03;

@SuppressWarnings("all")
public class ArrayClass {

    public static void main(String[] args) {

        Employee[] arrayOfEmployees = {
                new Employee("Gates", "Billy", 3_500),
                new Employee("Jobs", "Steve", 3_500)
        };

        int i = 34;

        double[] value = {1, 2, 3};


        float[] b = new float[10];
        byte c[] = new byte[10];

        c[0] = 5;

        b[0] = 42;
        b[1] = 52;

        System.out.println("Done.");
    }

}
