package ch.aplu;

import ch.aplu.turtle.Turtle;

public class TurtleStar {

    public static void main(String[] args) {
        Turtle t = new Turtle();
        for (int n = 1; n < 50; n++) {
            t.forward(100);
            t.left(110);
        }
    }

}
