package ch.aplu;

import ch.aplu.util.GPanel;

/**
 * See https://aplu.ch/home/apluhomex.jsp?site=65
 */
class Chessboard extends GPanel {

    public Chessboard() {
        window(0, 8, 0, 8);
        for (int i = 0; i < 8; i++)
            for (int j = 0; j < 8; j++)
                if ((i + j) % 2 == 0)
                    fillRectangle(i, j, i + 1, j + 1);
    }

    public static void main(String[] args) {
        new Chessboard();
    }
}
