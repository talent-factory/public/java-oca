package aufgaben.a3_24;

import static edu.stdlib.IOTools.*;

/**
 * Schreiben Sie ein Programm, das mit Hilfe geschachtelter Schleifen ein
 * aus * -Zeichen zusammengesetztes Dreieck auf der Konsole ausgibt. Die
 * Benutzerin bzw. der Benutzer soll vorher nach der Anzahl der Zeilen gefragt
 * werden. Beispiel für den Programmablauf:
 * <p>
 * Anzahl der Zeilen: 5
 * *
 * **
 * ***
 * ****
 * *****
 */
public class Aufgabe {

    public static void main(String[] args) {

        int rows = readInt("Anzahl der Zeilen: ");

        // TODO Your code goes here
    }
} 
