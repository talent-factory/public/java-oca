package graphics;

public class Drawing {

    public static void main(String[] args) {

        Figure figure = new Rectangle(200, 250, 100, 150);
        Display drawing = new Display(figure);

        drawing.display();  // Redundant in this context
    }
}
