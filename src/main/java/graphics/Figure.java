package graphics;

import lombok.Data;

import java.awt.*;

@Data
public abstract class Figure {

    /**
     * Position der Figur(en) auf dem Display. Pro Zeichnungselement wird
     * jeweils ein Referenzpunkt angenommen.
     */
    protected Point point = new Point();

    /**
     * Das Objekt zeichnet sich selbst.
     *
     * @param graphics zu verwendendes Grafikobjekt für die Darstellung
     */
    abstract public void draw(Graphics graphics);

}
