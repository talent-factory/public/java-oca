package graphics;

import javax.swing.*;
import java.awt.*;

/**
 * Die Klasse Display stellt ein Fenster auf dem Bildschirm zur Verfügung, in welchem
 * Figur-Objekte dargestellt werden können. Man könnte auch sagen: Die ist unser
 * Editor, mit welchem wir unsere Grafikobjekte zeichnen.
 */
public class Display extends JFrame {

    /**
     * Die dargestellte Figur.
     */
    private final Figure figure;

    /**
     * Konstruktor. Initialisiert das Fenster in der Mitte des Bildschirms und erzeugt ein
     * JFrame-Objekt, auf welchem die Figur gezeichnet wird.
     */
    public Display(Figure figure) {

        this.figure = figure;

        Dimension windowSize = new Dimension(600, 600);
        setSize(windowSize);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int windowPositionX = (screenSize.width - windowSize.width) / 2;
        int windowPositionY = (screenSize.height - windowSize.height) / 2;
        Point windowPosition = new Point(windowPositionX, windowPositionY);
        setLocation(windowPosition);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        createAndAddDrawingPanel();

        setTitle("Zeichenbrett");
        setResizable(false);
        setVisible(true);
    }

    /**
     * Erstellt und fügt unser 'Zeichenbrett' hinzu.
     */
    private void createAndAddDrawingPanel() {
        add(new JPanel() {
            // Die paintComponent()-Methode wird automatisch aufgerufen, wenn
            // irgendwer die repaint()-Methode beim display aufruft.
            @Override
            protected void paintComponent(Graphics graphics) {
                super.paintComponent(graphics);
                draw(graphics);
            }
        });
    }

    /**
     * Zeichnet alle Figuren.
     *
     * @param graphics Referenz auf das Graphics-Objekt zum zeichnen.
     */
    private void draw(Graphics graphics) {
        figure.draw(graphics);
    }

    /**
     * Zeichnet alle Figuren auf dem Display.
     */
    public void display() {
        repaint();
    }

}
