package graphics;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.awt.*;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class Rectangle extends Figure {

    /**
     * Länge und Breite des Rechteckes.
     */
    private final int height, width;

    /**
     * Erstellen eines neuen Rechteckes an der Position [0,0].
     *
     * @param height Länge des Rechteckes
     * @param width  Breite des Rechteckes
     */
    public Rectangle(int height, int width) {
        this(new Point(), height, width);
    }

    /**
     * Erstellen eines neuen Rechteckes. Die Position [x, y] entspricht
     * der oberen, linken Ecke des Rechteckes.
     *
     * @param point  Position des Rechteckes auf dem Display
     * @param height Länge des Rechteckes
     * @param width  Breite des Rechteckes
     */
    public Rectangle(Point point, int height, int width) {
        this.point.x = point.x;
        this.point.y = point.y;
        this.height = height;
        this.width = width;
    }

    /**
     * Erstellen eines neuen Rechteckes. Die Position [x, y] entspricht
     * der oberen, linken Ecke des Rechteckes.
     *
     * @param x      Position des Rechteckes auf dem Display
     * @param y      Position des Rechteckes auf dem Display
     * @param height Länge des Rechteckes
     * @param width  Breite des Rechteckes
     */
    public Rectangle(int x, int y, int height, int width) {
        this(new Point(x, y), height, width);
    }

    @Override
    public void draw(Graphics graphics) {
        graphics.drawRect(
                point.x,
                point.y,
                width, height);
    }

}
