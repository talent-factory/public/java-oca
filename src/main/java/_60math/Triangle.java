/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _60math;

import edu.princeton.cs.stdlib.StdOut;
import org.apache.commons.cli.*;

import java.util.HashMap;

/**
 * Ein Dreieck lässt sich durch drei Punkte in der Ebene definieren, die nicht
 * auf einer Geraden liegen. Diese Punkte werden Ecken des Dreiecks genannt.
 * Die Verbindungsstrecken zwischen jeweils zwei Ecken sind die Seiten des
 * Dreiecks. Das Dreieck begrenzt in der Ebene damit eine Fläche.
 * <p>
 * <img src="https://www.smart-rechner.de/assets/pics/dreieck.png"></img>
 *
 * <p>
 * In unserem Beispiel verwenden wir den Punkt A, B oder C innerhalb eines
 * Koordinatennetzes nicht. Unsere Verwendung an dieser Stelle ist der
 * jeweilige Innenwinkel α (<em>Alpha</em>), β (<em>Beta</em>) und
 * γ (<em>Gamma</em>).
 */
public final class Triangle {

    /**
     * Die möglichen Optionen, welche unser Programm zulässt, werden an dieser
     * Stelle statisch initialisiert, damit diese auch unseren Testfällen zur
     * Verfügung stehen.
     */
    static final Options options = new Options();

    static {
        options
                .addOption("a", true, "Seitenlänge a")
                .addOption("b", true, "Seitenlänge b")
                .addOption("c", true, "Seitenlänge c")
                .addOption("A", true, "Winkel α (Alpha)")
                .addOption("B", true, "Winkel β (Beta)")
                .addOption("C", true, "Winkel γ (Gamma)");
    }

    /**
     * In dieser Tabelle speichern wir alle eingegebenen und berechneten Werte
     * des Dreieckes ab. Der Schlüssel (<em>Key</em>) in diese Tabelle entspricht:
     * <ul>
     *     <li>a → Seitenlänge a</li>
     *     <li>b → Seitenlänge b</li>
     *     <li>c → Seitenlänge c</li>
     *     <li>A → Winkel α</li>
     *     <li>B → Winkel β</li>
     *     <li>C → Winkel γ</li>
     * </ul>
     */
    private final HashMap<Character, Double> values = new HashMap<>();

    /**
     * Geprüfte Werte der Eingabezeile.
     */
    private CommandLine cmd;

    /**
     * Startmethode zur Berechnung des Dreiecks.
     *
     * @param args Parameterliste gemäss Beschreibung {@link #values}
     */
    public static void main(String[] args) {

        // Mit diesem Objekt lesen und verarbeiten wir die Kommandozeile
        CommandLineParser parser = new DefaultParser();

        Triangle triangle = new Triangle();
        try {
            triangle.cmd = parser.parse(options, args);
            triangle.assignArguments();
        } catch (ParseException e) {
            StdOut.printf(e.getLocalizedMessage() + "\n\n");
        }

        if (!triangle.isDefined()) {
            HelpFormatter formatter = new HelpFormatter();
            StdOut.println("""
                    Mit diesem Programm werden alle Berechnungen am Dreieck durchgeführt.
                    Um ein Dreieck eindeutig berechnen zu können müssen drei Werte in
                    beliebiger Reihenfolge eingegeben werden.
                                        
                    Falls diese Bedingung nicht erfüllt ist oder ungültige Werte eingegeben
                    werden, dann findet keine Berechnung statt.
                    """);
            formatter.printHelp("java " + Triangle.class.getName() + " [options]", options);
        }

        // TODO Berechnung des Dreiecks
    }

    private void assignArguments() {
        if (cmd.hasOption('a')) setValueOf('a');
        if (cmd.hasOption('b')) setValueOf('b');
        if (cmd.hasOption('c')) setValueOf('c');
        if (cmd.hasOption('A')) setValueOf('A');
        if (cmd.hasOption('B')) setValueOf('B');
        if (cmd.hasOption('C')) setValueOf('C');
    }

    /**
     * Setzte den Wert eines Argumentes der Eingabezeile. Falls der eingegebene
     * Buchstaben einen gültigen Wert besitzt, dann wird dieser Wert in der
     * vorgesehenen Datenstruktur gespeichert. Im Falle einer Falscheingabe
     * wird diese ignoriert.
     *
     * @param c zu prüfendes Zeichen
     */
    private void setValueOf(final char c) {
        try {
            double value = Double.parseDouble(cmd.getOptionValue(c));
            values.put(c, value);
        } catch (NumberFormatException ignore) {
            // Intentionally left blank
        }
    }

    /**
     * Diese Funktion liefert den Wert {@code true}, wenn das Dreieck eindeutig
     * definiert ist. Dies ist dann der Fall, wenn von den sechs möglichen
     * Parameter (Seiten oder Winkel) deren drei definiert sind.
     *
     * @return {@code true}, wenn das Dreieck eindeutig definiert ist
     */
    private boolean isDefined() {
        return values.size() == 3;
    }
}
