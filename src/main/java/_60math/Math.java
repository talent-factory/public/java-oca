/*
 * Released under MIT License
 *
 * Copyright (©) 2022 Daniel Senften
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to
 * do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS IN THE SOFTWARE.
 */

package _60math;

/**
 * Diese Klasse stellt verschiedene statische Methoden zur Berechnung von ... zur Verfügung.
 */
public class Math {

    /**
     * Utility Klassen werden nicht instanziiert.
     */
    private Math() {
    }

    /**
     * Berechnen der Fakultät von {@code n}.
     *
     * @param n zu berechnende Fakultät
     * @return Fakultät von {@code n}
     * @see <a href="https://de.wikipedia.org/wiki/Fakult%C3%A4t_(Mathematik)">Wikipedia</a>
     */
    public static long factorial(int n) {
        if (n < 2) return 1;
        return n * factorial(n - 1);
    }

    /**
     * Berechnen des Binominalkoeffizienten. Er gibt an, auf wie viele verschiedene Arten man aus einer Menge
     * von {@code n} verschiedenen Objekten jeweils {@code k} Objekte auswählen kann. Der Binomialkoeffizient
     * ist also die Anzahl der {@code k}-elementigen Teilmengen in der Potenzmenge einer {@code n}-elementigen
     * Grundmenge.
     *
     * @param n Anzahl verschiedener Objekte aus der Teilmenge {@code k}
     * @param k Anzahl Elemente der Teilmenge
     * @return Berechneter Binominalkoeffizient
     * @see <a href="https://de.wikipedia.org/wiki/Binomialkoeffizient">Wikipedia</a>
     */
    public static long binominal(int n, int k) {
        if (n > 20 || k > 20)
            throw new IllegalArgumentException("Der Wertebereich ist überschritten.");
        if ((n == k) || (k == 0))
            return 1;
        else
            return factorial(n) / (factorial(k) * factorial(n - k));
    }

    /**
     * Berechnet die Anzahl Zeichen des übergebenen Wertes, wobei wir dessen absoluten
     * Betrag berücksichtigen.
     *
     * <p>Es gilt somit folgende Definition:</p>
     * <pre>
     *     numberOfDigits(-10) → 2
     *     numberOfDigits(  0) → 1
     *     numberOfDigits(123) → 3
     * </pre>
     *
     * @param value zu berechnender Wert
     * @return Anzahl Zeichen
     */
    public static int numberOfDigits(long value) {

        int digits = 1;
        value = java.lang.Math.abs(value);

        while (value > 10) {
            digits++;
            value /= 10;
        }
        return digits;
    }
}
