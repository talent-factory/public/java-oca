package book.dirk_louis.ch03;

public class ErstesBeispiel {
    public static void main(String[] args) {
        int ersteZahl;                           // Deklaration
        int zweiteZahl;
        int ergebnis;

        ersteZahl = 8754;                        // Initialisierung
        zweiteZahl = 398;

        ergebnis = ersteZahl - zweiteZahl;       // Verwendung
        System.out.println(" 8754 - 398  = " + ergebnis);
    }
}
