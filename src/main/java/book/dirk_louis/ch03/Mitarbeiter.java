package book.dirk_louis.ch03;

import lombok.Data;

@SuppressWarnings({"SameParameterValue", "unused"})
@Data
public class Mitarbeiter {

    private static int counter;

    private int id; // Mitarbeiternummer
    private String lastName;
    private String firstName;
    protected int salary;

    public Mitarbeiter(String lastName, String firstName,
                       int salary) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.salary = salary;
        this.id = ++counter;
    }

    public Mitarbeiter(String lastName, String firstName) {
        this(lastName, firstName, 5000);
    }

    public void datenAusgeben() {
        System.out.println(toString());
    }

    public void increaseSalary(int increase) {
        salary += increase;
    }

}
