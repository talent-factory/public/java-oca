package book.dirk_louis.ch13;

import javax.swing.*;
import java.awt.*;

/**
 * Aufgabe 13.3
 */
public class NotenEingabe extends JFrame {
    Container c;

    public NotenEingabe() {
        c = getContentPane();
        c.setLayout(new GridLayout(5, 1));
        c.add(new JCheckBox("sehr gut"));
        c.add(new JCheckBox("gut"));
        c.add(new JCheckBox("befriedigend"));
        c.add(new JCheckBox("ausreichend", true));
        c.add(new JCheckBox("ungenuegend"));
    }
}
