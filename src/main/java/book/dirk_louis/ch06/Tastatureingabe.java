package book.dirk_louis.ch06;

import java.io.PrintStream;
import java.util.Scanner;

public class Tastatureingabe {

    public static void main(String[] args) {

        PrintStream out = System.out;  // Console cons = System.console();

        Scanner in = new Scanner(System.in);

        try {
            out.print(" Geben Sie Ihren Nachnamen ein: ");
            String name = in.nextLine();

            out.print(" Geben Sie Ihr Alter ein      : ");
            int alter = in.nextInt();

            out.print("\n");
            out.printf(" %d Jahre?", alter);
            out.printf(" Hast du auch nicht geflunkert, %s?\n", name);
        } catch (Exception e) {
            System.err.println("\n Fehler! Zahleneingabe kann nicht eingelesen werden.");
        }
    }
}
