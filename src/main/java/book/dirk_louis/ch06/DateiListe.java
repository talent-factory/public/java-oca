package book.dirk_louis.ch06;

import java.io.File;

@SuppressWarnings("ThrowablePrintedToSystemOut")
public class DateiListe {

    public static void main(String[] args) {

        if (args.length != 1) {
            System.out.println(" Verzeichnis angeben!");
            System.exit(0);
        }

        try {
            File wurzel = new File(args[0]);

            if (wurzel.isDirectory()) {
                File[] inhalt = wurzel.listFiles();

                assert inhalt != null;
                for (File file : inhalt) {
                    System.out.println(file.getCanonicalPath());
                }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
