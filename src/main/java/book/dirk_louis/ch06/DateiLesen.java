package book.dirk_louis.ch06;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DateiLesen {

    public static void main(String[] args) throws IOException {

        String directory = "dirk-louis/src/main/resources/";

        try (FileReader eingabestream =
                     new FileReader(directory + "john_maynard.txt")) {

            StringBuilder text = new StringBuilder(10);
            int gelesen = 0;

            // lese Zeichen, bis Dateiende erreicht ist
            while (gelesen != -1) {
                gelesen = eingabestream.read();
                text.append((char) gelesen);
            }
            System.out.print(text);
        } catch (FileNotFoundException ignored) {
            System.err.println("File not found");
        }

    }
}
