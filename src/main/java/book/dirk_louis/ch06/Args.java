package book.dirk_louis.ch06;

public class Args {

    public static void main(String[] args) {

        for (String arg : args) {
            System.out.printf(" %s\n", arg);
        }
    }
}
