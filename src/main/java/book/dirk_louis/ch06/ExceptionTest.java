package book.dirk_louis.ch06;

public class ExceptionTest {

    public static void main(String[] args) {

        try {
            double pi = Double.parseDouble(args[0]);
            System.out.println("PI: " + pi);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Usage:");
            System.out.println("\tprogram argument1 argument2...");
        } catch (NumberFormatException e) {
            System.out.println("Bitte eine Zahl eingeben:");
        }
    }
}
