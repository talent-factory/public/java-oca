package book.dirk_louis.ch06;

import java.io.PrintStream;

/**
 * Beispiel auf Seite 116
 */
public class ConsoleExample {

    public static void main(String[] args) {

        PrintStream console = System.out;
        console.print("Liebe Grüsse aus Bern.\n");
    }
}
