package book.dirk_louis.ch06;

public class StringExample {

    public static void main(String[] args) {

        var x = 5;


        int s = 5, t = s;

        System.out.println("s: " + s + ", t: " + t);

        t = 6;
        System.out.println("s: " + s + ", t: " + t);

        t = s;
        System.out.println("s: " + s + ", t: " + t);
    }
}
