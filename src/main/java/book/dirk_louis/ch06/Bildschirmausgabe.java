package book.dirk_louis.ch06;

public class Bildschirmausgabe {

    public static void main(String[] args) {

        System.out.print("\n");

        String ware = "Heft";
        double preis = 1234.75643;
        System.out.printf(" 1 '%10s' kostet %7.2f Euro \n", ware, preis);

        ware = "Füller";
        preis = 0.55;
        System.out.printf(" 1 '%10s' kostet %7.2f Euro \n", ware, preis);
    }
}
