package book.dirk_louis.ch09;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

public class Malprogramm_2D extends JFrame {
  Leinwand paintArea;          // Hier wird gezeichnet
  ButtonGroup m_formauswahl;        // liefert die zur Zeit ausgew�hlte Form
  int m_Xpos, m_Ypos;                 // aktuelle Mausposition

  // Im Konstruktor wird eine anvas-Malfl�che angelegt sowie
  // eine ButtonGroup-Gruppe mit
  // zur Auswahl stehenden Zeichenformen
  Malprogramm_2D(String titel) {
    super(titel);

    // Einen Layout-Manager anlegen
    setLayout(new FlowLayout());

    // Die Malfl�che anlegen
    paintArea = new Leinwand();
    add(paintArea);

    // Panel-Container f�r Schaltfl�chen anlegen
    Panel panel = new Panel();
    // Gitter mit 3 Zeilen, 1 Spalte
    panel.setLayout(new GridLayout(3, 1, 20, 20));

    // Optionsfelder zur Auswahl der Formen anlegen
    m_formauswahl = new ButtonGroup();

    // 1. Optionsfelder erzeugen
    JRadioButton opt1 = new JRadioButton("Kreis", false);
    JRadioButton opt2 = new JRadioButton("Scheibe", false);
    JRadioButton opt3 = new JRadioButton("Rechteck", false);

    // 2. Befehlsnamen f�r Optionsfelder festlegen
    opt1.setActionCommand("Kreis");
    opt2.setActionCommand("Scheibe");
    opt3.setActionCommand("Rechteck");

    // 3. Optionsfelder in ButtonGroup-Gruppe aufnehmen
    m_formauswahl.add(opt1);
    m_formauswahl.add(opt2);
    m_formauswahl.add(opt3);

    // 4. Optionsfelder in Panel aufnehmen
    panel.add(opt1);
    panel.add(opt2);
    panel.add(opt3);

    add(panel);

    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
  }

  // In main wird eine Instanz der Klasse angelegt und auf den Bildschirm
  // gebracht
  public static void main(String[] args) {
    Malprogramm_2D fenster = new Malprogramm_2D("Malen mit Java2D");
    fenster.pack();
    fenster.setSize(500, 350);
    fenster.setResizable(false);
    fenster.setVisible(true);
  }

  class Leinwand extends JPanel {
    // der Konstruktor implementiert die Mausbehandlung und setzt
    // Hintergrund und Vordergrund (=Zeichen)farbe
    Leinwand() {
      addMouseListener(new MeinMausAdapter());

      setBackground(Color.black);
      setForeground(Color.orange);
    }

    // Die wichtigste Methode: hier wird gezeichnet!
    public void paintComponent(Graphics g) {
      super.paintComponent(g);

      String label;
      ButtonModel aktuell = null;

      Graphics2D g_2d = (Graphics2D) g;

      // welche Form ist gerade ausgew�hlt?
      aktuell = m_formauswahl.getSelection();

      // entsprechend handeln
      if (aktuell == null)
        return;

      int w = (int) (Math.random() * 300);
      int h = (int) (Math.random() * 300);
      label = aktuell.getActionCommand();

      // die Attribute setzen
      BasicStroke strich = new BasicStroke(2.0f, BasicStroke.CAP_BUTT,
              BasicStroke.JOIN_BEVEL);
      GradientPaint fuell = new GradientPaint(0.0f, 0.0f, Color.red,
              300.0f, 300.0f, Color.yellow, true);
      g_2d.setStroke(strich);
      g_2d.setPaint(fuell);

      if (label.equals("Kreis")) {
        Ellipse2D.Float oval = new Ellipse2D.Float((float) m_Xpos, (float) m_Ypos,
                (float) w, (float) w);
        g_2d.draw(oval);
      }

      if (label.equals("Scheibe")) {
        Ellipse2D.Float oval = new Ellipse2D.Float((float) m_Xpos, (float) m_Ypos,
                (float) w, (float) w);
        g_2d.fill(oval);
      }

      if (label.equals("Rechteck")) {
        Rectangle2D.Float rechteck = new Rectangle2D.Float((float) m_Xpos, (float) m_Ypos,
                (float) w, (float) h);
        g_2d.draw(rechteck);
      }
    }

    // Diese Methode liefert die minimale Gr��e der anvas
    public Dimension getMinimumSize() {
      return new Dimension(300, 300);
    }

    // Die Lieblingsgr��e setzen wir auf die Minimalgr��e
    public Dimension getPreferredSize() {
      return getMinimumSize();
    }

    // Die Adapter-Klasse f�r die Mausklicks  als innere Klasse
    // von Leinwand
    class MeinMausAdapter extends MouseAdapter {
      public void mousePressed(MouseEvent e) {
        // Die aktuelle Position der Maus merken
        m_Xpos = e.getX();
        m_Ypos = e.getY();

        // Malfl�che aktualisieren
        repaint();
      }
    }
  }

}  
