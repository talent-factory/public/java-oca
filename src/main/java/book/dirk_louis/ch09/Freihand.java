package book.dirk_louis.ch09;// Freihandlinien

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.Stack;

public class Freihand extends JFrame {

    Leinwand paintArea;        // Hier wird gezeichnet
    ButtonGroup buttonGroup;   // liefert die zur Zeit ausgewählte Form
    int yPosition, xPosition;

    // Positionen für das Dreieck
    Stack<Integer> xPositions = new Stack<>();
    Stack<Integer> yPositions = new Stack<>();

    // Im Konstruktor wird eine Malfläche angelegt sowie
    // eine ButtonGroup-Gruppe mit
    // zur Auswahl stehenden Zeichenformen
    Freihand(String titel) {
        super(titel);

        // Einen Layout-Manager anlegen
        setLayout(new FlowLayout());

        // Die Malfläche anlegen
        paintArea = new Leinwand();
        add(paintArea);

        // Panel-Container für Schaltflächen anlegen
        JPanel panel = new JPanel();
        // Gitter mit 3 Zeilen, 1 Spalte
        panel.setLayout(new GridLayout(5, 1, 20, 20));

        // Optionsfelder zur Auswahl der Formen anlegen
        buttonGroup = new ButtonGroup();

        // 1. Optionsfelder erzeugen
        JRadioButton opt1 = new JRadioButton("Kreis", false);
        JRadioButton opt2 = new JRadioButton("Scheibe", false);
        JRadioButton opt3 = new JRadioButton("Rechteck", false);
        JRadioButton opt4 = new JRadioButton("Freihand", false);
        JRadioButton opt5 = new JRadioButton("Dreieck", false);

        // 2. Befehlsnamen für Optionsfelder festlegen
        opt1.setActionCommand("Kreis");
        opt2.setActionCommand("Scheibe");
        opt3.setActionCommand("Rechteck");
        opt4.setActionCommand("Freihand");
        opt5.setActionCommand("Dreieck");
        
        // 3. Optionsfelder in ButtonGroup-Gruppe aufnehmen
        buttonGroup.add(opt1);
        buttonGroup.add(opt2);
        buttonGroup.add(opt3);
        buttonGroup.add(opt4);
        buttonGroup.add(opt5);

        // 4. Optionsfelder in Panel aufnehmen
        panel.add(opt1);
        panel.add(opt2);
        panel.add(opt3);
        panel.add(opt4);
        panel.add(opt5);

        add(panel);

        // Fenster schliessen soll das ganze Programm beenden
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    // In main wird eine Instanz der Klasse angelegt und auf den Bildschirm
    // gebracht
    public static void main(String[] args) {
        Freihand fenster = new Freihand("Freihandlinien");
        fenster.pack();
        fenster.setSize(500, 350);
        fenster.setResizable(false);
        fenster.setVisible(true);
    }

    @SuppressWarnings("DuplicatedCode")
    class Leinwand extends JPanel {

        // der Konstruktor implementiert die Mausbehandlung und setzt
        // Hintergrund und Vordergrund (=Zeichen)farbe
        Leinwand() {
            addMouseListener(new MeinMausAdapter());
            addMouseMotionListener(new MeinMausMotionAdapter());

            setBackground(Color.black);
            setForeground(Color.orange);
        }

        // Die wichtigste Methode: hier wird gezeichnet!
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            String label;
            ButtonModel aktuell = null;

            // welche Form ist gerade ausgewählt?
            aktuell = buttonGroup.getSelection();

            // entsprechend handeln
            if (aktuell == null)
                return;

            int w = (int) (Math.random() * 300);
            int h = (int) (Math.random() * 300);

            label = aktuell.getActionCommand();

            if (label.equals("Kreis")) {
                g.drawOval(xPosition, yPosition, w, h);
            }

            if (label.equals("Scheibe")) {
                g.fillOval(xPosition, yPosition, w, h);
            }

            if (label.equals("Rechteck")) {
                g.drawRect(xPosition, yPosition, w, h);
            }

            if (label.equals("Dreieck")) {
                xPositions.push(xPosition);
                yPositions.push(yPosition);
                if (xPositions.size() == 3) {
                    int[] x = {xPositions.pop(), xPositions.pop(), xPositions.pop()};
                    int[] y = {yPositions.pop(), yPositions.pop(), yPositions.pop()};
                    g.drawPolygon(x, y, 3);
                }
            }
        }

        // Diese Methode liefert die minimale Grösse
        public Dimension getMinimumSize() {
            return new Dimension(300, 300);
        }

        // Die Lieblingsgrösse setzen wir auf die Minimalgrösse
        public Dimension getPreferredSize() {
            return getMinimumSize();
        }

        // Die Adapter-Klasse für die Mausklicks  als innere Klasse
        // von Leinwand
        class MeinMausAdapter extends MouseAdapter {
            public void mousePressed(MouseEvent e) {
                // Die aktuelle Position der Maus merken
                xPosition = e.getX();
                yPosition = e.getY();

                repaint(); // Malfläche aktualisieren
            }
        }

        class MeinMausMotionAdapter extends MouseMotionAdapter {

            public void mouseDragged(MouseEvent e) {
                ButtonModel aktuell;
                String label;

                // Herausfinden, welche Box gerade aktiviert ist
                aktuell = buttonGroup.getSelection();

                // Da nach dem Programmstart keine Box aktiviert ist,
                // muss dies getestet werden. Dann wird kein Objekt
                // zurückgeben, sondern ein null-Wert
                if (aktuell == null)
                    return;

                label = aktuell.getActionCommand();

                // Nur wenn die Freihandfunktion ausgewählt ist, die
                // Mausposition merken und neuzeichnen
                if (label.equals("Freihand")) {
                    Graphics tmp = paintArea.getGraphics();
                    xPosition = e.getX();
                    yPosition = e.getY();
                    tmp.setColor(Color.orange);
                    tmp.drawOval(xPosition, yPosition, 2, 2);
                    tmp.dispose();
                }
            }
        }
    }  // Ende von Leinwand

} // Ende von Freihand
