package book.dirk_louis.ch09;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// Hauptfenster von Swing-Klasse JFrame ableiten
public class FunkPlotter extends JFrame {

    PaintArea paintArea;
    int function = 0;    // diese Variable bestimmt die
    // zu zeichnende Funktion;
    // Startwert 0 = keine Funktion

    // Im Konstruktor werden die Canvas-Malfläche und
    // Schaltflächen zur Auswahl der Funktionen angelegt
    FunkPlotter(String titel) {
        super(titel);

        // Einen Layout-Manager einrichten
        setLayout(new FlowLayout());

        // Die Malfläche aufnehmen
        paintArea = new PaintArea();
        add(paintArea);

        // Schaltflächen anlegen und in Panel aufnehmen
        JButton f1 = new JButton("tan(x)");
        JButton f2 = new JButton("x^3");
        JButton f3 = new JButton("sin(x)");
        add(f1);
        add(f2);
        add(f3);

        // Das Ereignisbehandlung für die Schaltflächen
        class MeinActionLauscher implements ActionListener {
            public void actionPerformed(ActionEvent e) {
                String label;

                label = e.getActionCommand();

                switch (label) {
                    case "tan(x)" -> function = 1;
                    case "x^3" -> function = 2;
                    case "sin(x)" -> function = 3;
                }

                // Neuzeichnen veranlassen
                paintArea.repaint();
            }
        }

        // Die Lausch-Objekte anlegen
        f1.addActionListener(new MeinActionLauscher());
        f2.addActionListener(new MeinActionLauscher());
        f3.addActionListener(new MeinActionLauscher());

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        FunkPlotter window = new FunkPlotter("Funktionenplotter");
        window.pack();
        window.setSize(500, 350);
        window.setResizable(false);
        window.setVisible(true);
    }

    @SuppressWarnings("DuplicatedCode")
    class PaintArea extends JPanel {
        // Konstruktor
        PaintArea() {
            // den Hintergrund auf schwarz setzen
            setBackground(Color.black);

            // Vordergrund (=Zeichenfarbe) auf blau setzen
            setForeground(Color.green);
        }

        // Die wichtigste Methode: hier wird gezeichnet!
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            double x, y = 0;
            int xpos, ypos;

            // Ursprung umsetzen
            g.translate(150, 150);

            // Koordinatenachsen einzeichnen
            g.setColor(Color.red);
            g.drawLine(0, -150, 0, 150);
            g.drawLine(-150, 0, 150, 0);
            g.drawString("-3", -150, 12);
            g.drawString("-3", 4, 147);
            g.drawString("+3", 135, 12);
            g.drawString("+3", 4, -140);

            // Farbe zum Zeichnen der Funktion
            g.setColor(new Color(255, 255, 0));

            // Wenn keine Funktion ausgewählt ist, nichts tun
            if (function == 0)
                return;

            for (x = -3.0; x <= 3; x += 0.005) {

                switch (function) {
                    case 1 -> y = Math.tan(x);
                    case 2 -> y = Math.pow(x, 3);
                    case 3 -> y = Math.sin(x);
                }

                xpos = (int) (x * 50);
                ypos = (int) (-y * 50);

                g.fillOval(xpos, ypos, 3, 3);
            }
        }

        // Diese Methode liefert die minimale Grösse der anvas
        public Dimension getMinimumSize() {
            return new Dimension(300, 300);
        }

        // Die Lieblingsgrösse setzen wir auf die Minimalgrösse
        public Dimension getPreferredSize() {
            return getMinimumSize();
        }
    }

} // Ende der Klasse FunkPlotter
