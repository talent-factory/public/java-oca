package book.dirk_louis.ch09;// Ein Malprogramm

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Malprogramm extends JFrame {

    Leinwand paintArea;     // Hier wird gezeichnet
    ButtonGroup buttonGroup;   // aktuelle Form
    int xPosition, yPosition;           // aktuelle Mausposition

    // Im Konstruktor wird eine anvas-Malfl�che
    // angelegt sowie eine ButtonGroup-Gruppe mit
    // zur Auswahl stehenden Zeichenformen
    Malprogramm(String titel) {
        super(titel);

        // Einen Layout-Manager anlegen
        setLayout(new FlowLayout());

        // Die Malfläche anlegen
        paintArea = new Leinwand();
        add(paintArea);

        // Panel-Container für Schaltflächen anlegen
        JPanel panel = new JPanel();
        // Gitter mit 3 Zeilen, 1 Spalte
        panel.setLayout(new GridLayout(3, 1, 20, 20));

        // Optionsfelder zur Auswahl der Formen
        buttonGroup = new ButtonGroup();

        // 1. Optionsfelder erzeugen
        JRadioButton opt1 = new JRadioButton("Kreis", false);
        JRadioButton opt2 = new JRadioButton("Scheibe", false);
        JRadioButton opt3 = new JRadioButton("Rechteck", false);

        // 2. Befehlsnamen für Optionsfelder
        opt1.setActionCommand("Kreis");
        opt2.setActionCommand("Scheibe");
        opt3.setActionCommand("Rechteck");

        // 3. Optionsfelder in ButtonGroup-Gruppe aufnehmen
        buttonGroup.add(opt1);
        buttonGroup.add(opt2);
        buttonGroup.add(opt3);

        // 4. Optionsfelder in Panel aufnehmen
        panel.add(opt1);
        panel.add(opt2);
        panel.add(opt3);

        add(panel);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    // In main() wird eine Instanz der Klasse angelegt
    // und auf den Bildschirm gebracht
    public static void main(String[] args) {
        Malprogramm fenster = new Malprogramm("Malprogramm");
        fenster.pack();
        fenster.setSize(500, 350);
        fenster.setResizable(false);
        fenster.setVisible(true);
    }

    @SuppressWarnings("DuplicatedCode")
    class Leinwand extends JPanel {
        // Der Konstruktor implementiert die Mausbehandlung und setzt Hintergrund und Vordergrundfarbe
        Leinwand() {
            addMouseListener(new MeinMausAdapter());

            setBackground(Color.black);
            setForeground(Color.orange);
        }

        // Die wichtigste Methode: hier wird gezeichnet!
        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            String label;
            ButtonModel aktuell = null;

            // welche Form ist gerade ausgewählt?
            aktuell = buttonGroup.getSelection();

            // entsprechend handeln
            if (aktuell == null)
                return;

            int w = (int) (Math.random() * 300);
            int h = (int) (Math.random() * 300);
            label = aktuell.getActionCommand();

            if (label.equals("Kreis"))
                g.drawOval(xPosition, yPosition, w, w);

            if (label.equals("Scheibe"))
                g.fillOval(xPosition, yPosition, w, h);

            if (label.equals("Rechteck"))
                g.drawRect(xPosition, yPosition, w, h);
        }

        // Diese Methode liefert die minimale Grösse der anvas
        public Dimension getMinimumSize() {
            return new Dimension(300, 300);
        }

        // Die Lieblingsgrösse setzen wir auf die Minimalgrösse
        public Dimension getPreferredSize() {
            return getMinimumSize();
        }

        // Die Adapter-Klasse für die Mausklicks  als innere Klasse
        class MeinMausAdapter extends MouseAdapter {
            public void mousePressed(MouseEvent e) {
                // Die aktuelle Position der Maus merken
                xPosition = e.getX();
                yPosition = e.getY();

                // Malfläche aktualisieren
                repaint();
            }
        }
    }  // Ende von Leinwand

} // Ende von Malprogramm 
