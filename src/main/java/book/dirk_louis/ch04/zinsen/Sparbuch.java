package book.dirk_louis.ch04.zinsen;

public class Sparbuch {
    double kapital;
    double zinssatz;

    Sparbuch(double kap, double zins) {
        kapital = kap;
        zinssatz = zins;
    }

    void einzahlen(double betrag) {
        kapital += betrag;
    }

    void abheben(double betrag) {
        kapital -= betrag;
    }

    double ertragZ(double laufzeit) {
        return kapital *
                Math.pow((1 + zinssatz / 100), laufzeit);
    }

    double ertrag(double laufzeit) {
        return kapital * (1 + zinssatz / 100 * laufzeit);
    }
}
