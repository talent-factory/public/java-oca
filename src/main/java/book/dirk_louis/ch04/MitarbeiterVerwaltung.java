package book.dirk_louis.ch04;

import book.dirk_louis.ch03.Mitarbeiter;

public class MitarbeiterVerwaltung {
    public static void main(String[] args) {
        Mitarbeiter[] personalliste = new Mitarbeiter[4];
        int mitarbeiterzahl;

        personalliste[0] = new Mitarbeiter("Marx", "Groucho", 8000);
        personalliste[1] = new Mitarbeiter("Marx", "hico", 7000);
        personalliste[2] = new Mitarbeiter("Marx", "Harpo", 7000);
        personalliste[3] = new Mitarbeiter("Marx", "Zeppo", 7000);
        mitarbeiterzahl = 4;

        // alle Mitarbeiter ausgeben
        for (Mitarbeiter mitarbeiter : personalliste)
            mitarbeiter.datenAusgeben();
    }
}
