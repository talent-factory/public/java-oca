package book.dirk_louis.ch07;

import java.io.PrintStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("all")
public class ListenDemo {

    public static void main(String[] args) {

        // eine Liste anlegen und einige Namen ans Ende einf�gen
        List<ListItem> freunde = new LinkedList<>();

        ListItem aktuell = new ListItem("Dirk", 455689);
        freunde.add(aktuell);  // ans Ende anhängen

        aktuell = new ListItem("Peter", 543679);
        freunde.add(aktuell); // ans Ende anhängen

        // Objekt direkt erzeugen und anhängen
        freunde.add(new ListItem("Katja", 238590));

        // Objekt direkt erzeugen und vorne einfügen
        freunde.add(0, new ListItem("Julia", 749326));


        // den Inhalt der Liste auf drei verschiedene Arten ausgeben
        PrintStream cons = System.out;

        cons.print("\n Ausgabe mit get() \n");
        for (int i = 0; i < freunde.size(); i++) {
            aktuell = freunde.get(i);
            cons.printf(" %s %d\n", aktuell.name, aktuell.number);
        }

        cons.print("\n Ausgabe mit Iterator \n");
        Iterator<ListItem> it = freunde.iterator();
        while (it.hasNext()) {
            aktuell = it.next();
            cons.printf(" %s %d\n", aktuell.name, aktuell.number);
        }

        cons.print("\n Ausgabe mit for \n");
        for (ListItem akt : freunde) {
            cons.printf(" %s %d\n", akt.name, akt.number);
        }
    }
}
