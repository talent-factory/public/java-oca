package book.dirk_louis.ch07;

import java.util.Random;

/**
 * Listing 7.1, S.142
 * <p>
 * Einsatz des Zufallszahlengenerators
 */
class Lotto {

    public static void main(String[] args) {

        int zahl;
        int anzahl = 0;
        Random generator = new Random();

        System.out.println("\n Die Ziehung der Lottozahlen \n");

        while (true) {
            // Zahl zwischen 0 (inkl.) und 50 (exkl.)
            zahl = generator.nextInt(50);
            if (zahl == 0)
                // 0 brauchen wir nicht
                continue;

            // Zahl ausgeben
            System.out.println(" Gezogene Zahl: " + zahl);

            // Sind sechs Zahlen gezogen worden? Dann Ende.
            anzahl++;

            if (anzahl == 6)
                break;
        }
    }
}
