package book.dirk_louis.ch07;

import edu.stdlib.IOTools;

import java.util.Stack;

public class Revert {

    public static void main(String[] args) {

        System.out.print("Text eingeben: ");
        String word = IOTools.readLine();

        int length = word.length();

        // Die Buchstaben in einen Keller einfügen
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < length; i++) {
            stack.push(word.charAt(i));
        }

        // Elemente vom Stack entfernen und ausgeben
        System.out.print("Umgedreht    : ");
        for (int i = 0; i < length; i++) {
            System.out.printf("%s", stack.pop());
        }

        System.out.print("\n");
    }
}
