package book.dirk_louis.ch07;

class DictionaryItem {

    String word, type;

    DictionaryItem(String word, String type) {
        this.word = word;
        this.type = type;
    }

}
