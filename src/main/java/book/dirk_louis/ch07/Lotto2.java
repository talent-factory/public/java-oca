package book.dirk_louis.ch07;

import java.util.HashSet;
import java.util.Random;

/**
 * Listing 7.1, S.142
 * <p>
 * Einsatz des Zufallszahlengenerators, Verbesserung: Keine Doubletten
 */
class Lotto2 {
    public static void main(String[] args) {

        boolean eingefuegt;
        int i, zahl, anzahl = 0;
        Random generator = new Random();

        /*
         * Mit einem Set (Menge) stellen wir sicher, dass jeder Eintrag nur
         * 1x vorkommt (Definition einer Menge).
         */
        HashSet<Integer> gezogen = new HashSet<>();

        System.out.println("\n Die Ziehung der Lottozahlen \n");

        while (true) {
            zahl = generator.nextInt(50);

            if (zahl == 0)       // 0 brauchen wir nicht
                continue;

            // versuchen, die Zahl in die Menge einzufuegen
            // falls nicht moeglich -> neue Zahl erzeugen
            eingefuegt = gezogen.add(zahl);

            if (!eingefuegt)    // schon vorhanden -> neuer Versuch
                continue;

            // Zahl ausgeben
            System.out.println(" Gezogene Zahl: " + zahl);

            // Sind 6 Zahlen gezogen worden? Dann Ende.
            anzahl++;

            if (anzahl == 6)
                break;
        }
    }
}
