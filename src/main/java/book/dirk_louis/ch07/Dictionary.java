package book.dirk_louis.ch07;

import java.io.Console;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Dictionary {

    public static void main(String[] args) {

        Console cons = System.console();
        cons.printf("\n");

        Map<String, DictionaryItem> tabelle = new HashMap<>();

        tabelle.put("gehen", new DictionaryItem("walk", "Verb"));
        tabelle.put("laufen", new DictionaryItem("run", "Verb"));
        tabelle.put("schwimmen", new DictionaryItem("swim", "Verb"));
        tabelle.put("Reissverschluss", new DictionaryItem("zipper", "Nomen"));

        // nach einem Wort suchen
        Scanner tastatur = new Scanner(cons.reader());
        cons.printf(" Deutsches Wort: ");
        String suchString = tastatur.next();

        // In Hashtabelle nachschlagen
        DictionaryItem ergebnis = tabelle.get(suchString);

        if (ergebnis == null)
            cons.printf(" %s nicht gefunden! \n", suchString);
        else
            cons.printf(" %s heisst auf Englisch %s \n", suchString, ergebnis.word);
    }
}    
