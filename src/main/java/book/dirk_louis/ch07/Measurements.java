package book.dirk_louis.ch07;

import java.util.*;

/**
 * Wir möchten den zeitlichen Aufwand verschiedene Datenstrukturen aus dem
 * Java Collection Framework messen.
 * <p>
 * - ArrayList
 * - HashSet
 * - TreeSet
 */
public class Measurements {

    private static final int NUMBER_OF_INSERTS = 100_000_000;
    private static final int RANDOM_VALUES = 10_000;

    public static void main(String[] args) {

        {
            List<Integer> list = new ArrayList<>();
            System.out.println("ArrayList");
            insert(list);
            search(list);
        }

        {
            Set<Integer> list = new TreeSet<>();
            System.out.println("\nTreeSet");
            insert(list);
            search(list);
        }

        {
              Set<Integer> list = new HashSet<>();
              System.out.println("\nHashSet");
              insert(list);
              search(list);
          }
        

    }

    /**
     * Misst die Zeit, die für das Einfügen von Werten mit einer bestimmten
     * Datenstruktur benötigt wird.
     *
     * @param list die zu messende Liste
     */
    private static void insert(Collection<Integer> list) {
        Random random = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < NUMBER_OF_INSERTS; i++) {
            list.add(random.nextInt(RANDOM_VALUES));
        }

        long stop = System.currentTimeMillis();
        System.out.println("Insert: " + (stop - start) + "ms");
    }

    private static void search(Collection<Integer> list) {
        Random random = new Random();
        long start = System.currentTimeMillis();
        for (int i = 0; i < RANDOM_VALUES; i++) {
            // Wir suchen den Wert, ignorieren aber das Resultat
            list.contains(random.nextInt(RANDOM_VALUES));
        }

        long stop = System.currentTimeMillis();
        System.out.println("Search: " + (stop - start) + "ms");
    }

}
