package book.dirk_louis.ch07;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Abschnitt 7.2, S.143
 */
@SuppressWarnings("unused")
public class DateTimeValues {

    /**
     * Seit Java 8 kennen wir ein neues API java.time, welches wir an dieser
     * Stelle auch verwenden.
     * <p>
     * Manchmal müssen wir jedoch noch Konvertierungen zwischen den neuen
     * und den alten Klassen durchführen und mit Datumsdarstellungen von
     * beiden arbeiten.
     *
     * @param dateToConvert zu konvertierendes Datum
     * @return Datum gemäss 'alten' Klassen
     */
    public static Date convertToDate(LocalDate dateToConvert) {
        return java.sql.Date.valueOf(dateToConvert);
    }

    public LocalDate convertToLocalDate(Date dateToConvert) {
        return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
    }

    /**
     * Hauptprogramm zum Testen/Zeigen einiger Datumsfunktionen.
     *
     * @param args wird nicht verwendet.
     */
    public static void main(String[] args) {

        LocalDate date = LocalDate.now();
        System.out.println(date);

        LocalTime time = LocalTime.now();
        System.out.println(time);

        // Verwenden der 'alten' Formatklassen
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        System.out.println(formatter.format(convertToDate(date)));

        // Verwenden der 'neuen' Formatklassen
        // https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/time/format/DateTimeFormatter.html
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        System.out.println(date.format(dateTimeFormatter));
    }
}
