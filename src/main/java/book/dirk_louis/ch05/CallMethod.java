package book.dirk_louis.ch05;

import lombok.Data;

@SuppressWarnings("all")
public class CallMethod {

    /*
     * Java ist IMMER call-by-value
     */
    private static void increaseValue(int value) {
        value += 100;
    }

    private static void changeMember(final Member member) {
        // member = new Member(); // Zuweisung ist nicht erlaubt (final)
        member.setName("Daniel");
    }

    public static void main(String[] args) {

        int value = 57;
        System.out.printf("Value: %d\n", value); // 57

        increaseValue(value);
        System.out.printf("Value: %d\n", value); // 57

        Member member = new Member();
        System.out.println(member);              // Hallo

        changeMember(member);
        System.out.println(member);              // Daniel
    }

    @Data
    private static class Member {
        private String name = "Hallo";
    }
}
