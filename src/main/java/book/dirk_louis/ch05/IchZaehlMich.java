package book.dirk_louis.ch05;

public class IchZaehlMich {

    static int m_klassenvariable = 0;
    int m_instanzvariable = 0;

    public IchZaehlMich() {
        m_instanzvariable++;
        m_klassenvariable++;
        System.out.print(" Inst.variable = " + m_instanzvariable);
        System.out.println("\t Klas.variable = " + m_klassenvariable);
    }

}
