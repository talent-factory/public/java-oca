package book.dirk_louis.ch05;

public class Constants {

    // Der Wert dieser Konstante kann nur einmal zugewiesen werden.
    private final int MAX_VALUE;

    public Constants(int maxValue) {
        MAX_VALUE = maxValue;
    }

    public int getMAX_VALUE() {
        return MAX_VALUE;
    }
}
