package book.dirk_louis.ch05;

@SuppressWarnings("all")
public class Tauschprogramm {
    public static void main(String[] args) {
        int i;
        Tauscher tausche = new Tauscher();

        // Array von 3 Punkten anlegen
        Punkt[] liste = new Punkt[3];  // zuerst das
        // Array mit den
        // Referenzen

        // Nun Liste mit Punkt-Objekten initialisieren
        for (i = 0; i < liste.length; i++)
            liste[i] = new Punkt();

        // Koordinaten initialisieren
        liste[0].x = 0.0;
        liste[0].y = 1.0;
        liste[1].x = 2.0;
        liste[1].y = 3.0;
        liste[2].x = 4.0;
        liste[2].y = 5.0;

        System.out.println("\n Nach Initialisierung");
        for (i = 0; i < liste.length; i++) {
            System.out.println(" Liste[" + i + "] : x = " +
                    liste[i].x + "  y = " + liste[i].y);
        }

        tausche.koordinatenTausch(liste);

        System.out.println("\n Nach Tauschen");
        for (i = 0; i < liste.length; i++) {
            System.out.println(" Liste[" + i + "] : x = " +
                    liste[i].x + "  y = " + liste[i].y);
        }
    }

    // ------------------------------------------------------------------------

    static class Punkt {
        double x, y, z;
    }

    static class Tauscher {
        void koordinatenTausch(Punkt[] pliste) {
            int i = 0;
            double tmp;

            for (i = 0; i < pliste.length; i++) {
                // x und y vertauschen
                tmp = pliste[i].x;
                pliste[i].x = pliste[i].y;
                pliste[i].y = tmp;
            }
        }
    }

}
