package book.dirk_louis.ch05.interfaces;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class Lift {

    private double price;

}
