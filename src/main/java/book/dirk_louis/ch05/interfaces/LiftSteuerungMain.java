package book.dirk_louis.ch05.interfaces;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class LiftSteuerungMain {

    public static void main(String[] args) {

        Liftsteuerung lift = new TransportLift();
        lift.fahreZuDeck(6);

        ((Lift) lift).setPrice(500);
    }
}
