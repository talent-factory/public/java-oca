package book.dirk_louis.ch05.interfaces;

public interface Liftsteuerung {

    void fahreZuDeck(int deck);

    void oeffneTuere();

    void schliesseTuer();

}
