package book.dirk_louis.ch05.interfaces;

public class TransportLift extends Lift implements Liftsteuerung {

    public static void main(String[] args) {

        TransportLift lift = new TransportLift();
    }

    @Override
    public void fahreZuDeck(int deck) {

    }

    @Override
    public void oeffneTuere() {

    }

    @Override
    public void schliesseTuer() {

    }
}
