package book.dirk_louis.ch05;

public class Instanzenzaehler {

    static void erzeugeInstanzen() {
        IchZaehlMich instanz_1 = new IchZaehlMich();
        IchZaehlMich instanz_2 = new IchZaehlMich();
        IchZaehlMich instanz_3 = new IchZaehlMich();
    }

    public static void main(String[] args) {
        System.out.println("\n Instanzen erzeugen: \n");
        erzeugeInstanzen();
    }

}
