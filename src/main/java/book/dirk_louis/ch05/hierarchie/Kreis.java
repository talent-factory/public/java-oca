package book.dirk_louis.ch05.hierarchie;

@SuppressWarnings("all")
public class Kreis extends Figur {

    private int m_radius;

    public Kreis(int x, int y, int r) {
        super(x, y);
        m_radius = r;
    }

    @Override
    public void zeichnen() {
        System.out.println("Zeichnen-Methode für Kreise");
    }
}
