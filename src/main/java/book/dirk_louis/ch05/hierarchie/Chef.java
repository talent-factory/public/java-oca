package book.dirk_louis.ch05.hierarchie;

import book.dirk_louis.ch03.Mitarbeiter;

@SuppressWarnings("unused")
public class Chef extends Mitarbeiter {
    // keine Erweiterungen

    // Konstruktor von Chef ruft nur den Konstruktor
    // der Basisklasse auf
    public Chef(String lastName, String firstName, int salary) {
        super(lastName, firstName, salary);
    }

    @Override
    public void increaseSalary(int increase) {
        salary += 2 * increase;
    }

}
