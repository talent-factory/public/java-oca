package book.dirk_louis.ch05.hierarchie;

import book.dirk_louis.ch03.Mitarbeiter;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public final class Lehrling extends Mitarbeiter {

    private int abgelegtePruefungen;

    // Konstruktor setzt die Anzahl der Prüfungen auf 0
    Lehrling(String name, String vorname, int gehalt) {
        // den Konstruktor der Basisklasse aufrufen
        super(name, vorname, gehalt);

        // Initialisierung der eigenen Felder
        abgelegtePruefungen = 0;
    }

}
