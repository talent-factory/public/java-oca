package book.dirk_louis.ch05.hierarchie;

public class Zeichnen {

    private static Figur[] zeichenobjekte = new Figur[5];

    public static void main(String[] args) {
        System.out.println();

        zeichenobjekte[0] = new Kreis(20, 30, 10);
        System.out.println(zeichenobjekte[0].getCount());

        zeichenobjekte[1] = new Rechteck(2, 78, 50, 50);
        System.out.println(zeichenobjekte[1].getCount());

        zeichenobjekte[2] = new Kreis(99, 30, 10);
        System.out.println(zeichenobjekte[2].getCount());

        zeichenobjekte[3] = new Linie(201, 44, 201, 66);
        System.out.println(zeichenobjekte[3].getCount());

        zeichenobjekte[4] = new Linie(10, 50, 50, 50);
        System.out.println(zeichenobjekte[4].getCount());

        for (Figur figur : zeichenobjekte) {
            figur.zeichnen();
        }
    }
}    

