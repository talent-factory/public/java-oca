package book.dirk_louis.ch05.hierarchie;

import book.dirk_louis.ch03.Mitarbeiter;

@SuppressWarnings("unused")
public class MitarbeiterBeispiel {

    public static void main(String[] args) {

        Mitarbeiter[] personalListe = new Mitarbeiter[3];

        personalListe[0] = new Chef("Groucho", "Marx", 8000);
        personalListe[1] = new Junior("Hico", "Marx", 4000);
        personalListe[2] = new Lehrling("Harpo", "Marx", 1000);

        System.out.println("\nDaten ausgeben:");
        for (Mitarbeiter mitarbeiter : personalListe) {
            mitarbeiter.datenAusgeben();
        }

        // Gehalt erhöhen
        for (Mitarbeiter mitarbeiter : personalListe) {
            mitarbeiter.increaseSalary(1000);
        }

        System.out.println("\nDaten ausgeben:");
        for (Mitarbeiter mitarbeiter : personalListe) {
            mitarbeiter.datenAusgeben();
        }

        Mitarbeiter chef = new Chef("Daniel", "Senften", 5);
        Mitarbeiter daniel = new Junior("Daniel", "Senften", 5);
        Lehrling peter = new Lehrling("Peter", "Müller", 6);

        System.out.println("Done.");
    }
}

