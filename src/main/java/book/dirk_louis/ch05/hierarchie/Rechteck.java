package book.dirk_louis.ch05.hierarchie;

@SuppressWarnings("all")
public class Rechteck extends Figur {

    private int m_breite, m_laenge;

    public Rechteck(int x, int y, int l, int b) {
        super(x, y);
        m_laenge = l;
        m_breite = b;
    }

    @Override
    public void zeichnen() {
        System.out.println("Zeichnen-Methode für Rechtecke");
    }
}
