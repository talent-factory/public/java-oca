package book.dirk_louis.ch05.hierarchie;


@SuppressWarnings("FieldCanBeLocal")
public abstract class Figur {

    // Klassenvariable - Diese existier nur einmal.
    private static int count = 0;

    private final int m_xKoord, m_yKoord;  //xy.Koordinate der Figur

    public Figur(int x, int y) {
        m_xKoord = x;
        m_yKoord = y;
        count++;
    }

    abstract void zeichnen();

    public int getCount() {
        return count;
    }
}
