package book.dirk_louis.ch05.hierarchie;

import book.dirk_louis.ch03.Mitarbeiter;

@SuppressWarnings("unused")
public abstract class Angestellter extends Mitarbeiter {

    private static final int MAX_HIERARCHY = 5;
    private int hierarchiestufe;

    // Konstruktor
    public Angestellter(String name, String vorname, int gehalt) {
        // den Konstruktor der Basisklasse aufrufen
        super(name, vorname, gehalt);

        // Initialisierung der eigenen Felder
        hierarchiestufe = 0;
    }

    public void befoerdern() {
        // falls noch möglich, befördern
        if (hierarchiestufe < MAX_HIERARCHY)
            hierarchiestufe++;
    }
}
