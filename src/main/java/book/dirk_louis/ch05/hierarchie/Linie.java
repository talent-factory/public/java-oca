package book.dirk_louis.ch05.hierarchie;

@SuppressWarnings("all")
public class Linie extends Figur {

    private final int m_endpX, m_endpY;

    public Linie(int ax, int ay, int ex, int ey) {
        super(ax, ay);
        m_endpX = ex;
        m_endpY = ey;
    }

    @Override
    public void zeichnen() {
        System.out.println("Zeichnen-Methode für Linien");
    }
}
