package book.dirk_louis.ch08;// Das erste GUI-Programm mit Ereignisbehandlung

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUI_Beispiel2 extends JFrame {

    /**
     * Die eigenen Adapter- und Listener-Klassen als
     * innere Klassen innerhalb der Klasse
     * GUI_GBeispiel2 definieren
     */
    static class MeinWindowLauscher extends WindowAdapter {
        public void windowClosing(WindowEvent e) {

            // Das Programm beenden
            System.exit(0);
        }
    }

    static class MeinActionLauscher implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            // einmal piepen
            Toolkit.getDefaultToolkit().beep();
        }
    }

    // der Konstruktor legt drei Schaltflächen an
    GUI_Beispiel2(String titel) {
        super(titel);

        // Schaltflächen erzeugen
        JButton haensel = new JButton("Hänsel");
        JButton und = new JButton("und");
        JButton gretel = new JButton("Gretel");

        // Einen Layout-Manager zum Anordnen der Schalter festlegen
        setLayout(new FlowLayout());

        // Schaltflächen zum Frame hinzuf�gen
        add(haensel);
        add(und);
        add(gretel);

        // den Frame bei einem WindowListener anmelden
        this.addWindowListener(new MeinWindowLauscher());

        // ActionListener für die Schaltflächen registrieren
        // Es wird jedes Mal eine neue Instanz angelegt. Man
        // kann aber auch eine Instanz mehrfach verwenden
        haensel.addActionListener(new MeinActionLauscher());
        und.addActionListener(new MeinActionLauscher());
        gretel.addActionListener(new MeinActionLauscher());
    }

    public static void main(String[] args) {
        // eine Instanz der Klasse anlegen und anzeigen
        GUI_Beispiel2 fenster = new GUI_Beispiel2("GUI mit Ereignisbehandlung");

        fenster.pack();
        fenster.setVisible(true);
    }
}
