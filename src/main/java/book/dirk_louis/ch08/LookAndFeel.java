package book.dirk_louis.ch08;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LookAndFeel extends JFrame implements ActionListener {

    public static void main(String[] args) {
        LookAndFeel auswahl = new LookAndFeel("Look&Feel-Demo");

        auswahl.pack();
        auswahl.setSize(640, 400);
        auswahl.setVisible(true);
    }


    // der Konstruktor
    LookAndFeel(String titel) {
        super(titel);

        //*******************************************
        // den Native Look and Feel erzwingen
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            System.err.println("Fehler bei LookandFeel\n");
        }

        setLayout(new GridLayout(3, 4, 3, 3));

        JPanel zelle1 = new JPanel();
        JPanel zelle2 = new JPanel();
        JPanel zelle3 = new JPanel();
        JPanel zelle4 = new JPanel();
        JPanel zelle5 = new JPanel();
        JPanel zelle6 = new JPanel();
        JPanel zelle7 = new JPanel();
        JPanel zelle8 = new JPanel();
        JPanel zelle9 = new JPanel();
        JPanel zelle10 = new JPanel();
        JPanel zelle11 = new JPanel();
        JPanel zelle12 = new JPanel();

        //*******************************************
        // Komponenten aufnehmen

        // Bild für Label und Schaltfläche laden
        ImageIcon icon = new ImageIcon("src/main/resources/buttonImage.gif");

        // Label
        JLabel text1 = new JLabel("Statischer Text", icon, SwingConstants.LEFT);
        text1.setFont(new Font("Monospaced", Font.PLAIN, 12));
        zelle1.add(text1);

        // Liste
        String[] elemente = {"Rot", "Grün", "Blau", "Rosa", "Schwarz", "Weiss",
                "Magenta", "Gelb", "Azurblau"};
        JList<String> liste = new JList<>(elemente);
        liste.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        JScrollPane listenScrollPane = new JScrollPane(liste);
        listenScrollPane.setPreferredSize(new Dimension(70, 100));
        zelle2.add(listenScrollPane);

        // Kombinationsfeld
        JComboBox<String> kombi = new JComboBox<>(elemente);
        zelle3.add(kombi);

        // Bildlaufleisten
        JScrollBar bildlauf = new JScrollBar(Adjustable.VERTICAL, 4, 1, 0, 10);
        bildlauf.setPreferredSize(new Dimension(20, 100));
        zelle4.add(bildlauf);

        // Schaltfläche
        JButton schalter = new JButton("Klick mich", icon);
        zelle5.add(schalter);

        // Kontrollkästchen
        JCheckBox opt1 = new JCheckBox("Schafe", false);
        JCheckBox opt2 = new JCheckBox("Ziegen", false);
        zelle6.add(opt1);
        zelle6.add(opt2);

        // Optionsfelder
        ButtonGroup gruppe = new ButtonGroup();

        JRadioButton opt3 = new JRadioButton("Rot", false);
        JRadioButton opt4 = new JRadioButton("Grün", false);
        JRadioButton opt5 = new JRadioButton("Blau", true);

        gruppe.add(opt3);
        gruppe.add(opt4);
        gruppe.add(opt5);

        zelle7.add(opt3);
        zelle7.add(opt4);
        zelle7.add(opt5);

        // Textkomponenten
        JTextField text2 = new JTextField(10);
        text2.setText("Eingabe");
        zelle9.add(text2);

        JPasswordField text3 = new JPasswordField(10);
        text3.setEchoChar('*');
        text3.setText("Eingabe");
        zelle10.add(text3);

        JTextArea text4 = new JTextArea(" Dies ist ein \n", 4, 15);
        text4.append(" mehrzeiliges\n Textfeld\n");
        zelle11.add(text4);

        add(zelle1);
        add(zelle2);
        add(zelle3);
        add(zelle4);
        add(zelle5);
        add(zelle6);
        add(zelle7);
        add(zelle8);
        add(zelle9);
        add(zelle10);
        add(zelle11);
        add(zelle12);

        //*******************************************
        // Menueleiste erstellen
        JMenuBar menue_leiste = new JMenuBar();
        JMenu menu1 = new JMenu("Programm");
        JMenuItem prog_item1 = new JMenuItem("Programm beenden");
        prog_item1.addActionListener(this);
        menu1.add(prog_item1);

        JMenu menu2 = new JMenu("Dialoge");
        JMenuItem frage_item = new JMenuItem("Frage-Dialog");
        JMenuItem confirm_item = new JMenuItem("Bestätigungs-Dialog");
        frage_item.addActionListener(this);
        confirm_item.addActionListener(this);
        menu2.add(frage_item);
        menu2.add(confirm_item);

        JMenu menu3 = new JMenu("Aussehen");
        ButtonGroup lookandfeel = new ButtonGroup();
        JRadioButtonMenuItem metal_item =
                new JRadioButtonMenuItem("Metal LookAndFeel");
        JRadioButtonMenuItem native_item =
                new JRadioButtonMenuItem("Native LookAndFeel");
        metal_item.addActionListener(this);
        native_item.addActionListener(this);
        native_item.setSelected(true);
        lookandfeel.add(metal_item);
        lookandfeel.add(native_item);

        menu3.add(metal_item);
        menu3.add(native_item);

        menue_leiste.add(menu1);
        menue_leiste.add(menu2);
        menue_leiste.add(menu3);
        setJMenuBar(menue_leiste);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    //*******************************************
    // Behandlung der Menübefehle
    public void actionPerformed(ActionEvent e) {
        String quelle = e.getActionCommand();
        if (quelle.equals("Programm beenden"))
            System.exit(0);

        if (quelle.equals("Metal LookAndFeel")) {
            try {
                UIManager.setLookAndFeel(
                        UIManager.getCrossPlatformLookAndFeelClassName());
                SwingUtilities.updateComponentTreeUI(this);
                return;
            } catch (Exception ex) {
                // nichts weiter tun
                return;
            }
        }

        if (quelle.equals("Native LookAndFeel")) {
            try {
                UIManager.setLookAndFeel(
                        UIManager.getSystemLookAndFeelClassName());
                SwingUtilities.updateComponentTreeUI(this);
                return;
            } catch (Exception ex) {
                // nichts weiter tun
                return;
            }
        }

        if (quelle.equals("Frage-Dialog")) {
            String antwort;
            antwort = JOptionPane.showInputDialog(this, "Wie geht es Ihnen?");
            JOptionPane.showMessageDialog(this, "Es geht Ihnen " + antwort);
            return;
        }

        if (quelle.equals("Bestätigungs-Dialog")) {
            int antwort;
            antwort = JOptionPane.showConfirmDialog(this,
                    "Sind Sie mit Java zufrieden? ");

            switch (antwort) {
                case JOptionPane.YES_OPTION -> JOptionPane.showMessageDialog(this,
                        "Ihre Antwort war: JA ");
                case JOptionPane.NO_OPTION -> JOptionPane.showMessageDialog(this,
                        "Ihre Antwort war: NEIN ");
                default -> JOptionPane.showMessageDialog(this,
                        "Sie haben nicht geantwortet. ");
            }
        }
    }
} 
