package book.dirk_louis.ch08;

import javax.swing.*;
import java.awt.*;

public class GUI_Beispiel extends JFrame {

    // der Konstruktor legt drei Schaltflächen an
    GUI_Beispiel(String titel) {
        super(titel);                      // Fenstertitel

        // Schaltflächen erzeugen
        JButton haensel = new JButton("Hänsel");
        JButton und = new JButton("und");
        JButton gretel = new JButton("Gretel");

        // Einen Layout-Manager zum Anordnen der Schalter festlegen
        setLayout(new FlowLayout());

        // zum JFrame hinzufügen
        add(haensel);
        add(und);
        add(gretel);
    }

    public static void main(String[] args) {
        // eine Instanz der Fensterklasse anlegen
        GUI_Beispiel fenster = new GUI_Beispiel("Erstes GUI-Programm");

        fenster.pack();
        fenster.setVisible(true);
    }
}
