package book.dietmar_ratz.ch08.s7.p3;

interface IntA {
    default void tuWas() {
        System.out.println("tuWas: IntA");
    }
}

class KlasseA implements IntA {
    public void tuWas() {
        System.out.println("tuWas: KlasseA");
    }
}

class KonflikteA {
    public static void main(String[] args) {
        KlasseA ka = new KlasseA();
        ka.tuWas();
    }
}

interface IntB extends IntA {
    default void tuWas() {
        System.out.println("tuWas: IntB");
    }
}

class KlasseB implements IntB {
    public void arbeite() {
        tuWas();
    }
}

class KonflikteB {
    public static void main(String[] args) {
        KlasseB kb = new KlasseB();
        kb.arbeite();
    }
}

interface IntC {
    default void tuWas() {
        System.out.println("tuWas: IntC");
    }
}

//class KlasseAC implements IntA, IntC {
//    public void arbeite() {
//        // tuWas();
//    }
//}
