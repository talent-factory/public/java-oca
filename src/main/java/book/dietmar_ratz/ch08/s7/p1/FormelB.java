package book.dietmar_ratz.ch08.s7.p1;

interface FormelB {
    double rechne(double x);

    default double sqrt(double x) {
        return Math.sqrt(x);
    }
}
