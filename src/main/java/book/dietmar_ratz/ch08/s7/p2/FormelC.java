package book.dietmar_ratz.ch08.s7.p2;

public interface FormelC {
    double rechne(double x);

    default double sqrt(double x) {
        return Math.sqrt(x);
    }

    static double sqrt2() {
        return Math.sqrt(2);
    }
} 
