package book.dietmar_ratz.ch08.s2;

import book.dietmar_ratz.ch08.s1.p3.USDollar;

public class NonsensDollar extends USDollar {

    /**
     * Uebernehme den Konstruktor der Superklasse unveraendert.
     */
    public NonsensDollar(double wert) {
        super(wert);
    }

    /**
     * Gib beim Dollarbetrag etwas vollkommen UNSINNIGES aus
     */
    public double dollarBetrag() {
        return Math.random();
    }

}
