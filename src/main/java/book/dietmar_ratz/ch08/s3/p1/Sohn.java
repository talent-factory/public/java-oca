package book.dietmar_ratz.ch08.s3.p1;

public class Sohn extends Vater {

    /**
     * Eine oeffentliche Variable vari
     */
    public int vari;

    /**
     * Konstruktor
     */
    public Sohn() {
        vari = 2;
    }

    /**
     * Ausgabe des Variableninhalts
     */
    public void zeigeVari() {
        System.out.println("SOHN:  " + vari);
    }

}
