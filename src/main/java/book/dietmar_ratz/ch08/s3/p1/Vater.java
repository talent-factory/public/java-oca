package book.dietmar_ratz.ch08.s3.p1;

public class Vater {

    /**
     * Eine oeffentliche Variable vari
     */
    public int vari;

    /**
     * Konstruktor
     */
    public Vater() {
        vari = 1;
    }

    /**
     * Ausgabe des Variableninhalts
     */
    public void zeigeVari() {
        System.out.println("VATER: " + vari);
    }

}
