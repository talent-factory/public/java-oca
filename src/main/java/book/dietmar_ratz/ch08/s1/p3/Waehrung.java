package book.dietmar_ratz.ch08.s1.p3;

/**
 * Diese Klasse symbolisiert eine beliebige Waehrung
 */
public abstract class Waehrung {

    /**
     * Gibt den Wert des Objekts in US-Dollar zurueck
     */
    public abstract double dollarBetrag();
}
