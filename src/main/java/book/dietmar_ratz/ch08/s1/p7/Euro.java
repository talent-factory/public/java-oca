package book.dietmar_ratz.ch08.s1.p7;

import book.dietmar_ratz.ch08.s1.p3.Waehrung;

/**
 * Die Waehrung Europas
 */
public class Euro extends Waehrung {

    /**
     * Ein Euro ist soviel Dollar wert (Stand Dezember 1999)
     */
    private static double kurs = 1;

    /**
     * Instanzvariable: Wert in Euro
     */
    private double wert;

    /**
     * Konstruktor
     */
    public Euro(double wert) {
        this.wert = wert;
    }

    /**
     * Deklaration der sonst abstrakten Methode dollarBetrag
     */
    public double dollarBetrag() {
        return wert * kurs;
    }

    /**
     * Gibt den Wert der Waehrung in Euro zurueck
     */
    public double euroBetrag() {
        return wert;
    }

    /**
     * Zugriff auf die private Klassenvariable
     */
    public static void setEuroKurs(double Kurs) {
        kurs = Kurs;
    }

}
