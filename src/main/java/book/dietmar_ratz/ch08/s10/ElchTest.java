package book.dietmar_ratz.ch08.s10;

@SuppressWarnings("all")
public class ElchTest {

    public static void main(String[] argv) {
        AKlasse a = new AKlasse();
        System.out.println("Wert von a ist: " + a.wert);
        System.out.println("Wert von a ist: " + a.wert());
        CKlasse b = new CKlasse();
        System.out.println("Wert von b ist: " + b.wert);
        System.out.println("Wert von b ist: " + b.wert());
        AKlasse c = b;
        System.out.println("Wert von c ist: " + c.wert);
        System.out.println("Wert von c ist: " + c.wert());
    }
}
