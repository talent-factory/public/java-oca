package book.dietmar_ratz.ch08.s10;

/**
 * Klasse für Punkte (x,y) in der Ebene
 */
public class Point {
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static double distance(Point p, Point q) {
        // liefert den Abstand zwischen p und q
        double xdiff = p.getX() - q.getX();
        double ydiff = p.getY() - q.getY();
        return Math.sqrt(xdiff * xdiff + ydiff * ydiff);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void turn(double phi) {
        // dreht das aufrufende Point-Objekt um den Winkel phi
        double xAlt = x;
        x = xAlt * Math.cos(phi) - y * Math.sin(phi);
        y = xAlt * Math.sin(phi) + y * Math.cos(phi);
    }

    public String toString() {
        // liefert die String-Darstellung des aufrufenden Point-Objekts
        return "(" + x + "," + y + ")";
    }
}
