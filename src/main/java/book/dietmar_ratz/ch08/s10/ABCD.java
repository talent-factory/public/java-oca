package book.dietmar_ratz.ch08.s10;

public class ABCD {

    public static void main(String[] args) {
        A a = new A();
        a.tell();  // Aufruf 1
        B b = new B();
        b.tell();  // Aufruf 2
        C c = new C();
        c.tell();  // Aufruf 3
        D d = new D();
        d.tell();  // Aufruf 4
    }
}
