package book.dietmar_ratz.ch08.s10;

public class Bildschirm {

    /**
     * Schreibt 100 Leerzeilen auf den Bildschirm
     */
    public static void loeschen() {
        for (int i = 0; i < 100; i++)
            System.out.println();
    }
}
