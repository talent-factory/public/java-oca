package book.dietmar_ratz.ch08.s8.p4;

import book.dietmar_ratz.ch08.s8.p3.Funktion;

@SuppressWarnings("all")
public class ErzeugeWerteTabellen2 {
    // Methode fuer das Tabellieren von Funktionswerten
    public static void tabelliere(String titel, Funktion f) {
        System.out.println("Wertetabelle der " + titel + "-Funktion");
        System.out.println("  x        f(x)");
        for (double x = 1.0; x <= 5.0; x++) {
            System.out.println("  " + x + "      " + f.rechne(x));
        }
    }

    // main-Methode erzeugt Funktionsobjekte fuer das Tabellieren
    public static void main(String[] args) {
        Funktion sqr = new Funktion() {  // anonyme innere Klasse
            public double rechne(double x) {
                return x * x;
            }
        };

        Funktion sin = new Funktion() {  // anonyme innere Klasse
            public double rechne(double x) {
                return Math.sin(x);
            }
        };

        Funktion tan = new Funktion() {  // anonyme innere Klasse
            public double rechne(double x) {
                return Math.tan(x);
            }
        };

        tabelliere("Quadrat", sqr);
        tabelliere("Sinus", sin);
        tabelliere("Tangens", tan);
    }
} 
