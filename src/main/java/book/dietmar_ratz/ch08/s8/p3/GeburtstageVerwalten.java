package book.dietmar_ratz.ch08.s8.p3;

import stdlib.IOTools;

public class GeburtstageVerwalten {
    // maximal 1000 Geburtstage sollen verwaltet werden
    Geburtstag[] liste = new Geburtstag[1000];
    // beim Programmstart ist noch kein Geburtstag eingegeben
    int anzahl = 0;

    Menuepunkt[] menue = {   // die moeglichen Aktionen als Array
            new Beenden(),
            new EintragHinzufuegen(),
            new AlleAusgeben(),
    };
    boolean ende = false;

    // Methode fuer das User-Interface der Geburtstagsverwaltung
    public void zeigeHauptmenue() {
        while (!ende) {
            System.out.println();
            System.out.println("===========");
            System.out.println("Geburtstage");
            System.out.println("===========");
            System.out.println("[0] Programm beenden");
            System.out.println("[1] Eintrag hinzufuegen");
            System.out.println("[2] Alle ausgeben");

            int nr = IOTools.readInteger("Ihre Auswahl: ");

            if (nr >= 0 && nr < menue.length) {
                menue[nr].ausfuehren();
            }
        }
    }

    // main erzeugt das Geburtstage-Verwaltungsobjekt
    public static void main(String[] args) {
        GeburtstageVerwalten gv = new GeburtstageVerwalten();
        gv.zeigeHauptmenue();
    }

    // Innere Klasse fuer die Geburtstags-Objekte
    class Geburtstag {
        String name = IOTools.readLine("Name: ");
        String tag = IOTools.readLine("Geburtsdatum: ");

        public String toString() {
            return name + " (" + tag + ")";
        }
    }

    // Innere Klasse fuer einen Menuepunkt
    class Beenden implements Menuepunkt {
        public void ausfuehren() {
            ende = true;
        }
    }

    // Innere Klasse fuer einen Menuepunkt
    class EintragHinzufuegen implements Menuepunkt {
        public void ausfuehren() {
            liste[anzahl++] = new Geburtstag();
        }
    }

    // Innere Klasse fuer einen Menuepunkt
    class AlleAusgeben implements Menuepunkt {
        public void ausfuehren() {
            System.out.println("***  Alle Geburtstage:");
            for (int i = 0; i < anzahl; i++) {
                System.out.println("***    " + liste[i]);
            }
        }
    }
}
