package book.dietmar_ratz.ch08.s8.p3;

public class ErzeugeWerteTabellen {

    // Methode fuer das Tabellieren von Funktionswerten
    public static void tabelliere(String titel, Funktion f) {
        System.out.println("Wertetabelle der " + titel + "-Funktion");
        System.out.println("  x        f(x)");
        for (double x = 1.0; x <= 5.0; x++) {
            System.out.println("  " + x + "      " + f.rechne(x));
        }
    }

    // main-Methode erzeugt Funktionsobjekte fuer das Tabellieren
    public static void main(String[] args) {
        Funktion sqr = new Quadrat();
        Funktion sin = new Sinus();
        Funktion tan = new Tangens();
        tabelliere("Quadrat", sqr);
        tabelliere("Sinus", sin);
        tabelliere("Tangens", tan);
    }

    // Statische innere Klasse fuer die Quadrat-Funktion
    static class Quadrat implements Funktion {
        public double rechne(double x) {
            return x * x;
        }
    }

    // Statische innere Klasse fuer die Sinus-Funktion
    static class Sinus implements Funktion {
        public double rechne(double x) {
            return Math.sin(x);
        }
    }

    // Statische innere Klasse fuer die Tangens-Funktion
    static class Tangens implements Funktion {
        public double rechne(double x) {
            return Math.tan(x);
        }
    }

} 
