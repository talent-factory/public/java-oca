package book.dietmar_ratz.ch08.s8.p3;

class EinsMitInnerenKlassenDemo {

    public static void main(String[] args) {

        /* Erzeuge ein Objekt der inneren Klasse Drei
         * der Klasse EinsMitInnerenKlassen
         */
        new EinsMitInnerenKlassen().new Drei();

        /* Erzeuge ein Objekt der statischen inneren Klasse Vier
         * der Klasse EinsMitInnerenKlassen
         */
        new EinsMitInnerenKlassen.Vier();
    }
}
