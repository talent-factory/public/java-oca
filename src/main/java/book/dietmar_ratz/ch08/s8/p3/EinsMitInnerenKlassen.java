package book.dietmar_ratz.ch08.s8.p3;

class EinsMitInnerenKlassen {

    int x;

    /**
     * Erzeuge ein Objekt der inneren Klasse Drei
     */
    Drei d = new Drei();

    /**
     * Erzeuge ein Objekt der inneren Klasse Vier
     */
    Vier v = new Vier();

    void meth1() {
        d.w = 5.0;
        d.meth3();
        System.out.println(v.y);
    }

    /**
     * Innere Klasse (nicht-statisch)
     */
    class Drei {
        double w;

        void meth3() {
            x = 555;
        }
    }

    /**
     * Innere Klasse (statisch)
     */
    static class Vier {
        int y = 3;
    }
}
