package book.dietmar_ratz.ch08.s6.p3;

public class Bewegen {
    public static void losGehtEs(Beweglich b) {
        b.bewegeDichNach(3, 7);  // b ist Beweglich
    }

    public static void main(String[] args) {
        Beweglich b;

        b = new Kreis(5, 0, 0);  // jeder Kreis ist Beweglich
        losGehtEs(b);

        b = new Stuhl_v1(4, 9);    // jeder Stuhl ist Beweglich
        losGehtEs(b);
    }
}
