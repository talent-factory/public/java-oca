package book.dietmar_ratz.ch08.s6.p3;

import book.dietmar_ratz.ch08.s6.p2.Figur;

public class Kreis extends Figur implements Beweglich {
    public int radius;

    // Konstruktor
    public Kreis(int r, int x, int y) {
        super("Kreis");  // Konstruktor der Oberklasse aufrufen
        radius = r;      // Radius initialisieren
        ortX = x;        // geerbte Variable ortX initialisieren
        ortY = y;        // geerbte Variable ortY initialisieren
    }

    // durch abstrakte Klasse vorgeschriebene Methode implementieren
    public void show() {
        System.out.println(name + " mit Radius " + radius);
    }

    // durch abstrakte Klasse vorgeschriebene Methode implementieren
    public boolean contains(int x, int y) {
        return (ortX - x) * (ortX - x) +
                (ortY - y) * (ortY - y) <= radius * radius;
    }

    // durch Interface vorgeschriebene Methode implementieren
    public void bewegeDichNach(int x, int y) {
        ortX = x;
        ortY = y;
    }
}
