package book.dietmar_ratz.ch08.s6.p3;

public class Stuhl_v2 implements Beweglich, Klappbar {
    int reihe, platz;

    // Konstruktor
    public Stuhl_v2(int r, int p) {
        reihe = r;   // Sitzreihe initialisieren
        platz = p;   // Platznummer initialisieren
    }

    // durch 1. Interface vorgeschriebene Methode implementieren
    public void bewegeDichNach(int r, int p) {
        reihe = r;
        platz = p;
    }

    // durch 2. Interface vorgeschriebene Methode implementieren
    public void zusammenklappen() {
        reihe = 0;
        platz = 0;
    }

    // durch 2. Interface vorgeschriebene Methode implementieren
    public void aufstellen(int r, int p) {
        bewegeDichNach(r, p);
    }
}
