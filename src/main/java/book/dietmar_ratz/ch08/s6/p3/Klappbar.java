package book.dietmar_ratz.ch08.s6.p3;

public interface Klappbar {
    void zusammenklappen();

    void aufstellen(int r, int p);
}
