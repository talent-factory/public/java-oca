package book.dietmar_ratz.ch08.s6.p3;

public class Stuhl_v1 implements Beweglich {
    int reihe, platz;  // Sitzreihe und Platznummer des Stuhls

    // Konstruktor
    public Stuhl_v1(int r, int p) {
        reihe = r;    // Sitzreihe initialisieren
        platz = p;    // Platznummer initialisieren
    }

    // durch Interface vorgeschriebene Methode implementieren
    public void bewegeDichNach(int r, int p) {
        reihe = r;
        platz = p;
    }
}
