package book.dietmar_ratz.ch08.s6.p3;

public interface Beweglich {
    int DIMENSION = 2;

    void bewegeDichNach(int x, int y);
}
