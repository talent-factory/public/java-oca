package book.dietmar_ratz.ch08.s6.p1;

/**
 * Diese Klasse symbolisiert eine beliebige Waehrung
 */
public abstract class Waehrung {

    /**
     * Gibt den Wert des Objekts in US-Dollar zurueck
     */
    public abstract double dollarBetrag();

    /**
     * Gibt den Wert der Waehrung in Dollar als String zurueck
     */
    public String toString() {
        return "$" + dollarBetrag();
    }

    /**
     * Prueft, ob das Objekt gleich dem Parameterobjekt obj ist
     */
    public boolean equals(Object obj) {
        if (obj == null)               // Vergleich mit null-Referenz
            return false;
        if (obj == this)                // Vergleich mit sich selber
            return true;
        if (!obj.getClass().equals(getClass()))  // DatentypVergleich
            return false;

        Waehrung that = (Waehrung) obj;      // Typecast und Inhalts-
        return this.dollarBetrag() == that.dollarBetrag();  // vergl.
    }

    /**
     * Liefert den Hashcode eines Objekts
     */
    public int hashCode() {
        return (int) (dollarBetrag() * 100);
    }

}
