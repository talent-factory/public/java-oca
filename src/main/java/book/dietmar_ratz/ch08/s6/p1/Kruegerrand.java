package book.dietmar_ratz.ch08.s6.p1;

/**
 * Eine bekannte Anlage-Muenze
 */
@SuppressWarnings("all")
public class Kruegerrand extends Waehrung
        implements Wertgegenstand {

    /**
     * Ein Kruegerrand ist soviel Dollar wert
     */
    private static double kurs;

    /**
     * Instanzvariable: Wert in Kruegerrand
     */
    private double wert;

    /**
     * Konstruktor
     */
    public Kruegerrand(double wert) {
        this.wert = wert;
    }

    /**
     * Deklaration der sonst abstrakten Methode dollarBetrag
     */
    public double dollarBetrag() {
        return wert * kurs;
    }

    /**
     * Zugriff auf die private Klassenvariable
     */
    public static void setKurs(double kurs) {
        kurs = kurs;
    }

    @Override
    public Waehrung wert() {
        return null;
    }

    /*
     * Implementierung des Interface:
     * das Objekt ist selbst schon Waehrung
     */
//    public Waehrung wert() {
//        return this;
//    }

}
