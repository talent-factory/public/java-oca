package book.dietmar_ratz.ch08.s6.p1;

/** Ein Goldbarren (= Wertgegenstand) */
public class Goldbarren implements Wertgegenstand {

  /** Wie viel ist Gold heutzutage eigentlich wert? */
  public static double preisProGrammInDollar=42;

  /** Das Gewicht des Barrens */
  private double gewicht;

  /** Konstruktor - das Gewicht ist in Gramm anzugeben */
  public Goldbarren(double gewichtInGramm) {
    gewicht = gewichtInGramm;
  }

  /** Implementierung des Interfaces */
  public Waehrung wert() {
    return new USDollar(gewicht * preisProGrammInDollar);
  }
  
}
