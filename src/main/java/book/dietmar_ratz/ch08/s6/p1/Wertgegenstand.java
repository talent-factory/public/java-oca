package book.dietmar_ratz.ch08.s6.p1;

/**
 * Ein beliebiger Wertgegenstand
 */
public interface Wertgegenstand {

    /**
     * Gib den Wert des Objekts als Waehrung zurueck
     */
    Waehrung wert();

}
