package book.dietmar_ratz.ch08.s6.p2;

public class FigurBearbeitung {

    public static void check(Figur f) {
        f.show();
        if (f.contains(1, 2)) {
            System.out.println("Punkt (1,2) liegt im " + f.name);
        }
    }
}
