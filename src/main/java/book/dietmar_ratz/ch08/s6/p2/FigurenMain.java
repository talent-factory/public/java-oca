package book.dietmar_ratz.ch08.s6.p2;

public class FigurenMain {

    public static void main(String[] args) {
        Figur f = new Kreis(5, 0, 0);
        FigurBearbeitung.check(f);

        f = new RechtEckchen(10, 10, 6, 17);
        FigurBearbeitung.check(f);
    }
}
