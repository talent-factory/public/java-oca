package book.dietmar_ratz.ch08.s6.p2;

public class RechtEckchen extends Figur {

    int breite, hoehe;

    // Konstruktor
    public RechtEckchen(int x, int y, int b, int h) {
        super("RechtEckchen");  // Konstruktor der Oberklasse aufrufen
        ortX = x;               // geerbte Variable ortX initialisieren
        ortY = y;               // geerbte Variable ortY initialisieren
        breite = b;             // Breite initialisieren
        hoehe = h;              // Hoehe initialisieren
    }

    // durch abstrakte Klasse vorgeschriebene Methode implementieren
    public void show() {
        System.out.println(name + " mit Breite " + breite +
                " und Hoehe " + hoehe);
    }

    // durch abstrakte Klasse vorgeschriebene Methode implementieren
    public boolean contains(int x, int y) {
        return ortX <= x && x <= ortX + breite &&
                ortY <= y && y <= ortY + hoehe;
    }
}
