package book.dietmar_ratz.ch08.s6.p2;

public abstract class Figur {

    public String name;    // Bezeichnung der Figur
    public int ortX, ortY; // Ortskoordinaten der Figur

    // Konstruktor
    public Figur(String name) {
        this.name = name;
    }

    // Methode, mit der sich die Figur "zeigen" kann
    public abstract void show();

    // Methode, mit der geprueft werden kann, ob die
    // "Koordinaten" (x,y) innerhalb der Figur liegen
    public abstract boolean contains(int x, int y);
}
