package book.dietmar_ratz.ch17.s6;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;

public class DateiOperationen {

    public static void main(String[] args) {
        Path txtVersion = Paths.get("testdatei.txt");
        Path altVersion = Paths.get("testdatei.alt");
        Path oldVersion = Paths.get("testdatei.old");
        Path quark = Paths.get("quark.txt");

        try {
            Files.deleteIfExists(quark);
            Files.copy(txtVersion, altVersion, REPLACE_EXISTING);
            Files.move(altVersion, oldVersion, REPLACE_EXISTING);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        try (PrintWriter p =
                     new PrintWriter(
                             Files.newOutputStream(txtVersion, CREATE, APPEND))) {
            p.println("Eine weitere Zeile.");
            p.println("Und noch eine weitere Zeile!");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
