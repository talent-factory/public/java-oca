package book.dietmar_ratz.ch17.s6;

import java.nio.file.Path;
import java.nio.file.Paths;

@SuppressWarnings("DuplicatedCode")
public class PathBeispiele {

    public static void main(String[] args) {
        Path td;

        td = Paths.get("testdatei.txt");
        System.out.println("Path: " + td);
        System.out.println("FileName: " + td.getFileName());
        for (int i = 0; i < td.getNameCount(); i++)
            System.out.println("Name: " + td.getName(i));
        System.out.println("Parent: " + td.getParent());
        System.out.println("Root: " + td.getRoot());
        System.out.println();

        td = Paths.get("c:/Eigene Dateien/workspace/java/testdatei.txt");
        System.out.println("Path: " + td);
        System.out.println("FileName: " + td.getFileName());
        for (int i = 0; i < td.getNameCount(); i++)
            System.out.println("Name: " + td.getName(i));
        System.out.println("Parent: " + td.getParent());
        System.out.println("Root: " + td.getRoot());
    }
}
