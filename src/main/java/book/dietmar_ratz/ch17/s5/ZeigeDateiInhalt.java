package book.dietmar_ratz.ch17.s5;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ZeigeDateiInhalt {

    public static void main(String[] args) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(args[0]));
            String s;
            while ((s = br.readLine()) != null) {
                System.out.println(s);
            }
        } catch (ArrayIndexOutOfBoundsException a) {
            System.out.println("Kein Dateiname angegeben!");
        } catch (IOException ioe) {
            System.out.println("Fehler beim Lesen ...");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ioe2) {
                ioe2.printStackTrace();
            }
        }
    }
}
