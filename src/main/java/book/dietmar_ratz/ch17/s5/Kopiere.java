package book.dietmar_ratz.ch17.s5;

import java.io.*;

public class Kopiere {

    public static void main(String[] args) {
        BufferedReader br = null;
        PrintWriter pw = null;
        try {
            br = new BufferedReader(new FileReader(args[0]));
            pw = new PrintWriter(new FileWriter(args[1]));
            String s;
            while ((s = br.readLine()) != null) {
                pw.println(s);
            }
        } catch (ArrayIndexOutOfBoundsException a) {
            System.out.println("Keine zwei Dateinamen angegeben!");
        } catch (IOException ioe) {
            System.out.println("Fehler beim Lesen/Schreiben ...");
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
                if (pw != null) {
                    pw.close();
                }
            } catch (IOException ioe2) {
                ioe2.printStackTrace();
            }
        }
    }
}
