package book.dietmar_ratz.ch17.s5;

import java.io.*;

public class KopiereTWR {

    public static void main(String[] args) {
        try (BufferedReader br = new BufferedReader(
                new FileReader(args[0]));
             PrintWriter pw = new PrintWriter(
                     new FileWriter(args[1]))) {
            String s;
            while ((s = br.readLine()) != null) {
                pw.println(s);
            }
        } catch (ArrayIndexOutOfBoundsException a) {
            System.out.println("Kein Dateiname angegeben!");
        } catch (IOException ioe) {
            System.out.println("Fehler beim Lesen/Schreiben ...");
        }
    }
}
