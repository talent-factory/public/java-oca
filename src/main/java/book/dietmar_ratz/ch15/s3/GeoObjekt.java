package book.dietmar_ratz.ch15.s3;

import java.awt.*;

public interface GeoObjekt {

    void drehen(double phi);
    // dreht das Objekt um den Winkel phi

    void zeichnen(Graphics g, int xNull, int yNull);
    // zeichnet das Objekt auf der Zeichenebene
    // xNull und yNull sind die Koordinaten des Ursprungs
    // (Nullpunkts) des verwendeten Koordinatensystems
}
