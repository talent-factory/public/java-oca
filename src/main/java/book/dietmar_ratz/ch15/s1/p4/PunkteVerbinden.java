package book.dietmar_ratz.ch15.s1.p4;

import javax.swing.*;
import java.awt.*;

public class PunkteVerbinden extends JFrame {

    Container c;           // Container dieses Frames
    Zeichenbrett z;        // Zeichenbrett zum Linienmalen

    public PunkteVerbinden() { // Konstruktor
        c = getContentPane();      // Container bestimmen
        z = new Zeichenbrett();    // Zeichenbrett erzeugen
        c.add(z);                  // und dem Frame hinzufuegen
    }

    public static void main(String[] args) {
        PunkteVerbinden fenster = new PunkteVerbinden();
        fenster.setTitle("Punkte verbinden");
        fenster.setSize(250, 200);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
