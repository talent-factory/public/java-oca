package book.dietmar_ratz.ch15.s1.p3;

import javax.swing.*;
import java.awt.*;

public class Zeichnung extends JFrame {

    Container c;           // Container dieses Frames
    ZeichenPanel z;        // Zeichnung auf dem Zeichen-Panel

    public Zeichnung() {   // Konstruktor
        c = getContentPane();
        z = new ZeichenPanel(); // Erzeuge neue Zeichnung
        c.add(z);               // und fuege sie dem Frame hinzu
    }

    public static void main(String[] args) { // main-Methode
        Zeichnung fenster = new Zeichnung();
        fenster.setTitle("Zeichnung");
        fenster.setSize(200, 200);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
