package book.dietmar_ratz.ch06;

import ch03.Adresse;

/**
 * Erweiterung von Adresse um Telefon- und Faxnummer.
 */
public class FaxAdresse extends Adresse {
    public String telefon;
    public String fax;
}
