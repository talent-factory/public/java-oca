package book.dietmar_ratz.ch04.s1.p4;

import stdlib.IOTools;

public class Doppel {

    public static void main(String[] args) {
        // Lies die zu verdoppelnde Zahl ein
        int wert = IOTools.readInteger("Zahl: ");
        // Kopiere den Wert in eine neue Variable
        int wert2 = wert;
        // Verdopple den Wert
        wert2 = 2 * wert2;

        // Ausgabe
        System.out.println("Die Zahl " + wert +
                " ergibt verdoppelt " +
                wert2 + ".");
    }
}
