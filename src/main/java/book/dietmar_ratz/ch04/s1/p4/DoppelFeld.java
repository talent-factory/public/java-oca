package book.dietmar_ratz.ch04.s1.p4;

import stdlib.IOTools;

@SuppressWarnings("UnnecessaryLocalVariable")
public class DoppelFeld {

    public static void main(String[] args) {
        // Erzeuge Feld und lies Werte ein
        int n = IOTools.readInteger("Wie viele Zahlen ? ");
        int[] werte = new int[n];

        for (int i = 0; i < werte.length; i++) {
            werte[i] = IOTools.readInteger("Zahl Nr. " + i + ": ");
        }

        // Kopiere Feld und verdopple die Eintraege
        int[] werte2 = werte;

        for (int i = 0; i < werte2.length; i++) {
            werte2[i] = 2 * werte2[i];
        }

        // Ausgabe
        for (int i = 0; i < n; i++) {
            System.out.println("Die Zahl " + werte[i] +
                    " ergibt verdoppelt " +
                    werte2[i] + ".");
        }
    }
}
