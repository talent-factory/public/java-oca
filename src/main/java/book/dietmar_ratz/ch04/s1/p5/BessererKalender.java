package book.dietmar_ratz.ch04.s1.p5;

import stdlib.IOTools;

public class BessererKalender {

    public static void main(String[] args) {
        // Ein Feld mit je einem Element fuer jede Stunde
        String[] termine = new String[24];

        for (int i = 0; i < termine.length; i++) {
            termine[i] = "";
        }

        // Das Hauptprogramm in einer Schleife
        boolean fertig = false;

        while (!fertig) {
            // Zuerst ein Bildschirmmenue
            System.out.println("1 = Neuer Eintrag");
            System.out.println("2 = Termine ausgeben");
            System.out.println("3 = Programm beenden");

            int auswahl = IOTools.readInteger("Ihre Wahl:");

            // Nun eine Fallunterscheidung
            switch (auswahl) {
                case 1: // Termine eingeben
                    int nummer = IOTools.readInteger("Wie viel Uhr?");

                    if (nummer < 0 || nummer > 23) {
                        System.out.println("Eingabefehler!");
                        break;
                    }

                    String eingabe = IOTools.readLine("Termin:");
                    termine[nummer] = eingabe;   // Termin einordnen
                    break;
                case 2: // Termine ausgeben
                    for (int i = 0; i < 24; i++) {
                        System.out.println(i + " Uhr: " + termine[i]);
                    }
                    break;
                case 3: // Programm beenden
                    fertig = true;
                    break;
                default: // Falsche Zahl eingegeben
                    System.out.println("Eingabefehler!");
            }
        }
    }
}
