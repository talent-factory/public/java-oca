package book.dietmar_ratz.ch04.s1.p3;

import stdlib.IOTools;

public class Swap {

    public static void main(String[] args) {
        // Wie viele Werte sollen eingelesen werden?
        int n = IOTools.readInteger("Wie viele Werte? ");

        // Lege ein Feld mit genau der eingegebenen Anzahl Werten an
        int[] werte = new int[n];

        // Lies die Werte von der Tastatur ein
        for (int i = 0; i < werte.length; i++) {
            n = IOTools.readInteger("Wert Nr. " + i + ": ");
            werte[i] = n;
        }

        // Gib die Werte verkehrt herum aus
        System.out.println("Und nun verkehrt herum");

        for (int i = 0; i < werte.length; i++) {
            System.out.println("Wert Nr. " + i + ": "
                    + werte[werte.length - 1 - i]);
        }
    }
}
