package book.dietmar_ratz.ch04.s1.p6;

import stdlib.IOTools;

@SuppressWarnings("ExplicitArrayFilling")
public class MonatsKalender {

    public static void main(String[] args) {
        // Ein zweidimensionales Feld fuer die Tage und Stunden
        String[][] termine = new String[31][];

        // Initialisiere die einzelnen Tage
        for (int i = 0; i < termine.length; i++) {
            termine[i] = new String[24];

            for (int j = 0; j < termine[i].length; j++) {
                termine[i][j] = "";
            }
        }

        // Das Hauptprogramm in einer Schleife
        boolean fertig = false;

        while (!fertig) {
            // Zuerst ein Bildschirmmenue
            System.out.println("1 = Neuer Eintrag");
            System.out.println("2 = Termine ausgeben");
            System.out.println("3 = Programm beenden");
            int auswahl = IOTools.readInteger("Ihre Wahl:");

            // Nun eine Fallunterscheidung
            switch (auswahl) {
// Termine eingeben
                case 1 -> {
                    int tag = IOTools.readInteger("Welcher Tag?");
                    if (tag < 1 || tag > 31) {
                        System.out.println("Eingabefehler!");
                        break;
                    }
                    int nummer = IOTools.readInteger("Wie viel Uhr?");
                    if (nummer < 0 || nummer > 23) {
                        System.out.println("Eingabefehler!");
                        break;
                    }
                    String eingabe = IOTools.readLine("Termin:");
                    termine[tag - 1][nummer] = eingabe;  // Termin einordnen
                }
// Termine ausgeben
                case 2 -> {
                    int t = IOTools.readInteger("Welcher Tag?");
                    if (t < 1 || t > 31) {
                        System.out.println("Eingabefehler!");
                        break;
                    }
                    for (int i = 0; i < termine[t - 1].length; i++)
                        System.out.println(i + " Uhr: " + termine[t - 1][i]);
                }
// Programm beenden
                case 3 -> fertig = true;
// Falsche Zahl eingegeben
                default -> System.out.println("Eingabefehler!");
            }
        }
    }
}
