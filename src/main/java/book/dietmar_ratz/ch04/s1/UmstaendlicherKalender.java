package book.dietmar_ratz.ch04.s1;

import stdlib.IOTools;

@SuppressWarnings("DuplicatedCode")
public class UmstaendlicherKalender {
  
  public static void main(String[] args) {
    // Fuer jede Stunde eine Variable
    String termin_00="";
    String termin_01="";
    String termin_02="";
    String termin_03="";
    String termin_04="";
    String termin_05="";
    String termin_06="";
    String termin_07="";
    String termin_08="";
    String termin_09="";
    String termin_10="";
    String termin_11="";
    String termin_12="";
    String termin_13="";
    String termin_14="";
    String termin_15="";
    String termin_16="";
    String termin_17="";
    String termin_18="";
    String termin_19="";
    String termin_20="";
    String termin_21="";
    String termin_22="";
    String termin_23="";
    // Das Hauptprogramm in einer Schleife
    boolean fertig=false;
    while (!fertig) {
      // Zuerst ein Bildschirmmenue
      System.out.println("1 = Neuer Eintrag");
      System.out.println("2 = Termine ausgeben");
      System.out.println("3 = Programm beenden");
      int auswahl= IOTools.readInteger("Ihre Wahl:");
      // Nun eine Fallunterscheidung
      // Termin einordnen
      switch (auswahl) {
// Termine eingeben
        case 1 -> {
          int nummer = IOTools.readInteger("Wie viel Uhr?");
          if (nummer < 0 | nummer > 23) {
            System.out.println("Eingabefehler!");
            break;
          }
          String eingabe = IOTools.readLine("Termin:");
          switch (nummer) {
            case 0 -> termin_00 = eingabe;
            case 1 -> termin_01 = eingabe;
            case 2 -> termin_02 = eingabe;
            case 3 -> termin_03 = eingabe;
            case 4 -> termin_04 = eingabe;
            case 5 -> termin_05 = eingabe;
            case 6 -> termin_06 = eingabe;
            case 7 -> termin_07 = eingabe;
            case 8 -> termin_08 = eingabe;
            case 9 -> termin_09 = eingabe;
            case 10 -> termin_10 = eingabe;
            case 11 -> termin_11 = eingabe;
            case 12 -> termin_12 = eingabe;
            case 13 -> termin_13 = eingabe;
            case 14 -> termin_14 = eingabe;
            case 15 -> termin_15 = eingabe;
            case 16 -> termin_16 = eingabe;
            case 17 -> termin_17 = eingabe;
            case 18 -> termin_18 = eingabe;
            case 19 -> termin_19 = eingabe;
            case 20 -> termin_20 = eingabe;
            case 21 -> termin_21 = eingabe;
            case 22 -> termin_22 = eingabe;
            case 23 -> termin_23 = eingabe;
          }
        }
// Termine ausgeben
        case 2 -> {
          System.out.println("0 Uhr: " + termin_00);
          System.out.println("1 Uhr: " + termin_01);
          System.out.println("2 Uhr: " + termin_02);
          System.out.println("3 Uhr: " + termin_03);
          System.out.println("4 Uhr: " + termin_04);
          System.out.println("5 Uhr: " + termin_05);
          System.out.println("6 Uhr: " + termin_06);
          System.out.println("7 Uhr: " + termin_07);
          System.out.println("8 Uhr: " + termin_08);
          System.out.println("9 Uhr: " + termin_09);
          System.out.println("10 Uhr: " + termin_10);
          System.out.println("11 Uhr: " + termin_11);
          System.out.println("12 Uhr: " + termin_12);
          System.out.println("13 Uhr: " + termin_13);
          System.out.println("14 Uhr: " + termin_14);
          System.out.println("15 Uhr: " + termin_15);
          System.out.println("16 Uhr: " + termin_16);
          System.out.println("17 Uhr: " + termin_17);
          System.out.println("18 Uhr: " + termin_18);
          System.out.println("19 Uhr: " + termin_19);
          System.out.println("20 Uhr: " + termin_20);
          System.out.println("21 Uhr: " + termin_21);
          System.out.println("22 Uhr: " + termin_22);
          System.out.println("23 Uhr: " + termin_23);
        }
// Programm beenden
        case 3 -> fertig = true;
// Falsche Zahl eingegeben
        default -> System.out.println("Eingabefehler!");
      }
    }
  }
}
    
