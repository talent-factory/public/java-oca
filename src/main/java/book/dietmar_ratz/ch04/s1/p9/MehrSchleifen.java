package book.dietmar_ratz.ch04.s1.p9;

@SuppressWarnings("ForLoopReplaceableByForEach")
public class MehrSchleifen {

    public static void main(String[] args) {
        // Zweidimensionale Matrix mit Zeilen unterschiedlicher
        // Laenge (hier speziell eine Dreiecksmatrix)
        int[][] matrix = {{1},
                {2, 3},
                {4, 5, 6},
                {7, 8, 9, 10}};

        // Summation der Elemente mit traditioneller Schleifen-Notation
        int summe = 0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                summe = summe + matrix[i][j];
            }
        }

        System.out.println("Summe: " + summe);

        // Summation der Elemente mit vereinfachter Schleifen-Notation
        summe = 0;
        for (int[] zeile : matrix) {
            for (int element : zeile) {
                summe = summe + element;
            }
        }

        System.out.println("Summe: " + summe);
    }
}
