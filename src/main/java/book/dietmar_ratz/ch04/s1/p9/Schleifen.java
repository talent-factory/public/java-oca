package book.dietmar_ratz.ch04.s1.p9;

@SuppressWarnings("ForLoopReplaceableByForEach")
public class Schleifen {

    public static void main(String[] args) {
        int[] werte = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        // Traditionelle Schleifen-Notation
        int summe = 0;
        for (int i = 0; i < werte.length; i++) {
            summe = summe + werte[i];
        }

        System.out.println("Summe: " + summe);

        // Neue, vereinfachte Schleifen-Notation
        summe = 0;
        for (int x : werte) {
            summe = summe + x;
        }

        System.out.println("Summe: " + summe);
    }
}
