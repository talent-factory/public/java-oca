package book.dietmar_ratz.ch04.s2.p4;

import ch03.Adresse;
import stdlib.IOTools;

@SuppressWarnings("DuplicatedCode")
public class AdressBuch_v2 {

    public static void main(String[] args) { // Hauptprogramm
        // Benoetigte Variablen
        Adresse adr0 = new Adresse();
        Adresse adr1 = new Adresse();
        Adresse adr = adr0;
        boolean fertig = false;

        // Starte das Programm mit einer huebschen Ausgabe
        System.out.println("================");
        System.out.println("Adressverwaltung");
        System.out.println("================");

        // Schleifenbeginn
        while (!fertig) {
            // Menue
            System.out.println(" ");
            System.out.println("1 = Adresseingabe");
            System.out.println("2 = Adressausgabe");
            System.out.println("3 = Aktuelle Adresse wechseln");
            System.out.println("4 = Programm beenden");

            int auswahl = IOTools.readInteger("Ihre Wahl:");

            // Fallunterscheidung
            switch (auswahl) {
// Adresse eingeben
                case 1 -> {
                    adr.name = IOTools.readLine("Name      : ");
                    adr.strasse = IOTools.readLine("Strasse   : ");
                    adr.hausnummer = IOTools.readInteger("Hausnummer: ");
                    adr.wohnort = IOTools.readLine("Wohnort   : ");
                    adr.postleitzahl = IOTools.readInteger("PLZ       : ");
                    adr.mail = IOTools.readLine("E-Mail    : ");
                    adr.kommentar = IOTools.readLine("Kommentar : ");
                }
// Adresse ausgeben
                case 2 -> {
                    System.out.println(adr.name);
                    System.out.println(adr.strasse + " " +
                            adr.hausnummer);
                    System.out.println(adr.postleitzahl + " " +
                            adr.wohnort);
                    System.out.println("E-Mail: " + adr.mail);
                    System.out.println("Kommentar: " + adr.kommentar);
                }
// Adresse wechseln
                case 3 -> {
                    int nr = IOTools.readInteger("Welche Adresse (0 oder 1)?");
                    if (nr == 0) {
                        adr = adr0;
                    } else if (nr == 1) {
                        adr = adr1;
                    } else {
                        System.out.println("Eingabefehler!");
                    }
                }
// Programm beenden
                case 4 -> fertig = true;
// Falsche Zahl eingegeben
                default -> System.out.println("Eingabefehler!");
            }
        } // Schleifenende
    } // Ende des Hauptprogramms
} // Ende des Programms
