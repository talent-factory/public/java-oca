package book.dietmar_ratz.ch04.s2.p8;

/**
 * Diese Klasse realisiert eine Adresse einer
 * natuerlichen Person
 */
public class Adresse {
    public String name;
    public String strasse;
    public int hausnummer;
    public int postleitzahl;
    public String wohnort;
    public String mail;
    public String kommentar;
}
