package book.dietmar_ratz.ch04.s2.p8;

public class Referenzen {

    public static void main(String[] args) {
        int matrNr = 4711; //  Hier Ihre Matrikelnummer eintragen!
        Komponente p, q;
        int i;
        p = new Komponente();
        p.ref = null;
        p.wert = matrNr % 10;
        matrNr = matrNr / 10;
        for (i = 2; i <= 3; i++) {
            q = new Komponente();
            q.ref = p;
            p = q;
            p.wert = matrNr % 10;
            matrNr = matrNr / 10;
        }
        for (i = 1; i <= 3; i++) {
            System.out.print(p.wert);
            p = p.ref;
        }
    }
}
