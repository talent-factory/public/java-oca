package book.dietmar_ratz.ch04.s2.p3;

import stdlib.IOTools;
import ch03.Adresse;

@SuppressWarnings("DuplicatedCode")
public class AdressBuch_v1 {

    public static void main(String[] args) { // Hauptprogramm
        // Benoetigte Variablen
        Adresse adr = new Adresse();
        boolean fertig = false;

        // Starte das Programm mit einer huebschen Ausgabe
        System.out.println("================");
        System.out.println("Adressverwaltung");
        System.out.println("================");

        // Schleifenbeginn
        while (!fertig) {
            // Menue
            System.out.println(" ");
            System.out.println("1 = Adresseingabe");
            System.out.println("2 = Adressausgabe");
            System.out.println("3 = Programm beenden");

            int auswahl = IOTools.readInteger("Ihre Wahl:");

            // Fallunterscheidung
            switch (auswahl) {
                // Adresse eingeben
                case 1 -> {
                    adr.name = IOTools.readLine("Name      : ");
                    adr.strasse = IOTools.readLine("Strasse   : ");
                    adr.hausnummer = IOTools.readInteger("Hausnummer: ");
                    adr.wohnort = IOTools.readLine("Wohnort   : ");
                    adr.postleitzahl = IOTools.readInteger("PLZ       : ");
                    adr.mail = IOTools.readLine("E-Mail    : ");
                    adr.kommentar = IOTools.readLine("Kommentar : ");
                }
                // Adresse ausgeben
                case 2 -> {
                    System.out.println(adr.name);
                    System.out.println(adr.strasse + " " + adr.hausnummer);
                    System.out.println(adr.postleitzahl + " " + adr.wohnort);
                    System.out.println("E-Mail: " + adr.mail);
                    System.out.println("Kommentar: " + adr.kommentar);
                }
                // Programm beenden
                case 3 -> fertig = true;
                // Falsche Zahl eingegeben
                default -> System.out.println("Eingabefehler!");
            }
        }
    }
}
