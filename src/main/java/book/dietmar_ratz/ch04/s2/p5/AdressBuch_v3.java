package book.dietmar_ratz.ch04.s2.p5;

import stdlib.IOTools;
import ch03.Adresse;

public class AdressBuch_v3 {

    public static void main(String[] args) { // Hauptprogramm
        // Benoetigte Variablen
        Adresse[] adressen = new Adresse[20];
        Adresse adr;
        boolean fertig = false;

        // Initialisiere das Feld
        for (int i = 0; i < adressen.length; i++) {
            adressen[i] = new Adresse();
        }

        adr = adressen[0];

        // Starte das Programm mit einer huebschen Ausgabe
        System.out.println("================");
        System.out.println("Adressverwaltung");
        System.out.println("================");

        // Schleifenbeginn
        while (!fertig) {
            // Menue
            System.out.println(" ");
            System.out.println("1 = Adresseingabe");
            System.out.println("2 = Adressausgabe");
            System.out.println("3 = Aktuelle Adresse wechseln");
            System.out.println("4 = Programm beenden");

            int auswahl = IOTools.readInteger("Ihre Wahl:");

            // Fallunterscheidung
            switch (auswahl) {
// Adresse eingeben
                case 1 -> {
                    adr.name = IOTools.readLine("Name      : ");
                    adr.strasse = IOTools.readLine("Strasse   : ");
                    adr.hausnummer = IOTools.readInteger("Hausnummer: ");
                    adr.wohnort = IOTools.readLine("Wohnort   : ");
                    adr.postleitzahl = IOTools.readInteger("PLZ       : ");
                    adr.mail = IOTools.readLine("E-Mail    : ");
                    adr.kommentar = IOTools.readLine("Kommentar : ");
                }
// Adresse ausgeben
                case 2 -> {
                    System.out.println(adr.name);
                    System.out.println(adr.strasse + " " +
                            adr.hausnummer);
                    System.out.println(adr.postleitzahl + " " +
                            adr.wohnort);
                    System.out.println("E-Mail: " + adr.mail);
                    System.out.println("Kommentar: " + adr.kommentar);
                }
// Adresse wechseln
                case 3 -> {
                    int nr = IOTools.readInteger("Welche Adresse (0 bis 19)?");
                    if (nr >= 0 && nr < adressen.length) {
                        adr = adressen[nr];
                    } else {
                        System.out.println("Eingabefehler!");
                    }
                }
// Programm beenden
                case 4 -> fertig = true;
// Falsche Zahl eingegeben
                default -> System.out.println("Eingabefehler!");
            }
        } // Schleifenende
    } // Ende des Hauptprogramms
} // Ende des Programms
