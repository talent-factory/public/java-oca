package book.dietmar_ratz.ch12.s4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Erzeuge ein einfaches Swing-Fenster mit einem Textlabel
 */
public class FrameMitText extends JFrame {

    Container c;          // Container dieses Frames
    JButton beschriftung;  // Label, das im Frame erscheinen soll

    public FrameMitText() {  // Konstruktor
        // Bestimme die Referenz auf den eigenen Container
        c = getContentPane();
        // Setze das Layout
        c.setLayout(new FlowLayout());
        // Erzeuge das Labelobjekt mit Uebergabe des Labeltextes
        beschriftung = new JButton("Label-Text im Frame");
        beschriftung.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               System.out.println("Knopf gedrückt.");
            }
        });
        // Fuege das Label dem Frame hinzu
        c.add(beschriftung);
    }

    public static void main(String[] args) {
        FrameMitText fenster = new FrameMitText();
        fenster.setTitle("Frame mit Text im Label");
        fenster.setSize(300, 150);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
