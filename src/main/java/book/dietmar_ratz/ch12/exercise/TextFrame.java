package book.dietmar_ratz.ch12.exercise;

/*
 * "Grundkurs Programmieren in Java (8. Auflage, 2018)"
 * 2003-2018, Carl Hanser Verlag
 * Loesungsvorschlag zu Aufgabe 12.2 (Version 2.0)
 * (c) 2003-2018 D. Ratz, D. Schulmeister-Zimolong, D. Seese, J. Wiesenberger
 *
 */

import book.dietmar_ratz.ch12.s4.FrameMitText;

import javax.swing.*;

public class TextFrame {
    public static void main(String[] args) {
        try {
            FrameMitText fenster = new FrameMitText();
            fenster.setTitle(args[0]);
            fenster.setSize(Integer.parseInt(args[1]), Integer.parseInt(args[2]));
            fenster.setVisible(true);
            fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        } catch (ArrayIndexOutOfBoundsException aioube) {
            System.out.println("Aufruf:  java TextFrame <Titel> <Breite> <Hoehe>");
        } catch (NumberFormatException nfe) {
            System.out.println("Breiten- und Hoehen-Angabe muessen ganzzahlig sein!");
        }
    }
}
