package book.dietmar_ratz.ch12.exercise;

/*
 * "Grundkurs Programmieren in Java (8. Auflage, 2018)"
 * 2003-2018, Carl Hanser Verlag
 * Loesungsvorschlag zu Aufgabe 12.1 (Version 2.0)
 * (c) 2003-2018 D. Ratz, D. Schulmeister-Zimolong, D. Seese, J. Wiesenberger
 *
 */

import book.dietmar_ratz.ch12.s4.FrameMitText;
import stdlib.IOTools;

import javax.swing.*;

public class ZweiFrames {
    public static void main(String[] args) {
        FrameMitText fenster = new FrameMitText();
        fenster.setTitle("Das erste Fenster");
        fenster.setSize(400, 250);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String s = IOTools.readLine("Neuer Fenstertitel: ");
        fenster.setTitle(s);

        int b = IOTools.readInteger("Neue Fensterbreite: ");
        int h = IOTools.readInteger("Neue Fensterhoehe: ");
        fenster.setSize(b, h);

        IOTools.readLine("Fenster unsichtbar machen mit Eingabetaste: ");
        fenster.setVisible(false);

        IOTools.readLine("Fenster wieder sichtbar machen mit Eingabetaste: ");
        fenster.setVisible(true);

        IOTools.readLine("Fenster auf die Koordinaten (300,10) verschieben mit Eingabetaste: ");
        fenster.setLocation(300, 10);

        IOTools.readLine("Noch ein Fenster erzeugen mit Eingabetaste: ");
        FrameMitText fenster2 = new FrameMitText();
        fenster2.setTitle("Das zweite Fenster");
        fenster2.setSize(300, 150);
        fenster2.setVisible(true);
        System.out.println("done.");
    }
}
