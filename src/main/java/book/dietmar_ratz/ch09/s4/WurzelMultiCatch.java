package book.dietmar_ratz.ch09.s4;

import stdlib.IOTools;

public class WurzelMultiCatch {

    public static String compute(int x) {
        if (x < 0) {
            return null;
        }
        return "Resultat: " + Math.sqrt(x) + 1 / x;
    }

    public static void main(String[] args) {
        try {
            int x = IOTools.readInt("x = ");
            String erg = compute(x);
            assert erg != null;
            System.out.println(erg.toUpperCase());
        } catch (ArithmeticException | NullPointerException e) {
            System.out.println("Fehler, da x <= 0");
        }
    }
}
