package book.dietmar_ratz.ch09.s2.p8;

public class Exueb8 {

    /**
     * Bestimme eine Zufallszahl zwischen 0 und 0.5
     */
    public static double gibZufallszahlBisEinhalb()
            throws Exception {
        double res = Math.random();
        if (res > 0.5)
            throw new Exception("Zahl zu gross");
        return res;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
        double zahl;
        // Bestimme eine Zufallszahl zwischen 0 und 0.5
        try {
            zahl = gibZufallszahlBisEinhalb();
        }
        // Falls etwas schief geht (Exception)
        // verwende die Zahl 0.5
        catch (Exception e) {
            zahl = 0.5;
        }
        // gib die Zahl auf dem Bildschirm aus
        System.out.println(zahl);
    }

}
