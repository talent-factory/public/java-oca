package book.dietmar_ratz.ch09.s2.p5;

import book.dietmar_ratz.ch09.s2.p3.PingException;
import book.dietmar_ratz.ch09.s2.p3.PingPongException;
import book.dietmar_ratz.ch09.s2.p3.PongException;
import stdlib.IOTools;

/**
 * Ein einfaches Programm, das das Verhalten beim Fangen von
 * Exceptions testet, die in einer verwandtschaftlichen
 * Beziehung stehen.
 */
@SuppressWarnings("DuplicatedCode")
public class PingPong_v7 {

    /**
     * Wirft einen PingException
     */
    public static void Ping() throws PingException {
        System.out.println("Ping aufgerufen!");
        throw new PingException();
    }

    /**
     * Wirft einen PongException
     */
    public static void Pong() throws PongException {
        System.out.println("Pong aufgerufen!");
        throw new PongException();
    }

    /**
     * Wirft einen PingPongException
     */
    public static void PingPong() throws PingPongException {
        System.out.println("PingPong aufgerufen!");
        throw new PingPongException();
    }

    /**
     * Fragt den Benutzer, welche Exception ausgeloest werden
     * soll, und ruft die entsprechende Methode auf.
     */
    public static void Hauptprogramm()
            throws PingPongException {
        System.out.println("1 = Ping");
        System.out.println("2 = Pong");
        System.out.println("3 = PingPong");
        System.out.println();
        int choice = IOTools.readInteger("Ihre Wahl:");
        switch (choice) {
            case 1 -> Ping();
            case 2 -> Pong();
            case 3 -> PingPong();
            default -> System.out.println("Eingabefehler!");
        }
    }

    /**
     * Hauptprogramm.
     */
    public static void main(String[] args)
            throws PingPongException {
        System.out.println("Betrete kritischen Bereich.");
        try {
            Hauptprogramm();
            System.out.println("Verlasse kritischen Bereich.");
        } catch (PongException e) {
            System.out.println("PONG");
        }
    }

}
