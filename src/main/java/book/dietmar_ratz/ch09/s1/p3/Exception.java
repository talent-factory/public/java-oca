package book.dietmar_ratz.ch09.s1.p3;

import stdlib.IOTools;

public class Exception {

    public static void main(String[] args) {
        int a = IOTools.readInteger("a=");
        int b = IOTools.readInteger("b=");
        try {
            System.out.println("a/b=" + (a / b));
        } catch (ArithmeticException e) {
            System.out.println("Achtung -- Sie haben eine " +
                    "ArithmeticException ausgeloest!");
            System.out.println("Es gab folgendes Problem: " +
                    e.getMessage());
            System.out.println("Seien Sie in Zukunft etwas " +
                    "vorsichtiger!");
        }
    }
}
