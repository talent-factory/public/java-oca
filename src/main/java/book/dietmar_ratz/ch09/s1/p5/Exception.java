package book.dietmar_ratz.ch09.s1.p5;

import stdlib.IOTools;

public class Exception {

    public static void main(String[] args) {
        int a = IOTools.readInteger("a=");
        int b = IOTools.readInteger("b=");
        System.out.println("a/b=" + (a / b));
    }
}
