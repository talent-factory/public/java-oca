package book.dietmar_ratz.ch09.s1.p4;

import stdlib.IOTools;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@SuppressWarnings("DuplicatedCode")
public class LiesAusDatei_v3 {

    public static void main(String[] args) {
        try {
            // Lies den Dateinamen ein:
            String dateiname = IOTools.readString("Dateiname: ");
            // Oeffne die Datei zum Lesen:
            FileReader dateileser = new FileReader(dateiname);
            // Lies alle Zeichen aus der Datei ein (read liefert int)
            // bis das Dateiende erreicht wird (signalisiert durch -1)
            // und gib sie (wieder als Buchstabe) auf den Bildschirm aus:
            System.out.println("In der Datei " + dateiname + " steht");
            while (true) {
                int gelesen = dateileser.read();
                if (gelesen == -1)
                    break;
//                check(gelesen);
                System.out.print((char) gelesen);
            }
        } catch (FileNotFoundException fe) {
            System.out.println("Diese Datei existiert nicht!");
        } catch (IOException ie) {
            System.out.println("Fehler beim Lesen: " + ie.getMessage());
        }
    }
}
