package book.dietmar_ratz.ch09.s1.p6;

import java.io.FileReader;
import java.io.IOException;

public class Exception {

    public static void main(String[] args) {
        try {
            FileReader f = new FileReader("Exueb7.java");
            while (true) {
                int c = f.read();
                if (c < 0) return;
                System.out.print((char) c);
            }
        } catch (IOException ignored) {
        }
    }

}
