package book.dietmar_ratz.ch09;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exueb1 {

    static final String dir = "/Users/daniel/GitRepository/java-oca/src/main/resources/";

    static final String fileName = dir + "numbers.txt";

    public static void main(String[] args) throws IOException {
        FileReader f = new FileReader(fileName);
        while (true) {
            int c = f.read();
            if (c < 0)
                return;
            System.out.println((char) c);
        }
    }
}
