package book.dietmar_ratz.ch09.s3.p1;

import stdlib.IOTools;

public class AssertionTest {

    public static double kehrwert(double x) {
        assert x != 0 : "/ by 0";
        return 1 / x;
    }

    public static void main(String[] summand) {
        double x = IOTools.readDouble("x = ");
        try {
            System.out.println(kehrwert(x));
        } catch (AssertionError e) {
            System.out.println(e.getMessage());
        }
    }
}
