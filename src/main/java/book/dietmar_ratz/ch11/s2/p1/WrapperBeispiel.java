package book.dietmar_ratz.ch11.s2.p1;

public class WrapperBeispiel {

    public static void main(String[] args) {
        Object[] etwas = new Object[4];

        etwas[0] = Boolean.TRUE;
        etwas[1] = 3.1415;
        etwas[2] = 'x';
        etwas[3] = 12;

        for (int i = 0; i < 4; i++)
            System.out.println(etwas[i]);

        etwas[2] = 987654321L;

        for (int i = 0; i < 4; i++)
            System.out.println(etwas[i]);
    }
}
