package book.dietmar_ratz.ch11.s2.p3;

public class Summiere {

    public static void main(String[] summand) {
        int i = 0;
        double ergebnis = 0;
        try {
            for (i = 0; i < summand.length; i++)
                ergebnis = ergebnis + Double.parseDouble(summand[i]);
            System.out.println("Ergebnis: " + ergebnis);
        } catch (NumberFormatException e) {
            System.out.println(i + 1 + ". Summand unzulaessig!");
        }
    }
}
