package book.dietmar_ratz.ch11.s2.p4;

@SuppressWarnings("all")
public class AutoBoxingDangers {

    public static void main(String[] args) {
        Double u = 1.0;
        Double v = 1.0;
        System.out.println(u == v);
        Integer i = 126;
        Integer j = 126;
        System.out.println(i == j);
        i++;
        j++;
        System.out.println(i == j);
        i++;
        j++;
        System.out.println(i == j);
        i = Integer.valueOf("1");
        j = Integer.valueOf("1");
        System.out.println(i == j);
    }
}
