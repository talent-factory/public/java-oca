package book.dietmar_ratz.ch11.s2.p4;

@SuppressWarnings("all")
public class Boxing {

    public static void main(String[] args) {

        Object[] w = new Object[2];
        Integer a = 3;
        Double b = 5.0;
        w[0] = a;
        w[1] = b;

        double x = 7 + 4 * a - b / 8;

        System.out.println(x);
    }
}
