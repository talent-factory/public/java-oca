package book.dietmar_ratz.ch11.s5.p4;

import stdlib.IOTools;

import java.util.Calendar;

public class CalStoppuhr {
  
    public static void main(String[] args) {
        // Auf Betaetigen der Eingabetaste warten
        IOTools.readLine("Stoppuhr starten mit Eingabetaste!");
        // Aktuellen Zeitpunkt im Calendar-Objekt start festhalten
        Calendar start = Calendar.getInstance();
        // Zeitpunkt ausgeben
        System.out.println("Startzeitpunkt: "
                + start.get(Calendar.HOUR_OF_DAY) + ":"
                + start.get(Calendar.MINUTE) + ":"
                + start.get(Calendar.SECOND) + ":"
                + start.get(Calendar.MILLISECOND));
        System.out.println();
        // Statusmeldung anzeigen
        System.out.println("Die Stoppuhr laeuft ...");
        System.out.println();
        // Auf Betaetigen der Eingabetaste warten
        IOTools.readLine("Stoppuhr anhalten mit Eingabetaste!");
        // Aktuellen Zeitpunkt im Calendar-Objekt stopp festhalten
        Calendar stopp = Calendar.getInstance();
        // Zeitpunkt ausgeben
        System.out.println("Stoppzeitpunkt: "
                + stopp.get(Calendar.HOUR_OF_DAY) + ":"
                + stopp.get(Calendar.MINUTE) + ":"
                + stopp.get(Calendar.SECOND) + ":"
                + stopp.get(Calendar.MILLISECOND));
        System.out.println();
        // Laufzeit als Differenz von stopp und start bestimmen
        long laufzeit = stopp.getTimeInMillis() - start.getTimeInMillis();
        // Laufzeit ausgeben
        System.out.println("Gesamtlaufzeit: " + laufzeit + " ms");
        // Laufzeit als Zeitpunkt darstellen
        stopp.setTimeInMillis(laufzeit);
        // Zeitpunkt ausgeben
        System.out.println("Gesamtlaufzeit (min:sec:ms): "
                + stopp.get(Calendar.MINUTE) + ":"
                + stopp.get(Calendar.SECOND) + ":"
                + stopp.get(Calendar.MILLISECOND));
    }
}
