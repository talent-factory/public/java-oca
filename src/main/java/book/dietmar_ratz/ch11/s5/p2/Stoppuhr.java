package book.dietmar_ratz.ch11.s5.p2;

import stdlib.IOTools;

import java.util.Date;

public class Stoppuhr {

    public static void main(String[] args) {
        // Auf Betaetigen der Eingabetaste warten
        IOTools.readLine("Stoppuhr starten mit Eingabetaste!");
        // Aktuellen Zeitpunkt im Date-Objekt start festhalten
        Date start = new Date();
        // Zeitpunkt ausgeben
        System.out.println("Startzeitpunkt: " + start);
        System.out.println();
        // Statusmeldung anzeigen
        System.out.println("Die Stoppuhr laeuft ...");
        System.out.println();
        // Auf Betaetigen der Eingabetaste warten
        IOTools.readLine("Stoppuhr anhalten mit Eingabetaste!");
        // Aktuellen Zeitpunkt im Date-Objekt stopp festhalten
        Date stopp = new Date();
        // Zeitpunkt ausgeben
        System.out.println("Stoppzeitpunkt: " + stopp);
        System.out.println();
        // Laufzeit als Differenz von stopp und start bestimmen
        long laufzeit = stopp.getTime() - start.getTime();
        // Laufzeit ausgeben
        System.out.println("Gesamtlaufzeit: " + laufzeit + " ms");
    }
}
