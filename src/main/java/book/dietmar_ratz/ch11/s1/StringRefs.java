package book.dietmar_ratz.ch11.s1;

@SuppressWarnings("all")
public class StringRefs {

    public static void main(String[] args) {
        String s1 = "JavaBuch";
        String s2 = "JavaBuch";
        String s3 = "Java" + "Buch";
        String s4 = new String("JavaBuch");
        String s5 = "Java";
        String s6 = s5 + "Buch";
        System.out.println(s1 == "JavaBuch");
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s1 == s4);
        System.out.println(s1 == s6);
        System.out.println(s5 == "Java");
        System.out.println(s1.equals(s6));
    }
}
