package book.dietmar_ratz.ch11.s6;

import java.text.SimpleDateFormat;
import java.util.Date;

@SuppressWarnings("SuspiciousDateFormat")
public class MyDateFormats {

    // Verschiedene Formate als Konstanten definieren
    public static final SimpleDateFormat
            eins = new SimpleDateFormat("dd.MM.yyyy' um 'HH:mm:ss:S"),
            zwei = new SimpleDateFormat("EE, MMM d, ''yy"),
            drei = new SimpleDateFormat("H:mm"),
            vier = new SimpleDateFormat("H' Uhr und 'm' Minuten'"),
            fuen = new SimpleDateFormat("d. MMMM yyyy'  'HH:mm"),
            sech = new SimpleDateFormat("EE, d. MMM yyyy HH:mm:ss"),
            sieb = new SimpleDateFormat("yyMMddHHmmssS");

    // Methode zur formatierten Ausgabe
    public static void println(Date d, SimpleDateFormat f) {
        System.out.println(f.format(d));
    }

    // Einige Tests
    public static void main(String[] args) {
        Date d = new Date();
        println(d, eins);
        println(d, zwei);
        println(d, drei);
        println(d, vier);
        println(d, fuen);
        println(d, sech);
        println(d, sieb);
    }
}
