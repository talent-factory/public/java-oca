package book.dietmar_ratz.ch11.s6;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("all")
public class MyStandardDateFormats {

    // Verschiedene Stanard-Formate als Konstanten definieren
    public static final DateFormat
            eins = DateFormat.getDateInstance(),
            zwei = DateFormat.getDateInstance(DateFormat.SHORT),
            drei = DateFormat.getDateInstance(DateFormat.LONG,
                    Locale.FRANCE),
            vier = DateFormat.getTimeInstance(),
            fuen = DateFormat.getTimeInstance(DateFormat.LONG),
            sech = DateFormat.getTimeInstance(DateFormat.FULL,
                    Locale.US),
            sieb = DateFormat.getDateTimeInstance(),
            acht = DateFormat.getDateTimeInstance(DateFormat.SHORT,
                    DateFormat.SHORT),
            neun = DateFormat.getDateTimeInstance(DateFormat.LONG,
                    DateFormat.LONG,
                    Locale.ITALY);

    // Methode zur formatierten Ausgabe
    public static void println(Date d, DateFormat f) {
        System.out.println(f.format(d));
    }

    // Einige Tests
    public static void main(String[] args) {
        try {
            Date d = acht.parse("11.11.2022 11:11");
            println(d, eins);
            println(d, zwei);
            println(d, drei);
            println(d, vier);
            println(d, fuen);
            println(d, sech);
            println(d, sieb);
            println(d, acht);
            println(d, neun);
        } catch (ParseException pe) {
            System.out.println(pe);
        }
    }
}
