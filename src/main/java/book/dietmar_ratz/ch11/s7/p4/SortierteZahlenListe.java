package book.dietmar_ratz.ch11.s7.p4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

@SuppressWarnings("ForLoopReplaceableByForEach")
class SortierteZahlenListe {
    /**
     * Methode zur Ausgabe der Listenelemente
     */
    public static void printList(List<Double> l) {
        System.out.println("Die Liste enthaelt die Elemente");
        for (Iterator<Double> i = l.iterator(); i.hasNext(); )
            System.out.print(i.next() + "   ");
        System.out.println();
        System.out.println();
    }

    /**
     * Aufbau und Modifikation einer Liste
     */
    public static void main(String[] args) {
        List<Double> l = new ArrayList<>();
        l.add(2.2);
        l.add(1.1);
        l.add(3.3);
        l.add(0.0);
        l.add(7.7);
        l.add(3.3);
        printList(l);
        Collections.sort(l);
        printList(l);
        System.out.println("Index des Elements mit Wert 0.0: " +
                Collections.binarySearch(l, 0.0));
        System.out.println("Index des Elements mit Wert 2.2: " +
                Collections.binarySearch(l, 2.2));
        System.out.println("Index des Elements mit Wert 5.5: " +
                Collections.binarySearch(l, 5.5));
    }
}

