package book.dietmar_ratz.ch11.s7.p4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

@SuppressWarnings("ForLoopReplaceableByForEach")
class ZahlenListe {

    /**
     * Methode zur Ausgabe von Infos ueber eine Collection
     */
    public static void printInfo(Collection<Double> c) {
        System.out.println("Die Liste enthaelt " + c.size()
                + " Elemente");
        System.out.println("Ist 3.3 in der Liste enthalten? " +
                c.contains(3.3));
        System.out.println("Alle Elemente der Liste:");
        for (Iterator<Double> i = c.iterator(); i.hasNext(); )
            System.out.print(i.next() + "   ");
        System.out.println();
        System.out.println();
    }

    /**
     * Aufbau und Modifikation einer Collection
     */
    public static void main(String[] args) {
        Collection<Double> c = new ArrayList<>();
        c.add(1.1);
        c.add(2.2);
        c.add(3.3);
        c.add(0.0);
        c.add(3.3);
        c.add(4.4);
        printInfo(c);
        c.remove(3.3);
        c.remove(0.0);
        c.remove(4.4);
        printInfo(c);
    }
}
