package book.dietmar_ratz.ch11.s7.p3;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

@SuppressWarnings("all")
class ZahlenMenge {
    /**
     * Methode zur Ausgabe von Infos ueber eine Collection
     */
    public static void printInfo(Collection<Double> c) {
        System.out.println("Die Menge enthaelt " + c.size()
                + " Elemente");
        System.out.println("Ist 3.3 in der Menge enthalten? " +
                c.contains(3.3));
        System.out.println("Alle Elemente der Menge:");
        for (Iterator<Double> i = c.iterator(); i.hasNext(); )
            System.out.print(i.next() + "   ");
        System.out.println();
        System.out.println();
    }

    /**
     * Aufbau und Modifikation einer Collection
     */
    public static void main(String[] args) {
        Collection<Double> c = new HashSet<>();
        c.add(1.1);
        c.add(2.2);
        c.add(3.3);
        c.add(0.0);
        c.add(3.3);
        c.add(4.4);
        printInfo(c);
        c.remove(3.3);
        c.remove(0.0);
        c.remove(4.4);
        printInfo(c);
    }
}
