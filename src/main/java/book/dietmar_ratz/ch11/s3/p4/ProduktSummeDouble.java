package book.dietmar_ratz.ch11.s3.p4;

public class ProduktSummeDouble {

    public static void main(String[] args) {
        double[] x = new double[6];
        double[] y = new double[6];
        double p, s;
        x[0] = 1e16;
        x[1] = 0.1223;
        x[2] = 1e14;
        x[3] = 1e15;
        x[4] = 3.0;
        x[5] = -1e12;
        y[0] = 1e20;
        y[1] = 2.0;
        y[2] = -1e22;
        y[3] = 1e9;
        y[4] = 0.2111;
        y[5] = 1e12;
        s = 0;
        System.out.println("s = " + s);
        for (int i = 0; i < 6; i++) {
            p = x[i] * y[i];
            System.out.println("  + " + p + " liefert");
            s = s + p;
            System.out.println("s = " + s);
        }
    }
}
