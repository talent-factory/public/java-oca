package book.dietmar_ratz.ch11.s3.p4;

import java.math.BigDecimal;

public class ProduktSummeBigDec {

    public static void main(String[] args) {
        BigDecimal[] x = new BigDecimal[6];
        BigDecimal[] y = new BigDecimal[6];
        BigDecimal p, s;
        x[0] = new BigDecimal("1e16");
        x[1] = new BigDecimal("0.1223");
        x[2] = new BigDecimal("1e14");
        x[3] = new BigDecimal("1e15");
        x[4] = new BigDecimal("3.0");
        x[5] = new BigDecimal("-1e12");
        y[0] = new BigDecimal("1e20");
        y[1] = new BigDecimal("2.0");
        y[2] = new BigDecimal("-1e22");
        y[3] = new BigDecimal("1e9");
        y[4] = new BigDecimal("0.2111");
        y[5] = new BigDecimal("1e12");
        s = new BigDecimal("0");
        System.out.println("s = " + s);
        for (int i = 0; i < 6; i++) {
            p = x[i].multiply(y[i]);
            System.out.println("  + " + p + " liefert");
            s = s.add(p);
            System.out.println("s = " + s);
        }
    }
}
