package book.dietmar_ratz.ch11.s3.p1;

import java.math.BigInteger;

public class QuadrierungBigInt {

    public static void main(String[] args) {
        BigInteger zahl = new BigInteger("16");
        System.out.println("Zahl vor der Quadrierung: " + zahl);
        for (int i = 1; i <= 5; i++) {
            zahl = zahl.multiply(zahl);
            System.out.println("Zahl nach " + i + ". Quadrierung: " + zahl);
        }
    }
}
