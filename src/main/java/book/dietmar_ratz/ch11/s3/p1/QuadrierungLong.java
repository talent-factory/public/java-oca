package book.dietmar_ratz.ch11.s3.p1;

public class QuadrierungLong {

    public static void main(String[] args) {
        long zahl = 16;
        System.out.println("Zahl vor der Quadrierung: " + zahl);
        for (int i = 1; i <= 5; i++) {
            zahl = zahl * zahl;
            System.out.println("Zahl nach " + i + ". Quadrierung: " + zahl);
        }
    }
}
