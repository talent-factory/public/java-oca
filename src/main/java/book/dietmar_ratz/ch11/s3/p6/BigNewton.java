package book.dietmar_ratz.ch11.s3.p6;

import stdlib.IOTools;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigNewton {

    public static BigDecimal zwei = new BigDecimal("2");

    public static BigDecimal f(BigDecimal x) {       // berechnet f(x)
        return (x.multiply(x)).subtract(zwei);         // x*x - 2
    }

    public static BigDecimal fstrich(BigDecimal x) { // berechnet f'(x)
        return x.multiply(zwei);                       // x*2
    }

    public static void main(String[] args) {
        System.out.println("Wurzel-2-Berechnung mit Newton-Verfahren");
        String start = IOTools.readString("Startwert fuer Iteration: ");
        int stellen = IOTools.readInteger("Gewuenschte Stellenzahl: ");

        BigDecimal xAlt, xNeu = new BigDecimal(start);
        BigDecimal fx, fsx;

        int k = 0;
        System.out.println("x = " + xNeu);
        do {                                           // Newton-Iteration
            k = k + 1;
            xAlt = xNeu;
            fx = f(xAlt);
            fsx = fstrich(xAlt);
            xNeu = xAlt.subtract(fx.divide(fsx, stellen, RoundingMode.HALF_DOWN));
            System.out.println("x = " + xNeu);
        } while (!(xNeu.compareTo(xAlt) == 0) && (k < 100));
    }
}
