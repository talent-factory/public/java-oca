package book.dietmar_ratz.ch11.s4.p3;

public class Ausgaben {

    public static void main(String[] args) {
        String format = "Ergebnis der Division: % 15.10e\n";
        System.out.printf(format, 3.5 / 7.1);
        System.out.printf(format, -2 / 3.0);
        System.out.printf(format, 123597.3 / 4);
    }
}
