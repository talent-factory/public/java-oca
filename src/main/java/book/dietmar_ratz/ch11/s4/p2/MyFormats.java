package book.dietmar_ratz.ch11.s4.p2;

import java.text.DecimalFormat;

@SuppressWarnings("DuplicatedCode")
public class MyFormats {

    // Verschiedene Formate als Konstanten definieren
    public static final DecimalFormat
            kurz = new DecimalFormat("0.0"),
            lang = new DecimalFormat("00000.00000000000"),
            euro = new DecimalFormat("EUR #0.00"),
            wiss = new DecimalFormat("#.#E000"),
            naja = new DecimalFormat("#,###,##0.00"),
            proz = new DecimalFormat("Anteilig: 0.0%");

    // Methode zur formatierten Ausgabe
    public static void println(double d, DecimalFormat f) {
        System.out.println(f.format(d));
    }

    // Einige Tests
    public static void main(String[] args) {
        double x = 987.654321;
        double y = 0.12345678;
        println(x, kurz);
        println(x, lang);
        println(x, euro);
        println(x, wiss);
        println(x, naja);
        println(x, proz);
        println(y, kurz);
        println(y, lang);
        println(y, euro);
        println(y, wiss);
        println(y, naja);
        println(y, proz);
    }
}
