package book.dietmar_ratz.ch05.s2.p1;

public class Unendlichkeit {

    // UNTERPROGRAMM(E)
    public static void unterprogramm() {
        System.out.println("Unterprogramm aufgerufen...");
        unterprogramm();           // rufe dich selbst auf
    }

    // UNSER HAUPTPROGRAMM
    public static void main(String[] args) {
        unterprogramm();
    }
}
