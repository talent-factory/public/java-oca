package book.dietmar_ratz.ch05.s2.p2;

public class IndirekteRekursion {

    public static void m() {
        System.out.println("m ruft a!");
        a();
    }

    public static void a() {
        System.out.println("a ruft b!");
        b();
    }

    public static void b() {
        System.out.println("b ruft m!");
        m();
    }

    public static void main(String[] args) {
        m();
    }
}
