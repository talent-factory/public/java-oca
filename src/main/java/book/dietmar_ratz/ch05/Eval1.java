package book.dietmar_ratz.ch05;

import stdlib.IOTools;

public class Eval1 {

    public static void main(String[] args) {  // Hauptprogramm
        int n = IOTools.readInteger("n=");      // lies n ein
        double x = IOTools.readDouble("x=");    // lies x ein
        double produkt = 1.0;                   // Berechnung der Potenz
        for (int i = 0; i < 2 * n; i++)             // ...
            produkt = produkt * x;                // abgeschlossen
        double f_x_n = produkt + n * n - n * x;     // Berechnung von f
        System.out.println("f(x,n)=" + f_x_n);  // Ergebnisausgabe
    }
}
