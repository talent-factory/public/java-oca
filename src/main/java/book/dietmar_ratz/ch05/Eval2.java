package book.dietmar_ratz.ch05;

import stdlib.IOTools;

public class Eval2 {

    public static void main(String[] args) {  // Hauptprogramm
        int n = IOTools.readInteger("n=");      // lies n ein
        double l = IOTools.readDouble("l=");    // lies l ein
        double r = IOTools.readDouble("r=");    // lies r ein

        double produkt = 1.0;                   // Berechnung der Potenz
        for (int i = 0; i < 2 * n; i++)             // ...
            produkt = produkt * l;                // abgeschlossen
        double f_l_n = produkt + n * n - n * l;     // Berechnung von f
        System.out.println("f(l,n)=" + f_l_n);  // Ergebnisausgabe

        produkt = 1.0;                          // Berechnung der Potenz
        for (int i = 0; i < 2 * n; i++)             // ...
            produkt = produkt * r;                // berechnet
        double f_r_n = produkt + n * n - n * r;     // Berechnung von f
        System.out.println("f(r,n)=" + f_r_n);  // Ergebnisausgabe

        double m = (l + r) / 2.0;               // Mittelpunkt
        produkt = 1.0;                          // Berechnung der Potenz
        for (int i = 0; i < 2 * n; i++)             // ...
            produkt = produkt * m;                // abgeschlossen
        double f_m_n = produkt + n * n - n * m;     // Berechnung von f(M,n)
        System.out.println("f(m,n)=" + f_m_n);  // Ergebnisausgabe

        double mw = (f_l_n + f_r_n + f_m_n) / 3;  // Mittelwert
        System.out.println("Mittelwert=" + mw); // Ergebnisausgabe
    }
}
