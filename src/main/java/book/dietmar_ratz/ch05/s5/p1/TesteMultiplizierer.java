package book.dietmar_ratz.ch05.s5.p1;

public class TesteMultiplizierer {

    public static void main(String[] args) {
        Multiplizierer m7 = new Multiplizierer();
        Multiplizierer m8 = new Multiplizierer();
        m7.faktor = 7;
        m8.faktor = 8;
        System.out.println("7 * 5 = " + m7.mul(5));
        System.out.println("8 * 5 = " + m8.mul(5));
    }
}
