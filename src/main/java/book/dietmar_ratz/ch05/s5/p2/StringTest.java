package book.dietmar_ratz.ch05.s5.p2;

@SuppressWarnings("all")
public class StringTest {

    public static void main(String[] args) {
        String s1 = "Weihnachten";
        String s2 = "Veihnachten";
        String s3 = "Xeihnachten";
        String s4 = "WEIHNACHTEN";

        System.out.println(s1);
        System.out.println(s1.charAt(4));
        System.out.println(s1.compareTo(s1));
        System.out.println(s1.compareTo(s2));
        System.out.println(s1.compareTo(s3));
        System.out.println(s1.endsWith("ten"));
        System.out.println(s1.equals(s2));
        System.out.println(s1.equalsIgnoreCase(s4));
        System.out.println(s1.indexOf("n"));
        System.out.println(s1.indexOf("ach"));
        System.out.println(s1.length());
        System.out.println(s1.replace('e', 'E'));
        System.out.println(s1.startsWith("Weih"));
        System.out.println(s1.substring(3));
        System.out.println(s1.substring(3, 7));
        System.out.println(s1.toLowerCase());
        System.out.println(s1.toUpperCase());
        System.out.println(String.valueOf(1.5e2));
    }
}
