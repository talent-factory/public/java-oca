package book.dietmar_ratz.ch05.s1.p7;

@SuppressWarnings("all")
public class AufrufTest3 {

    // UNTERPROGRAMM(E)
    public static void unterprogramm(int[] n) {
        n[0] = n[0] * 5;                     // veraendere Parameter
        System.out.println("n[0]=" + n[0]);  // gib diesen aus
    }

    public static int[] arraycopy(int[] n) {
        int[] ergebnis = new int[n.length];  // erzeuge ein neues Feld
        // derselben Laenge wie n
        for (int i = 0; i < n.length; i++)     // kopiere alle Elemente
            ergebnis[i] = n[i];                // in das neue Feld
        return ergebnis;
    }

    // UNSER HAUPTPROGRAMM
    public static void main(String[] args) {
        int n[] = {7};                       // Startwert fuer n[0]
        System.out.println("n[0]= " + n[0]); // gib diesen aus
        unterprogramm(arraycopy(n));         // Unterprogramm
        System.out.println("n[0]= " + n[0]); // gib n erneut aus
    }
}
