package book.dietmar_ratz.ch05.s1.p7;

public class AufrufTest2 {

    // UNTERPROGRAMM
    public static void unterprogramm(int[] n) {
        n[0] = n[0] * 5;                     // veraendere Parameter
        System.out.println("n[0]=" + n[0]);  // gib diesen aus
    }

    // UNSER HAUPTPROGRAMM
    public static void main(String[] args) {
        int[] n = {7};                       // Startwert fuer n[0]
        System.out.println("n[0]= " + n[0]); // gib diesen aus
        unterprogramm(n);                    // Unterprogrammaufruf
        System.out.println("n[0]=" + n[0]);  // gib n erneut aus
    }
}
