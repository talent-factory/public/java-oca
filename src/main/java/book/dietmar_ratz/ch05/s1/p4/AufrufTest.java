package book.dietmar_ratz.ch05.s1.p4;

public class AufrufTest {

    // UNTERPROGRAMM
    public static void unterprogramm(int n) { // n als formaler
        n = n * 5;                     // Parameter wird veraendert
        System.out.println("n=" + n);  // und ausgegeben
    }

    // UNSER HAUPTPROGRAMM
    public static void main(String[] args) {
        int n = 7;                     // Startwert fuer lokales n
        System.out.println("n= " + n); // wird ausgegeben
        unterprogramm(n);              // Unterprogrammaufruf
        System.out.println("n= " + n); // n wird erneut ausgegeben
    }
}
