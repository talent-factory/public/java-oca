package book.dietmar_ratz.ch05.s1.p8;

@SuppressWarnings("unused")
public class VerdeckenTest {

    static int a = 1, b = 2, c = 3;    // Klassenvariablen

    static int m(int a) {             // Formale Variable
        int b = 20;                      // Lokale Variable
        System.out.println("a = " + a);   //
        System.out.println("b = " + b);   //
        System.out.println("c = " + c);   //
        return 100;
    }

    public static void main(String[] args) {
        int a = 1000;                        // Lokale Variable
        System.out.println("a = " + a);      //
        System.out.println("b = " + b);      //
        System.out.println("m(c)= " + m(c)); //
    }
}
