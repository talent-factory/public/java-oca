package book.dietmar_ratz.ch05.s1.p6;

public class Argumente {

    public static int summiere(int... werte) {
        int summe = 0;
        for (int x : werte)
            summe = summe + x;
        return summe;
    }

    public static void main(String[] args) {
        System.out.println("summiere(1,2): " + summiere(1, 2));
        System.out.println("summiere(1,2,3,4,5): " + summiere(1, 2, 3, 4, 5));
        int[] feld = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        System.out.println("summiere(feld): " + summiere(feld));
    }
}
