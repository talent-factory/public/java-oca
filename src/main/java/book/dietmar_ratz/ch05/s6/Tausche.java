package book.dietmar_ratz.ch05.s6;

@SuppressWarnings("all")
public class Tausche {

    public static void tauscheA(int x, int y) {
        int h = x;
        x = y;
        y = h;
    }

    public static void tauscheB(int[] a, int i, int j) {
        int h = a[i];
        a[i] = a[j];
        a[j] = h;
    }

    public static void main(String[] args) {
        int[] feld = {100, 200, 300, 400};
        int i;
        for (i = 0; i < 4; i++)
            System.out.print(feld[i] + " ");
        System.out.println();
        System.out.println("tauscheA");
        tauscheA(feld[1], feld[2]);
        for (i = 0; i < 4; i++)
            System.out.print(feld[i] + " ");
        System.out.println();
        System.out.println("tauscheB");
        tauscheB(feld, 1, 2);
        for (i = 0; i < 4; i++)
            System.out.print(feld[i] + " ");
        System.out.println();
    }
}
