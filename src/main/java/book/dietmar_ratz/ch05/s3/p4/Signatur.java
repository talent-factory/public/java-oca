package book.dietmar_ratz.ch05.s3.p4;

public class Signatur {

    public static void hoppla(long x, double y, double z) {
        System.out.println("ldd");
    }

    public static void hoppla(long x, long y, double z) {
        System.out.println("lld");
    }

    public static void hoppla(long x, long y, long z) {
        System.out.println("lll");
    }

    public static void hoppla(double x, long y, double z) {
        System.out.println("dld");
    }

    public static void main(String[] args) {
        long a = 333;
        double b = 4.44;
        hoppla(a, a, a);  // Aufruf 1
        // hoppla(b, b, b);      // Aufruf 2
        hoppla(a, a, b);  // Aufruf 3
        // hoppla(b, b, a);      // Aufruf 4
        hoppla(a, b, a);  // Aufruf 5
        hoppla(a, b, b);  // Aufruf 6
        hoppla(b, a, b);  // Aufruf 7
        hoppla(b, a, a);  // Aufruf 8
    }
}
