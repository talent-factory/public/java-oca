package book.dietmar_ratz.ch05.s3.p1;

/**
 * Mit dieser Hilfsklasse zeigen wir auf, wie die main() Methode einer
 * anderen Klasse aufgerufen werden kann.
 */
public class HilfsKlasse {

    public static void main(String[] args) {

        /*
         * Erstellen eines String-Arrays, welcher anschliessend als
         * Argument zum Aufruf der main() Methode verwendet werden
         * kann.
         */
        String[] string = {"Daniel", "Peter", "Senften"};
        GrussWortErweitert.main(string);
    }
}
