package book.dietmar_ratz.ch05.s3.p1;

import static stdlib.IOTools.readString;

public class GrussWortErweitert {

    public static void main(String[] args) {

        if (args.length == 0) {
            System.out.println("Bist du stumm?");
            System.exit(0);
        }

        if (args.length == 1) {
            System.out.println("Hallo, " + args[0] + "!");
            String nachname = readString("Wie ist dein Nachname? ");
            System.out.println("Dein Nachname: " + nachname);
            System.exit(0);
        }

        System.out.print("Hallo, ");
        for (int i = 0; i < args.length - 1; i++) {
            System.out.print(args[i] + " ");
        }

        System.out.println(args[args.length - 1] + " ist ein schöner Nachname");
    }

}
