package book.dietmar_ratz.ch05.s4.p3;

import static java.lang.Math.*;

public class StatischeImports {

    public static void main(String[] args) {
        double x = 3.12345;
        double y = (sin(x) + cos(x) * sqrt(x)) / (x * sinh(x) - sqrt(x));
    }
}
