package book.dietmar_ratz.ch05.s4.p1;

public class TesteMethoden {

    public static void main(String[] args) {
        int x = 5;
        MeineMethoden.mal5nehmen(x);
        System.out.println(x + "! = " + MeineMethoden.fakultaet(x));
    }
}
