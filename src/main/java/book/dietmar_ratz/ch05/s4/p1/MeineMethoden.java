package book.dietmar_ratz.ch05.s4.p1;

public class MeineMethoden {

    public static void mal5nehmen(int n) {
        n = n * 5;
        System.out.println("n = " + n);
    }

    public static int fakultaet(int n) {
        if (n == 0)
            return 1;
        for (int i = n - 1; i > 0; i--)
            n = n * i;
        return n;
    }

    public static void main(String[] args) {
        int n = 7;
        mal5nehmen(n);
        System.out.println("n! = " + fakultaet(n));
    }
}
