package book.dietmar_ratz.ch07.s4.p1;

/**
 * Diese Klasse simuliert einen Studenten
 */
public class Student {

  /* ==========
     KONSTANTEN
     ==========
   */

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Mathematik
     */
    public static final int MATHEMATIKSTUDIUM = 1;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Informatik
     */
    public static final int INFORMATIKSTUDIUM = 2;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Architektur
     */
    public static final int ARCHITEKTURSTUDIUM = 3;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * der Wirtschaftswissenschaften
     */
    public static final int WIRTSCHAFTLICHESSTUDIUM = 4;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Biologie
     */
    public static final int BIOLOGIESTUDIUM = 5;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Geschichte
     */
    public static final int GESCHICHTSSTUDIUM = 6;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Germanistik
     */
    public static final int GERMANISTIKSTUDIUM = 7;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Politologie
     */
    public static final int POLITOLOGIESTUDIUM = 8;

    /**
     * Diese Konstante symbolisiert das Studienfach
     * Physik
     */
    public static final int PHYSIKSTUDIUM = 9;

  /* =========
     VARIABLEN
     =========
   */

    /**
     * Zaehlt die Anzahl der erzeugten Studentenobjekte
     */
    private static int zaehler = 0;

    /**
     * Der Name des Studenten
     */
    private String name;

    /**
     * Die Matrikelnummer des Studenten
     */
    private int nummer;

    /**
     * Studienfach des Studenten
     */
    private int fach;

  /* =========
     METHODEN
     =========
   */

    /**
     * Gib den Namen des Studenten als String zurueck
     */
    public String getName() {
        return this.name;
    }


    /**
     * Setze den Namen des Studenten auf einen bestimmten Wert
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gib die Matrikelnummer des Studenten als Integer zurueck
     */
    public int getNummer() {
        return nummer;
    }


    /**
     * Setze die Matrikelnummer des Studenten auf einen bestimmten Wert
     */
    public void setNummer(int n) {
        int alteNummer = nummer;
        nummer = n;
        if (!validateNummer()) { // neue Nummer ist nicht gueltig
            nummer = alteNummer;
        }
    }


    /**
     * Gib das Studienfach des Studenten als Integer zurueck
     */
    public int getFach() {
        return fach;
    }


    /**
     * Setze das Studienfach des Studenten auf einen bestimmten
     * Wert
     */
    public void setFach(int fach) {
        this.fach = fach;
    }


    /**
     * Pruefe die Matrikelnummer des Studenten
     * auf ihre Gueltigkeit
     */
    public boolean validateNummer() {
        return
                (nummer >= 10000 && nummer <= 99999 && nummer % 2 != 0);
    }


    /**
     * Gib eine textuelle Beschreibung dieses Studenten aus
     */
    public String toString() {
        String res = name + " (" + nummer + ")\n";

        return switch (fach) {
            case MATHEMATIKSTUDIUM -> res + "  studiert Mathematik.";
            case INFORMATIKSTUDIUM -> res + "  studiert Informatik.";
            case ARCHITEKTURSTUDIUM -> res + "  sollte sich fuer Gebaeude interessieren.";
            case WIRTSCHAFTLICHESSTUDIUM -> res + "  findet man oft in der Wirtschaft.";
            case BIOLOGIESTUDIUM -> res + "  kennt sich auch im Tierreich aus.";
            case GESCHICHTSSTUDIUM -> res + "   sollte Geschichte nicht mit Geschichten " +
                    "verwechseln.";
            case GERMANISTIKSTUDIUM -> res + "  wollte frueher Medizin studieren.";
            case POLITOLOGIESTUDIUM -> res + "  kommt bestimmt einmal in den Bundestag.";
            case PHYSIKSTUDIUM -> res + "  studiert schon relativ lange Physik.";
            default -> res + "  fragt sich, was er/sie studieren soll.";
        };
    }


    /**
     * Gib die Zahl der erzeugten Studentenobjekte zurueck
     */
    public static int getZaehler() {
        return zaehler;
    }


    /**
     * Erzeugt ein neues Studentenobjekt
     */
    public static Student createStudent() {
        return new Student();
    }

  /* =============
     KONSTRUKTOREN
     =============
   */

    /**
     * Argumentloser Konstruktor
     */
    public Student() {
        zaehler++;
    }

}
  
