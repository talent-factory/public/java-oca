package book.dietmar_ratz.ch07.s3.p2;

/**
 * Diese Klasse simuliert einen Studenten
 */
@SuppressWarnings("unused")
public class Student {

  /* ==========
     KONSTANTEN
     ==========
   */

    /**
     * Konstante fuer das Studienfach Mathematik
     */
    public static final int MATHEMATIKSTUDIUM = 1;

    /**
     * Konstante fuer das Studienfach Informatik
     */
    public static final int INFORMATIKSTUDIUM = 2;

    /**
     * Konstante fuer das Studienfach Architektur
     */
    public static final int ARCHITEKTURSTUDIUM = 3;

    /**
     * Konstante fuer das Studienfach Wirtschaftswissenschaften
     */
    public static final int WIRTSCHAFTLICHESSTUDIUM = 4;

    /**
     * Konstante fuer das Studienfach Biologie
     */
    public static final int BIOLOGIESTUDIUM = 5;

    /**
     * Konstante fuer das Studienfach Geschichte
     */
    public static final int GESCHICHTSSTUDIUM = 6;

    /**
     * Konstante fuer das Studienfach Germanistik
     */
    public static final int GERMANISTIKSTUDIUM = 7;

    /**
     * Konstante fuer das Studienfach Politologie
     */
    public static final int POLITOLOGIESTUDIUM = 8;

    /**
     * Konstante fuer das Studienfach Physik
     */
    public static final int PHYSIKSTUDIUM = 9;

  /* =========
     VARIABLEN
     =========
   */

    /**
     * Zaehlt die Anzahl der erzeugten Studentenobjekte
     */
    private static int zaehler = 0;

    /**
     * Der Name des Studenten
     */
    private String name;

    /**
     * Die Matrikelnummer des Studenten
     */
    private int nummer;

    /**
     * Studienfach des Studenten
     */
    private int fach;

  /* =========
     METHODEN
     =========
   */

    /**
     * Gib den Namen des Studenten als String zurueck
     */
    public String getName() {
        return this.name;
    }


    /**
     * Setze den Namen des Studenten auf einen bestimmten Wert
     */
    public void setName(String name) {
        this.name = name;
    }


    /**
     * Gib die Matrikelnummer des Studenten als Integer zurueck
     */
    public int getNummer() {
        return nummer;
    }


    /**
     * Setze die Matrikelnummer des Studenten auf einen bestimmten Wert
     */
    public void setNummer(int n) {
        int alteNummer = nummer;
        nummer = n;
        if (!validateNummer()) { // neue Nummer ist nicht gueltig
            nummer = alteNummer;
        }
    }


    /**
     * Gib das Studienfach des Studenten als Integer zurueck
     */
    public int getFach() {
        return fach;
    }


    /**
     * Setze das Studienfach des Studenten auf einen bestimmten Wert
     */
    public void setFach(int fach) {
        this.fach = fach;
    }


    /**
     * Pruefe die Matrikelnummer des Studenten
     * auf ihre Gueltigkeit
     */
    public boolean validateNummer() {
        return
                (nummer >= 10000 && nummer <= 99999 && nummer % 2 != 0);
    }


    /**
     * Gib eine textuelle Beschreibung dieses Studenten zurueck
     */
    public String toString() {
        String res = name + " (" + nummer + ")\n";
        return switch (fach) {
            case MATHEMATIKSTUDIUM -> res + "  ein Mathestudent " +
                    "(oder auch zwei, oder drei).";
            case INFORMATIKSTUDIUM -> res + "  ein Informatikstudent.";
            case ARCHITEKTURSTUDIUM -> res + "  angehender Architekt.";
            case WIRTSCHAFTLICHESSTUDIUM -> res + "  ein Wirtschaftswissenschaftler.";
            case BIOLOGIESTUDIUM -> res + "  Biologie ist seine Staerke.";
            case GESCHICHTSSTUDIUM -> res + "   sollte Geschichte nicht mit Geschichten " +
                    "verwechseln.";
            case GERMANISTIKSTUDIUM -> res + "  wird einmal Germanist gewesen tun sein.";
            case POLITOLOGIESTUDIUM -> res + "  kommt bestimmt einmal in den Bundestag.";
            case PHYSIKSTUDIUM -> res + "  studiert schon relativ lange Physik.";
            default -> res + "  keine Ahnung, was der Mann studiert.";
        };
    }


    /**
     * Gib die Zahl der erzeugten Studentenobjekte zurueck
     */
    public static int getZaehler() {
        return zaehler;
    }


    /**
     * Erzeugt ein neues Studentenobjekt
     */
    public static Student createStudent() {
        zaehler++; // erhoehe den Zaehler
        return new Student();
    }

}
  
