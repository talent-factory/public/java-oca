package book.dietmar_ratz.ch07.s1;

/**
 * Diese Klasse simuliert einen Studenten
 */
public class Student {

    /**
     * Der Name des Studenten
     */
    public String name;

    /**
     * Die Matrikelnummer des Studenten
     */
    public int nummer;
}
