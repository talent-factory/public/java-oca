package book.dietmar_ratz.ch07.s6;

public class Reifen {

    /**
     * Reifendruck
     */
    private final double druck;

    /**
     * Konstruktor
     */
    public Reifen(double luftdruck) {
        druck = luftdruck;
    }

    /**
     * Zugriffsfunktion für Reifendruck
     */
    public double aktuellerDruck() {
        return druck;
    }
}
