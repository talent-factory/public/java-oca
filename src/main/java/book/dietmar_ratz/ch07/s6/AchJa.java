package book.dietmar_ratz.ch07.s6;

public class AchJa {

    public int x;
    static int ach;

    int ja(int i, int j) {
        int y;
        if ((i <= 0) || (j <= 0) || (i % j == 0) || (j % i == 0)) {
            System.out.print(i + j);
            return i + j;
        } else {
            x = ja(i - 2, j);
            System.out.print(" + ");
            y = ja(i, j - 2);
            return x + y;
        }
    }

    public static void main(String[] args) {
        int n = 5, k = 2;
        AchJa so = new AchJa();
        System.out.print("ja(" + n + "," + k + ") = ");
        ach = so.ja(n, k);
        System.out.println(" = " + ach);
    }
}
