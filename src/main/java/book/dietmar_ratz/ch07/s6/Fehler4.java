package book.dietmar_ratz.ch07.s6;

public class Fehler4 {

    /**
     * Private Instanzvariable
     */
    private String name;

    /**
     * Konstruktor
     */
    public Fehler4(String nom) {
        name = nom;
    }

    /**
     * String-Ausgabe
     */
    public String toString() {
        return "Name = " + name;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
        System.out.println(new Fehler4("Testname"));
    }

}
