package book.dietmar_ratz.ch07.s6;

public class Patient {
    public String name;          // Name des Patienten
    public int alter;            // Alter (in Jahren)

    public int altersDifferenz(int alter) {
        return Math.abs(alter - this.alter);
    }
}
