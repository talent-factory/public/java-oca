package book.dietmar_ratz.ch07.s6;

public class Fehler6 {

    /**
     * Private Instanzvariable
     */
    private String name;

    /**
     * Konstruktor
     */
    public Fehler6(String nom) {
        name = nom;
    }

    /**
     * String-Ausgabe
     */
    public String toString() {
        return "Name = " + name;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
//        Fehler6 variable = new Fehler6();
//        variable.name = "Testname";
//        System.out.println(variable);
    }

}
