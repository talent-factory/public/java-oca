package book.dietmar_ratz.ch07.s6;

@SuppressWarnings("SillyAssignment")
public class Fehler1 {

    /**
     * Private Instanzvariable
     */
    private String name;

    /**
     * Konstruktor
     */
    public Fehler1(String name) {
        name = name;
    }

    /**
     * String-Ausgabe
     */
    public String toString() {
        return "Name = " + name;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
        System.out.println(new Fehler1("Testname"));
    }

}
