package book.dietmar_ratz.ch07.s6;

@SuppressWarnings("MethodNameSameAsClassName")
public class Fehler5 {

    /**
     * Private Instanzvariable
     */
    private String name;

    /**
     * Konstruktor
     */
    public void Fehler5(String name) {
        this.name = name;
    }

    /**
     * String-Ausgabe
     */
    public String toString() {
        return "Name = " + name;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
        // System.out.println(new Fehler5("Testname"));
    }

}
