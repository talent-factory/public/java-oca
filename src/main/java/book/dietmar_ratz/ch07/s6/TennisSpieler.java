package book.dietmar_ratz.ch07.s6;

public class TennisSpieler {
    public String name;                     // Name des Spielers
    public int alter;                       // Alter in Jahren

    public int altersDifferenz(int alter) {
        return Math.abs(alter - this.alter);
    }
}
