package book.dietmar_ratz.ch07.s6;

public class KlassenTest {
    public static void copy1(RefIntKlasse f, RefIntKlasse g) {
        g.x.a = f.x.a;
        g.y = f.y;
    }

    public static void copy2(RefIntKlasse f, RefIntKlasse g) {
        g.x = f.x;
        g.y = f.y;
    }

    public static void copy3(RefIntKlasse f, RefIntKlasse g) {
        g = f;
    }

    public static void main(String[] args) {
        RefIntKlasse p = new RefIntKlasse(5, 7);
        RefIntKlasse q = new RefIntKlasse(1, 2); // Ergibt das Ausgangsbild
        // HIER FOLGT NUN EINE KOPIERAKTION:
        // ***
    }
}
