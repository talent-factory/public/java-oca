package book.dietmar_ratz.ch07.s6;

public class Fehler2 {

    /**
     * Private Instanzvariable
     */
    private final String name;

    /**
     * Konstruktor
     */
    public Fehler2(String name) {
        this.name = name;
    }

    /**
     * String-Ausgabe
     */
    public String tostring() {
        return "Name = " + name;
    }

    /**
     * Hauptprogramm
     */
    public static void main(String[] args) {
        System.out.println(new Fehler2("Testname"));
    }

}
