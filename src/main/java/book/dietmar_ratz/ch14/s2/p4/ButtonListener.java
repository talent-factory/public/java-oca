package book.dietmar_ratz.ch14.s2.p4;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Eigenstaendige Listener-Klasse
 */
public class ButtonListener implements ActionListener {

    Container c;  // Referenz auf den zu beeinflussenden Container

    public ButtonListener(Container c) {
        this.c = c; // Referenz auf den zu beeinflussenden Container sichern
    }

    public void actionPerformed(ActionEvent e) {
        // Hintergrundfarbe des Containers zufaellig aendern
        float zufall = (float) Math.random();
        Color grauton = new Color(zufall, zufall, zufall);
        c.setBackground(grauton);
    }
}
