package book.dietmar_ratz.ch14.s2.p2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Farbwechsel-Klasse mit anonymer Listener-Klasse
 */
@SuppressWarnings("Convert2Lambda")
public class Farbwechsel2 extends JFrame {

    Container c;            // Container dieses Frames
    JButton button;         // Knopf

    public Farbwechsel2() { // Konstruktor
        // Container bestimmen
        c = getContentPane();
        // Button erzeugen und dem Container hinzufuegen
        button = new JButton("Hintergrundfarbe wechseln");
        c.add(button, BorderLayout.NORTH);

        // Listener-Objekt erzeugen und beim Button registrieren
        ActionListener bL = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Hintergrundfarbe des Containers zufaellig aendern
                float zufall = (float) Math.random();
                Color grauton = new Color(zufall, zufall, zufall);
                c.setBackground(grauton);
            }
        }; // Ende der anonymen Klassendefinition
        button.addActionListener(bL);
    }

    public static void main(String[] args) {
        Farbwechsel2 fenster = new Farbwechsel2();
        fenster.setTitle("Farbwechsel");
        fenster.setSize(200, 100);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
