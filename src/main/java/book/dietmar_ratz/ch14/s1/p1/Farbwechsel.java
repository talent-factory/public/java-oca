package book.dietmar_ratz.ch14.s1.p1;

import book.dietmar_ratz.ch14.s2.p4.ButtonListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Erzeuge ein Swing-Fenster mit einem Button, der in der Lage
 * ist, die Hintergrundfarbe des Frames zufaellig zu aendern
 */
public class Farbwechsel extends JFrame {

    Container c;           // Container dieses Frames
    JButton button;        // Knopf
    JTextField text;

    public Farbwechsel() { // Konstruktor
        // Container bestimmen
        c = getContentPane();
        // Button erzeugen und dem Container hinzufuegen
        button = new JButton("Hintergrundfarbe wechseln");
        text = new JTextField("", 15);

        c.add(button, BorderLayout.NORTH);
        c.add(text, BorderLayout.CENTER);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                float zufall = (float) Math.random();
                Color grauton = new Color(zufall, zufall, zufall);
                c.setBackground(grauton);
                text.setText("Zufallszahl: " + zufall);
            }
        });
    }

    public static void main(String[] args) {
        Farbwechsel fenster = new Farbwechsel();
        fenster.setTitle("Farbwechsel");
        fenster.setSize(200, 100);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

}
