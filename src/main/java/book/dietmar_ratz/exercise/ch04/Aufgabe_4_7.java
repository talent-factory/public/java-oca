package book.dietmar_ratz.exercise.ch04;

/**
 * Aufgabe 4.7 (s. 155)
 * <p>
 * Weisen Sie an der markierten Stelle der Variablen matrNr Ihre Matrikelnummer
 * zu. Falls Sie keine solche Matrikelnummer besitzen, verwenden Sie einfach
 * die letzten sechs Ziffern Ihrer Telefonnummer.
 * <p>
 * Geben Sie an, welche Ausgabe das Programm liefert. Versuchen Sie, das
 * Ergebnis ohne Zuhilfenahme des Computers zu erhalten.
 */
public class Aufgabe_4_7 {

    static class Komponente {
        public int wert;
        public Komponente ref;
    }

    @SuppressWarnings("all")
    static class Referenzen {
        public static void main(String[] args) {
            int matrNr = 304724; // Hier Ihre Matrikelnummer eintragen!
            Komponente p, q;
            int i;
            p = new Komponente();
            p.ref = null;
            p.wert = matrNr % 10;
            matrNr = matrNr / 10;
            for (i = 2; i <= 3; i++) {
                q = new Komponente();
                q.ref = p;
                p = q;
                p.wert = matrNr % 10;
                matrNr = matrNr / 10;
            }
            for (i = 1; i <= 3; i++) {
                System.out.print(p.wert);
                p = p.ref;
            }
        }
    }

}

