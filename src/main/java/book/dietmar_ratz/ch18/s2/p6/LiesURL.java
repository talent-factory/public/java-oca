package book.dietmar_ratz.ch18.s2.p6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class LiesURL {

    public static void main(String[] args) {
        try {
            URL u = new URL(args[0]);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(
                            u.openStream()));
            String zeile;
            while ((zeile = in.readLine()) != null)
                System.out.println(zeile);
            in.close();
        } catch (ArrayIndexOutOfBoundsException ae) {
            System.out.println("Aufruf: java LiesURL <URL>");
        } catch (MalformedURLException me) {
            System.out.println(args[0] + " ist keine zulaessige URL");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
