package book.dietmar_ratz.ch10.s1.p2;

import book.dietmar_ratz.ch10.s1.p1.Jahreszeit;

@SuppressWarnings("UnnecessaryLocalVariable")
public class Aufzaehlungen {

    public static void main(String[] args) {
        Jahreszeit x = Jahreszeit.HERBST;
        System.out.println(x);

        for (Jahreszeit jz : Jahreszeit.values())
            System.out.println(jz + " hat den Wert " + jz.ordinal());
    }
}
