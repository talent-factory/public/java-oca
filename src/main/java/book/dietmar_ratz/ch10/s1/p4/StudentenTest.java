package book.dietmar_ratz.ch10.s1.p4;

import book.dietmar_ratz.ch07.s4.p1.Student;

public class StudentenTest {

    public static void main(String[] args) {
        Student Peter = new Student();
        Peter.setName("Peter Honig");
        Peter.setNummer(12345);
        Peter.setFach(Student.WIRTSCHAFTLICHESSTUDIUM);
        System.out.println(Peter);
    }
}
