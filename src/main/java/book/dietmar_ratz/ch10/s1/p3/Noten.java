package book.dietmar_ratz.ch10.s1.p3;

public enum Noten {
    C, CIS, D, DIS, E, F, FIS, G, GIS, A, AIS, H;

    public boolean liegtAufSchwarzerTaste() {
        return switch (this) {
            case CIS, DIS, FIS, GIS, AIS -> true;
            default -> false;
        };
    }
}
