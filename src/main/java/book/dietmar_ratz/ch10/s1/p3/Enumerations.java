package book.dietmar_ratz.ch10.s1.p3;

public class Enumerations {
    public static void main(String[] args) {
        for (Noten n : Noten.values())
            if (n.liegtAufSchwarzerTaste())
                System.out.println(n + " liegt auf einer schwarzen Taste");
            else
                System.out.println(n + " liegt auf einer weissen Taste");
    }
}
