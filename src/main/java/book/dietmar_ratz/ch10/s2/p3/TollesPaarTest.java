package book.dietmar_ratz.ch10.s2.p3;

import book.dietmar_ratz.ch10.s2.p1.Ohrring;

public class TollesPaarTest {

    public static void main(String[] args) {
        Hemd he1 = new Hemd();
        Hemd he2 = new Hemd();
        Hose ho1 = new Hose();
        Hose ho2 = new Hose();
        Ohrring o1 = new Ohrring();
        Ohrring o2 = new Ohrring();
        TollesPaar<Hemd> p1 = new TollesPaar<>(he1, he2);
        TollesPaar<Hose> p2 = new TollesPaar<>(ho1, ho2);
//      TollesPaar<Ohrring> p3 = new TollesPaar<>(o1, o2); // unzulaessig
    }
}
