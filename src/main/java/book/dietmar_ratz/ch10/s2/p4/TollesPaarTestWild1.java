package book.dietmar_ratz.ch10.s2.p4;

import book.dietmar_ratz.ch10.s2.p3.Hose;
import book.dietmar_ratz.ch10.s2.p3.Kleidung;
import book.dietmar_ratz.ch10.s2.p3.TollesPaar;

public class TollesPaarTestWild1 {

    public static void paarAusgeben1(TollesPaar<Kleidung> tp) {
        System.out.println(tp);
    }

    public static void main(String[] args) {
        Hose ho1 = new Hose();
        Hose ho2 = new Hose();
        TollesPaar<Hose> p1 = new TollesPaar<>(ho1, ho2);
//        paarAusgeben1(p1);
    }
}
