package book.dietmar_ratz.ch10.s2.p2;

import book.dietmar_ratz.ch10.s2.p1.Ohrring;
import book.dietmar_ratz.ch10.s2.p1.Socke;

public class GenPaarTest1 {

    public static void main(String[] args) {
        Socke s1 = new Socke();
        Socke s2 = new Socke();
        GenPaar<Socke> sockenPaar = new GenPaar<>(s1, s2);
        System.out.println("1. Paar: " + sockenPaar);
        Ohrring o1 = new Ohrring();
        Ohrring o2 = new Ohrring();
        GenPaar<Ohrring> ohrringPaar = new GenPaar<>(o1, o2);
        System.out.println("2. Paar: " + ohrringPaar);
        Socke s = sockenPaar.getL();
        System.out.println(s);
    }
}
