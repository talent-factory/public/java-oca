package book.dietmar_ratz.ch10.s2.p7;

public class GPaar<T> {

    private final T l;
    private final T r;

    public GPaar(T l, T r) {
        this.l = l;
        this.r = r;
    }

    public GPaar() {
        this.l = null;
        this.r = null;
    }

    public T getL() {
        return l;
    }

    public T getR() {
        return r;
    }

    public String toString() {
        return "(l,r) = (" + l + "," + r + ")";
    }
}
