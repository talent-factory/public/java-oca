package book.dietmar_ratz.ch10.s2.p7;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
public class ListDiamondFalsch {

    public static void main(String[] args) {
        List<String> liste1 = new ArrayList<>();
        liste1.add("Hallo!");
        liste1.addAll(new ArrayList<>());
    }
}
