package book.dietmar_ratz.ch10.s2.p7;

@SuppressWarnings("DuplicatedCode")
public class GPaarDiamondDemo {

    public static void main(String[] args) {
        GPaar<Integer> paar1 = new GPaar<>(33, 3333);
        System.out.println("Paar 1: " + paar1);

        GPaar<String> paar2 = new GPaar<>("links", "rechts");
        System.out.println("Paar 2: " + paar2);

        GPaar<Integer> paar3 = new GPaar<>();
        System.out.println("Paar 3: " + paar3);
        Integer i3 = paar3.getL();
        System.out.println("links: " + i3);

        GPaar<String> paar4 = new GPaar<>();
        System.out.println("Paar 4: " + paar4);
        String s4 = paar4.getL();
        System.out.println("links: " + s4);

        int i = new GPaar<>(42, 1111).getL();
        System.out.println("links: " + i);

        String s = new GPaar<>("ich", "du").getL();
        System.out.println("links: " + s);
    }
}
