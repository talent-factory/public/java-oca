package book.dietmar_ratz.ch10.s2.p5;

import book.dietmar_ratz.ch10.s2.p3.Hose;

public class Jeans extends Hose {
    public String toString() {
        return "Jeans";
    }
}
