package book.dietmar_ratz.ch10.s2.p5;

import book.dietmar_ratz.ch10.s2.p2.GenPaar;
import book.dietmar_ratz.ch10.s2.p3.Hose;
import book.dietmar_ratz.ch10.s2.p3.Kleidung;

public class GenPaarTestWild2 {

    public static void genPaarAusgeben2(GenPaar<? super Hose> gp) {
        System.out.println(gp);
    }

    public static void main(String[] args) {
        Kleidung k1 = new Kleidung();
        Kleidung k2 = new Kleidung();
        GenPaar<Kleidung> p1 = new GenPaar<>(k1, k2);
        genPaarAusgeben2(p1);
    }
}
