package book.dietmar_ratz.ch10.s2.p5;

import book.dietmar_ratz.ch10.s2.p2.GenPaar;
import book.dietmar_ratz.ch10.s2.p3.Hose;

public class GenPaarTestWild3 {

    public static void genPaarAusgeben3(GenPaar<? super Hose> gp) {
        System.out.println(gp);
    }

    public static void main(String[] args) {
        Jeans j1 = new Jeans();
        Jeans j2 = new Jeans();
        GenPaar<Jeans> p1 = new GenPaar<>(j1, j2);
//        genPaarAusgeben3(p1);
    }
}
