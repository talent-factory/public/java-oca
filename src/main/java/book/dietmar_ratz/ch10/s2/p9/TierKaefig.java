package book.dietmar_ratz.ch10.s2.p9;

class TierKaefig<E> {
    private E insasse;
    public void setInsasse(E x) {
        insasse = x;
    }
    public E getInsasse() {
        return insasse;
    }
}
