package book.dietmar_ratz.ch10.s2.p9;

public class RateMal {

    public static void ausgabe(Object... eingabe) {
        System.out.print("Ausgabe: ");
        for (Object o : eingabe)
            System.out.print(o + " ");
        System.out.println();
    }

    public static <T extends Comparable<T>> T[] tueWas(T... eingabe) {
        eingabe = eingabe.clone();
        for (int i = eingabe.length - 1; i > 0; i--)
            for (int j = 0; j < i; j++)
                if (eingabe[j].compareTo(eingabe[j + 1]) > 0) {
                    T tmp = eingabe[j];
                    eingabe[j] = eingabe[j + 1];
                    eingabe[j + 1] = tmp;
                }
        return eingabe;
    }

    public static void main(String[] args) {
        ausgabe((Object[]) tueWas(Boolean.TRUE, Boolean.FALSE));
        ausgabe((Object[]) tueWas("welt", "schoene", "du", "hallo"));
    }
}
