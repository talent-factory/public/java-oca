package book.dietmar_ratz.ch19.s2.p1;

import java.util.stream.IntStream;

public class PipelineOperationen2 {

    public static void main(String[] args) {
        // IntStream mit Zahlen von 1 bis 99
        IntStream is = IntStream.range(1, 100)         // anlegen
                .map(x -> x * x)        // quadrieren
                .filter(x -> x % 2 == 0); // filtern

        // Summe der Streaminhalte ausgeben
        System.out.println("Summe aller geraden Quadratzahlen " +
                "zwischen 1 und 9801: " + is.sum());
    }
} 
