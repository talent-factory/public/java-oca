package book.dietmar_ratz.ch19.s2.p1;

import java.util.Arrays;
import java.util.List;

public class PipelineOperationen1 {

    public static void main(String[] args) {
        // Liste mit Namen anlegen
        List<String> namen = Arrays.asList("Dietmar", "Ratz",
                "Dennis", "Schulmeister",
                "Detlef", "Seese",
                "Jan", "Wiesenberger");
        namen
                .stream()
                .map(String::toUpperCase)
                .filter(s -> !s.contains("E"))
                .sorted()
                .forEach(n -> System.out.print(n + "  "));
        System.out.println();

        namen
                .stream()
                .map(String::toLowerCase)
                .filter(s -> s.contains("e"))
                .sorted((s, t) -> t.length() - s.length())
                .forEach(n -> System.out.print(n + "  "));
        System.out.println();
    }
} 
