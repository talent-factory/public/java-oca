package book.dietmar_ratz.ch19.s2.p5;

import java.util.Arrays;
import java.util.List;

public class Pipeline1 {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("bb", "ZZ", "aa", "PP", "zz");
        strings
                .stream()
                .sorted()
                .map(String::toUpperCase)
                .forEach(n -> System.out.print(n + " "));
    }
}
