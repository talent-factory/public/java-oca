package book.dietmar_ratz.ch19.s2.p5;

import java.util.Arrays;
import java.util.List;

public class Pipeline2 {

    public static void main(String[] args) {
        List<String> strings = Arrays.asList("bbb", "ZZZ", "aaa", "PPP",
                "zzz", "fff", "vvv");
        boolean i =
                strings.stream().distinct()
                        .sorted(String::compareToIgnoreCase)
                        .limit(4)
                        .map((a) -> a.substring(0, 2))
                        .noneMatch((a) -> a.equals("aa"));
        System.out.println(i);
    }
}
