package book.dietmar_ratz.ch19.s2.p4;

import stdlib.IOTools;

import java.util.stream.LongStream;

public class PipelineFakultaet {
    public static void main(String[] args) {
        // n einlesen
        int n = IOTools.readInt("n = ");

        // IntStream mit Zahlen von 1 bis n
        long f = LongStream.rangeClosed(1, n)         // anlegen und
                .reduce(1, (x, y) -> x * y); // auf Produkt
        // reduzieren

        // Fakultaet ausgeben
        System.out.println(n + "! = " + f);
    }
}
