package book.dietmar_ratz.ch19.s2.p4;

import stdlib.IOTools;

import java.math.BigInteger;
import java.util.stream.Stream;

public class PipelineFakultaetBigInt {

    public static void main(String[] args) {
        // n einlesen
        int n = IOTools.readInt("n = ");

        // Unendlichen Stream erzeugen
        Stream<BigInteger> s =
                Stream.iterate(BigInteger.ONE, x -> x.add(BigInteger.ONE))
                        .limit(n);

        // Auf Produkt aller Werte reduzieren (Berechnung der Fakultaet)
        BigInteger f =
                s.reduce(BigInteger.ONE, BigInteger::multiply);

        // Wert der Fakultaet ausgeben
        System.out.println(n + "! = " + f);
    }
}
