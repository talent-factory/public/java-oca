package book.dietmar_ratz.ch19.s1.p5;

public class BuchstabenDrucker {
    public void drucke() {
        for (char c = 'a'; c <= 'j'; c++) {
            System.out.print(c + " ");
        }
    }
} 
