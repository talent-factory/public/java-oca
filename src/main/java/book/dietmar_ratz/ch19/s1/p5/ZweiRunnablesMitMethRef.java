package book.dietmar_ratz.ch19.s1.p5;

public class ZweiRunnablesMitMethRef {

    public static void main(String[] args) {
        // Runnable mit statischer Methodenreferenz erzeugen
        Runnable r1 = BuchstabenDruck::drucke;

        // Runnable mit nicht-statischer Methodenreferenz erzeugen
        BuchstabenDrucker bd = new BuchstabenDrucker();
        Runnable r2 = bd::drucke;

        // Beide als Thread starten
        new Thread(r1).start();
        new Thread(r2).start();
    }
} 
