package book.dietmar_ratz.ch19.s1.p5;

import java.util.Arrays;
import java.util.List;

public class LambdaListOpsMitMethRef {

    public static void main(String[] args) {
        // Liste mit Namen anlegen
        List<String> namen = Arrays.asList("Dietmar", "Ratz",
                "Dennis", "Schulmeister",
                "Detlef", "Seese",
                "Jan", "Wiesenberger");
        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();

        // Alle Namen der Liste in Grossbuchstaben umwandeln
        namen.replaceAll(String::toUpperCase);

        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();

        // Liste alphabetisch aufsteigend sortieren
        namen.sort(String::compareTo);

        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();
    }
} 
