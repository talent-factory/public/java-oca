package book.dietmar_ratz.ch19.s1.p3;

public class WerteTabellen {

    public static void tabelliere(String titel, Funktion1p f) {
        System.out.println("Wertetabelle der " + titel + "-Funktion");
        System.out.println("     x           f(x)");
        for (double x = 1.0; x <= 5.0; x++) {
            System.out.printf("% 10.5e  % 10.5e\n", x, f.rechne(x));
        }
    }

    public static void main(String[] args) {
        tabelliere("Quadrat", x -> x * x);
        tabelliere("Sinus", Math::sin);
        tabelliere("Tangens", Math::tan);
    }
} 
