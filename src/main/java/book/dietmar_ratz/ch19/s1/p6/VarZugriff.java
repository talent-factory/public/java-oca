package book.dietmar_ratz.ch19.s1.p6;

public class VarZugriff {

    public String s = "s in VarZugriff";

    public void losGehts() {
        int x = 111;

        // Runnable-Objekt mit anonymer Klasse erzeugen
        Runnable r1 =
                new Runnable() {
                    public String s = "s in anonymer Klasse";

                    public void run() {
                        String s = "s in run";
                        System.out.println("r1: " + s);
                        System.out.println("r1: " + this.s);
                        System.out.println("r1: x=" + x);
                    }
                };

        // Runnable ausfuehren
        r1.run();

        // Runnable-Objekt mit Lambda-Ausdruck erzeugen
        Runnable r2 =
                () -> {
                    String s = "s in Lambda-Anweisungen";
                    System.out.println("r2: " + s);
                    System.out.println("r2: " + this.s);
                    System.out.println("r2: x=" + x);
                };

        // Runnable ausfuehren
        r2.run();
    }

    public static void main(String[] args) {
        VarZugriff vz = new VarZugriff();
        vz.losGehts();
    }
} 
