package book.dietmar_ratz.ch19.s1.p1;

public class BuchstabenDruck {

    public static void drucke() {
        for (char c = 'a'; c <= 'j'; c++) {
            System.out.print(c + " ");
        }
    }
} 
