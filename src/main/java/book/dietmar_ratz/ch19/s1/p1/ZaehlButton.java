package book.dietmar_ratz.ch19.s1.p1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("Convert2Lambda")
public class ZaehlButton extends JFrame {

    Container c;             // Container dieses Frames
    JButton b;               // Knopf
    int z;                   // Zaehler

    public ZaehlButton() { // Konstruktor
        c = getContentPane();  // Container bestimmen
        z = 0;                 // Zaehler auf 0 setzen
        b = new JButton("0");  // Button erzeugen und
        c.add(b);              // dem Container hinzufuegen

        // Listener-Objekt mit anonymer Klasse erzeugen
        ActionListener bl =
                new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        b.setText(++z + "");
                    }
                }; // Ende der anonymen Klassendefinition

        // Listener-Objekt beim Button registrieren
        b.addActionListener(bl);
    }

    public static void main(String[] args) {
        ZaehlButton fenster = new ZaehlButton();
        fenster.setTitle("ZaehlButton");
        fenster.setSize(220, 100);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
