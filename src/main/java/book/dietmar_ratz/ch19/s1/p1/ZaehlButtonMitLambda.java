package book.dietmar_ratz.ch19.s1.p1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class ZaehlButtonMitLambda extends JFrame {

    Container c;             // Container dieses Frames
    JButton b;               // Knopf
    int z;                   // Zaehler

    public ZaehlButtonMitLambda() { // Konstruktor
        c = getContentPane();  // Container bestimmen
        z = 0;                 // Zaehler auf 0 setzen
        b = new JButton("0");  // Button erzeugen und
        c.add(b);              // dem Container hinzufuegen

        // Listener-Objekt mit Lambda-Ausdruck erzeugen
        ActionListener bl =
                e -> b.setText(++z + "");

        // Listener-Objekt beim Button registrieren
        b.addActionListener(bl);
    }

    public static void main(String[] args) {
        ZaehlButtonMitLambda fenster = new ZaehlButtonMitLambda();
        fenster.setTitle("ZaehlButtonMitLambda");
        fenster.setSize(220, 100);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
