package book.dietmar_ratz.ch19.s1.p1;

public class ZweiRunnables {

    public static void main(String[] args) {
        // Runnable-Objekt mit anonymer Klasse erzeugen
        Runnable r1 = BuchstabenDruck::drucke;

        // Runnable-Objekt mit Lambda-Ausdruck erzeugen
        Runnable r2 = BuchstabenDruck::drucke;

        // Beide als Thread starten
        new Thread(r1).start();
        new Thread(r2).start();
    }
} 
