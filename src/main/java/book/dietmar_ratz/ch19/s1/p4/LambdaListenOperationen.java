package book.dietmar_ratz.ch19.s1.p4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class LambdaListenOperationen {

    public static void main(String[] args) {
        // Liste mit Namen anlegen
        List<String> namen = Arrays.asList("Dietmar", "Ratz",
                "Dennis", "Schulmeister",
                "Detlef", "Seese",
                "Jan", "Wiesenberger");
        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();

        // Alle Namen der Liste in Grossbuchstaben umwandeln
        namen.replaceAll(String::toUpperCase);

        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();

        // Liste alphabetisch aufsteigend sortieren
        namen.sort(String::compareTo);

        // Alle Namen der Liste ausgeben
        namen.forEach(n -> System.out.print(n + " "));
        System.out.println();

        // Liste mit ganzen Zahlen anlegen
        List<Integer> zahlen = Arrays.asList(1, 3, 5, 7, 8, 6, 4, 2);

        // Alle Zahlen der Liste ausgeben
        zahlen.forEach(k -> System.out.print(k + "  "));
        System.out.println();

        // Alle Zahlen quadrieren
        zahlen.replaceAll(k -> k * k);

        // Alle Zahlen der Liste ausgeben
        zahlen.forEach(k -> System.out.print(k + "  "));
        System.out.println();

        // Liste aufsteigend sortieren
        zahlen.sort(Comparator.comparingInt(a -> a));

        // Alle Zahlen der Liste ausgeben
        zahlen.forEach(k -> System.out.print(k + "  "));
        System.out.println();
    }
} 
