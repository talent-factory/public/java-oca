package book.dietmar_ratz.ch03;

import java.util.Scanner;

/**
 * Einlesen von der Konsole
 */
public class ConsolenEingabe {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Geben sie eine Zahl ein: ");
        int eingabe = scanner.nextInt();

        System.out.println();
        System.out.println("Ihre Eingabe war: " + eingabe);
    }
}
