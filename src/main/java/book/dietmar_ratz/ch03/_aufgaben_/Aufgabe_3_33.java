package book.dietmar_ratz.ch03._aufgaben_;

import stdlib.IOTools;

/**
 * <b>Aufgabe 3.33</b>
 *
 * <p>Zwei verschiedene natürliche Zahlen a und b heißen befreundet, wenn die
 * Summe der (von a verschiedenen) Teiler von a gleich b ist und die Summe der
 * (von b verschiedenen) Teiler von b gleich a ist.</p>
 *
 * Hier einige Paare befreundeter Zahlen zur Ueberpruefung von
 * abgegebenen Programmen:
 *
 *   220      284
 *  1184     1210
 *  2620     2924
 *  5020     5564
 *  6232     6368
 * 10744    10856
 * 12285    14595
 * 17296    18416
 */
public class Aufgabe_3_33 {

    public static void main(String[] args) {

        char antwort;
        int a, b;
        int teilersummeA, teilersummeB;

        do {  // Hauptschleife

            a = IOTools.readInteger("Erste  Zahl > ");
            b = IOTools.readInteger("Zweite Zahl > ");

            // Teilersumme von a berechnen
            teilersummeA = 0;
            for (int i = 1; i < a; i++)
                if (a % i == 0)
                    teilersummeA += i;

            //Teilersumme von b berechnen
            teilersummeB = 0;
            for (int i = 1; i < b; i++)
                if (b % i == 0)
                    teilersummeB += i;


            if (teilersummeA == b && teilersummeB == a)
                System.out.println("Die beiden Zahlen sind miteinander befreundet!");
            else
                System.out.println("Die beiden Zahlen sind NICHT miteinander befreundet!");

            antwort = IOTools.readChar("Noch eine Berechnung? (J/N) ");
        } while ((antwort == 'j') || (antwort == 'J'));
    }
}
