package book.dietmar_ratz.ch03._aufgaben_;

import stdlib.IOTools;

/**
 * <b>Aufgabe 3.31</b>
 *
 * <p>Schreiben Sie ein Java-Programm, das eine einzulesende ganze Dezimalzahl
 * <i>d</i> in eine Binärzahl <i>b</i>> umrechnet und ausgibt. Dabei soll <i>d</i>
 * mit Hilfe des Datentyps short und <i>b</i> mit Hilfe des Datentyps long
 * dargestellt werden, wobei b nur die Ziffern 0 und 1 enthalten darf. Die
 * long-Zahl 10101 soll also z. B. der Binärzahl
 *
 * 10101 2 = 1 · 2<sup>4</sup> + 0 · 2<sup>3</sup> + 1 · 2<sup>2</sup> + 0 · 2<sup>1</sup> + 1 · 2<sup>0</sup> =
 * 21<sub>10</sub> entsprechen.
 *
 * Verwenden Sie (bei geeigneter Behandlung des Falles d < 0) den Algorithmus:
 */
public class Aufgabe_3_31 {

    public static void main(String[] args) {

        short decimalValue = IOTools.readShort("  Dezimalzahl: ");

        long binaryValue = 0;
        int multiplier = 1;

        while (decimalValue > 0) {
            binaryValue += ((decimalValue % 2) * multiplier);
            decimalValue /= 2;
            multiplier *= 10;
        }

        System.out.printf("als Binärzahl: %d\n", binaryValue);
    }
}
