package book.dietmar_ratz.ch03._aufgaben_;

import stdlib.IOTools;

/**
 * <b>Aufgabe 3.34</b>
 *
 * <p>Schreiben Sie ein Java-Programm, das zu einem beliebigen Datum den
 * zugehörigen Wochentag ausgibt. Ein Datum soll jeweils durch drei ganzzahlige
 * Werte t (Tag), m (Monat) und j (Jahr) vorgegeben sein.</p>
 */
public class Aufgabe_3_34 {

    public static void main(String[] args) {

        int day, month, year, c, y, h;

        day = IOTools.readInteger("Tag = ");
        month = IOTools.readInteger("Monat = ");
        year = IOTools.readInteger("Jahr = ");

        System.out.print("Der " + day + "." + month + "." + year + " ist ein ");

        if (month <= 2) {
            month += 10;
            year--;
        } else
            month -= 2;

        c = year / 100;
        y = year % 100;

        h = (((26 * month - 2) / 10) + day + y + y / 4 + c / 4 - 2 * c) % 7;

        if (h < 0)
            h += 7;

        switch (h) {
            case 0 -> System.out.println("Sonntag.");
            case 1 -> System.out.println("Monatg.");
            case 2 -> System.out.println("Dienstag.");
            case 3 -> System.out.println("Mittwoch.");
            case 4 -> System.out.println("Donnerstag.");
            case 5 -> System.out.println("Freitag.");
            case 6 -> System.out.println("Samstag.");
        }
    }
}
