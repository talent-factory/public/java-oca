package book.dietmar_ratz.ch03.s4.p1;

public class StundenRechner2 {

    public static void main(String[] args) {

        int anzahlStunden = 12;
        int stundenLohn = 15;

        System.out.print("Arbeitsstunden: ");
        System.out.println(anzahlStunden);
        System.out.print("Stundenlohn in EUR: ");
        System.out.println(stundenLohn);
        System.out.print("Damit habe ich letzte Woche ");
        System.out.print(anzahlStunden * stundenLohn);
        System.out.println(" EUR verdient.");

    }
}
