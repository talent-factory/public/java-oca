package book.dietmar_ratz.ch03.s4.p1;

public class StundenRechner1 {

    public static void main(String[] args) {
        System.out.print("Arbeitsstunden: ");
        System.out.println(18);
        System.out.print("Stundenlohn in EUR: ");
        System.out.println(15);
        System.out.print("Damit habe ich letzte Woche ");
        System.out.print(18 * 15);
        System.out.println(" EUR verdient.");
    }
}
