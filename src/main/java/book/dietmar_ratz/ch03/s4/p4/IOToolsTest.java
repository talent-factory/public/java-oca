package book.dietmar_ratz.ch03.s4.p4;

import stdlib.IOTools;

@SuppressWarnings("DuplicatedCode")
public class IOToolsTest {

    public static void main(String[] args) {

        int i, j, k;
        double d;
        char c;
        boolean b;

        // int-Eingabe ohne Prompt
        i = IOTools.readInteger();

        // int-Eingabe mit Prompt
        System.out.print("j = ");
        j = IOTools.readInteger();

        // Vereinfachte int-Eingabe mit Prompt
        k = IOTools.readInteger("k = ");

        // double-Eingabe mit Prompt
        d = IOTools.readDouble("d = ");

        // char-Eingabe mit Prompt
        c = IOTools.readChar("c = ");

        // boolean-Eingabe mit Prompt
        b = IOTools.readBoolean("b = ");

        // Testausgaben
        System.out.println("i = " + i);
        System.out.println("j = " + j);
        System.out.println("k = " + k);
        System.out.println("d = " + d);
        System.out.println("c = " + c);
        System.out.println("b = " + b);
    }
}
