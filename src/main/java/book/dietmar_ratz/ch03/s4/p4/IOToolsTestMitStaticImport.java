package book.dietmar_ratz.ch03.s4.p4;

import static stdlib.IOTools.*;

@SuppressWarnings("DuplicatedCode")
public class IOToolsTestMitStaticImport {

    public static void main(String[] args) {
        int i, j, k;
        double d;
        char c;
        boolean b;

        // int-Eingabe ohne Prompt (ohne vorherige Ausgabe)
        i = readInteger();

        // int-Eingabe mit Prompt
        System.out.print("j = ");
        j = readInteger();

        // Vereinfachte int-Eingabe mit Prompt
        k = readInteger("k = ");

        // double-Eingabe mit Prompt
        d = readDouble("d = ");

        // char-Eingabe mit Prompt
        c = readChar("c = ");

        // boolean-Eingabe mit Prompt
        b = readBoolean("b = ");

        // Testausgaben
        System.out.println("i = " + i);
        System.out.println("j = " + j);
        System.out.println("k = " + k);
        System.out.println("d = " + d);
        System.out.println("c = " + c);
        System.out.println("b = " + b);
    }
}
