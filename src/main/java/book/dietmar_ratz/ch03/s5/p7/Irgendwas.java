package book.dietmar_ratz.ch03.s5.p7;

import stdlib.IOTools;

@SuppressWarnings("ManualMinMaxCalculation")
public class Irgendwas {

    public static void main(String[] args) {
        double a, b, c, d, e;
        a = IOTools.readDouble("a = ");
        b = IOTools.readDouble("b = ");
        c = IOTools.readDouble("c = ");
        d = IOTools.readDouble("d = ");

        if (b > a)
            if (c > b)
                if (d > c)
                    e = d;
                else
                    e = c;
            else if (d > b)
                e = d;
            else
                e = b;
        else if (c > a)
            if (d > c)
                e = d;
            else
                e = c;
        else if (d > a)
            e = d;
        else
            e = a;
        System.out.println("e = " + e);
    }
}
