package book.dietmar_ratz.ch03.s5.p3;

import stdlib.IOTools;

public class StringSwitchDemo {

    public static int monatsZahl(String monat) {
        int zahl = 0;
        if (monat == null) {
            return zahl;
        }
        switch (monat.toLowerCase()) {
            case "januar" -> zahl = 1;
            case "februar" -> zahl = 2;
            case "maerz" -> zahl = 3;
            case "april" -> zahl = 4;
            case "mai" -> zahl = 5;
            case "juni" -> zahl = 6;
            case "juli" -> zahl = 7;
            case "august" -> zahl = 8;
            case "september" -> zahl = 9;
            case "oktober" -> zahl = 10;
            case "november" -> zahl = 11;
            case "dezember" -> zahl = 12;
        }
        return zahl;
    }

    public static int tageImMonat(String monat, int jahr) {
        int tage = 0;
        if (monat == null) {
            return tage;
        }
        switch (monat.toLowerCase()) {
            case "februar":
                if ((jahr % 4 != 0) || ((jahr % 100 == 0) && (jahr % 400 != 0))) {
                    tage = 28;
                } else {
                    tage = 29;
                }
                break;
            case "april":
            case "juni":
            case "september":
            case "november":
                tage = 30;
                break;
            case "januar":
            case "maerz":
            case "mai":
            case "juli":
            case "august":
            case "oktober":
            case "dezember":
                tage = 31;
                break;
        }
        return tage;
    }

    public static void main(String[] args) {
        String m = IOTools.readString("Monat (als Zeichenkette): ");
        int j = IOTools.readInt("Jahr (positive ganze Zahl): ");

        int t = tageImMonat(m, j);
        if (t == 0) {
            System.out.println("Unzulaessiger Monat");
        } else if (j <= 0) {
            System.out.println("Unzulaessiges Jahr");
        } else {
            System.out.println("Der Monat " + m + " des Jahres " + j +
                    " hat " + t + " Tage.");
        }

        int z = monatsZahl(m);
        if (z == 0) {
            System.out.println("Unzulaessiger Monat");
        } else {
            System.out.println("Der Monat " + m + " ist der " + z +
                    ". Monat des Jahres.");
        }
    }
}
