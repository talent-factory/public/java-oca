package book.dietmar_ratz.ch03.s2.p2;

/**
 * Ausgaben auf der Konsole.
 * <p>
 * Beispiel s.55
 */
public class HalloWelt {

    public static void main(String[] args) {
        System.out.print("Hallo Welt - ");
        System.out.println("Mein erstes Programm.");
    }
}
