package book.dietmar_ratz.ch03.s1.p10;/* Beispiel: berechnet 7 + 11 */

/**
 * Aufgabe: 3.3, s.51
 */
public class Berechnung {

    public static void main(String[] args) {
        int summe;
        summe = 7 + 13;
        System.out.print("7 + 11 ergibt");
        System.out.println(summe);
    }
}
