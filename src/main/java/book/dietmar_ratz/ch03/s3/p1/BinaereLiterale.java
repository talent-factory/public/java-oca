package book.dietmar_ratz.ch03.s3.p1;

@SuppressWarnings("OctalInteger")
public class BinaereLiterale {

    public static void main(String[] args) {
        // Binaere Literale
        int dezWert = 42;       // Die Zahl 42, dezimal
        int oktWert = 052;      // Die Zahl 42, oktal
        int hexWert = 0x2A;     // Die Zahl 42, hexadezimal
        int binWert = 0b101010; // Die Zahl 42, binaer

        System.out.println(dezWert + ", " + oktWert + ", " +
                hexWert + " und nochmal " + binWert);
    }
}
