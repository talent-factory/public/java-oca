package book.dietmar_ratz.ch03.s3.p2;

@SuppressWarnings({"unused", "OctalInteger"})
public class Unterstrich {

    public static void main(String[] args) {

        //  Unterstrich in numerischen Literalen
        long creditKartenNummer = 1234_5678_9012_3456L;
        long versicherungsNummer = 999_99_9999L;
        float pi = 3.14_15F;
        double e = 2.71_82_81_82_84_59;
        long hexBytes = 0xFF_EC_DE_5E;
        long hexText = 0xCAFE_BABE;
        long maxLong = 0x7fff_ffff_ffff_ffffL;
        long binBytes = 0b11010010_01101001_10010100_10010010;
        int ok1 = 4_2;
        int ok2 = 4_______2;
        int ok3 = 0x2__a;
        int ok4 = 0_42;
        int ok5 = 04_2;

        System.out.println(binBytes);
    }
}
