package book.dietmar_ratz.ch03.s3.p2;

public class UnterstrichFalsch {
    public static void main(String[] args) {

//        UNZULAESSIGE Verwendung des Unterstriches

//        float badPi1 = 3_.1415F;  // _ direkt vor dem Dezimalpunkt
//        float badPi2 = 3._1415F;  // _ direkt hinter dem Dezimalpunkt
//        long badNr = 99_99_99_L;  // _ direkt vor dem L-Anhang
//        int badNr1 = _42;         // _ am Anfang (waere ein Bezeichner)
//        int badNr2 = 42_;         // _ am Ende der Ziffernfolge
//        int badNr3 = 0_ x42;      // _ innerhalb des 0x-Praefix
//        int badNr4 = 0x_42;       // _ am Anfang der Ziffernfolge
//        int badNr5 = 0x42_;       // _ am Ende
//        int badNr6 = 042_;        // _ am Ende
    }
}
