package book.dietmar_ratz.ch03.s3.ch02;

public class Modulo {

    public static void main(String[] args) {

        int max = 6;
        int job = 0;

        for (int i = 0; i < 15; i++) {

            int pos = (job++ % max);
            System.out.printf("Job #%2d @ %d\n", job, pos);

        }
    }
}
