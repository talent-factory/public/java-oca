package book.dietmar_ratz.ch03.s3.ch02;

public class IntDiv {

    public static void main(String[] args) {

        System.out.print("1/10 beträgt ");
        System.out.print(1 / 10);       // Ganzzahlige Division
        System.out.print(" mit Rest ");
        System.out.print(1 % 10);       // Rest
    }
}
