package book.dietmar_ratz.ch16.s2.p2;

class TVProgRunnable implements Runnable {

    // Instanzvariable als Referenz auf den eigentlichen Thread
    Thread t;

    // Konstruktor
    public TVProgRunnable(String name) {
        // Erzeuge einen Thread, der mit dem eigenen Objekt verbunden ist
        t = new Thread(this, name);
    }

    // start-Methode des Runnable-Objekts startet den eigentlichen Thread
    public void start() {
        t.start();
    }

    // run-Methode (Schleife mit Zufalls-Wartezeiten)
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(Thread.currentThread().getName()
                    + " zum " + i + ". Mal");
            try {
                Thread.sleep((int) (Math.random() * 1000));
            } catch (InterruptedException ignored) {
            }
        }
        System.out.println(Thread.currentThread().getName() + " FERTIG!");
    }
}
