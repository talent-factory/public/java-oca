package book.dietmar_ratz.ch16.s2.p2;

public class TVProgAuslosungMitRunnable {

    public static void main(String[] args) {
        TVProgRunnable t1 = new TVProgRunnable("Wer wird Millionaer?");
        TVProgRunnable t2 = new TVProgRunnable("Enterprise");
        TVProgRunnable t3 = new TVProgRunnable("Nils Holgersson");
        t1.start();
        t2.start();
        t3.start();
    }
}
