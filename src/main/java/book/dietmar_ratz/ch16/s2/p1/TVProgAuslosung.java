package book.dietmar_ratz.ch16.s2.p1;

public class TVProgAuslosung {

    public static void main(String[] args) {
        TVProgThread t1 = new TVProgThread("Wer wird Millionaer?");
        TVProgThread t2 = new TVProgThread("Enterprise");
        TVProgThread t3 = new TVProgThread("Nils Holgersson");
        t1.start();
        t2.start();
        t3.start();
    }
}
