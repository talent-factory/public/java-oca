package book.dietmar_ratz.ch16.s2.p1;

class TVProgThread extends Thread {

    // Konstruktor
    public TVProgThread(String name) {
        super(name);
    }

    // run-Methode (Schleife mit Zufalls-Wartezeiten)
    public void run() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(getName() + " zum " + i + ". Mal");
            try {
                sleep((int) (Math.random() * 1000));
            } catch (InterruptedException ignored) {
            }
        }
        System.out.println(getName() + " FERTIG!");
    }
}
