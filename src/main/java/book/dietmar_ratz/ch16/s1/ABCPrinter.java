package book.dietmar_ratz.ch16.s1;

import book.dietmar_ratz.ch16.s2.p1.MachMal;

public class ABCPrinter {

    public void run() {
        for (char b = 'A'; b <= 'Z'; b++) {
            // Gib den Buchstaben aus
            System.out.print(b);
            // Verbringe eine Sekunde mit "Nichtstun"
            MachMal.eineSekundeLangGarNichts();
        }
    }

    public void start() {
        run();
    }
}
