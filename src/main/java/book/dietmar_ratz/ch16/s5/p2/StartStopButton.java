package book.dietmar_ratz.ch16.s5.p2;

import javax.swing.*;
import java.awt.*;

public class StartStopButton extends JButton {

    public StartStopButton(Color c) {
        setBackground(c);
        setFont(new Font("Arial", Font.PLAIN, 25));
        setText("START");
    }

    public boolean isStart() {
        return getText().equals("START");
    }

    public void switchText() {
        if (isStart())
            setText("STOP");
        else
            setText("START");
    }
}

