package book.dietmar_ratz.ch16.s5.p2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AutomatFrame extends JFrame {

    Container c;
    ColorRunLabel rotAnzeige, gelbAnzeige, gruenAnzeige;
    StartStopButton rotKnopf, gelbKnopf, gruenKnopf;

    public AutomatFrame() {
        c = getContentPane();
        rotAnzeige = new ColorRunLabel(Color.RED);
        gelbAnzeige = new ColorRunLabel(Color.YELLOW);
        gruenAnzeige = new ColorRunLabel(Color.GREEN);
        rotKnopf = new StartStopButton(Color.RED);
        gelbKnopf = new StartStopButton(Color.YELLOW);
        gruenKnopf = new StartStopButton(Color.GREEN);

        c.setLayout(new GridLayout(2, 3, 5, 5));
        c.add(rotAnzeige);
        c.add(gelbAnzeige);
        c.add(gruenAnzeige);
        c.add(rotKnopf);
        c.add(gelbKnopf);
        c.add(gruenKnopf);

        rotKnopf.addActionListener(new KnopfListener(rotAnzeige, rotKnopf));
        gruenKnopf.addActionListener(new KnopfListener(gruenAnzeige, gruenKnopf));
        gelbKnopf.addActionListener(new KnopfListener(gelbAnzeige, gelbKnopf));
    }

    class KnopfListener implements ActionListener {
        ColorRunLabel crl;
        StartStopButton ssb;

        KnopfListener(ColorRunLabel crl, StartStopButton ssb) {
            this.crl = crl;
            this.ssb = ssb;
        }

        public void actionPerformed(ActionEvent e) {
            if (ssb.isStart())
                crl.start();
            else
                crl.stop();
            ssb.switchText();
        }
    }

    // main-Methode
    public static void main(String[] args) {
        AutomatFrame fenster = new AutomatFrame();
        fenster.setTitle("AutomatFrame");
        fenster.setSize(400, 200);
        fenster.setVisible(true);
        fenster.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
