package book.dietmar_ratz.ch16.s5.p2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Listener, der beim Druck auf einen Button eine
 * veraenderliche Label-Anzeige per Thread startet
 */
class KnopfListener implements ActionListener {

    ColorRunLabel crl;
    StartStopButton ssb;

    KnopfListener(ColorRunLabel crl, StartStopButton ssb) {
        this.crl = crl;
        this.ssb = ssb;
    }

    public void actionPerformed(ActionEvent e) {
        if (ssb.isStart()) // falls Start-Knopf
            crl.start();         // Thread des Labels starten
        else               // andernfalls
            crl.stop();          // Thread des Labels abbrechen
        ssb.switchText();  // Beschriftung des Buttons wechseln
    }
}
