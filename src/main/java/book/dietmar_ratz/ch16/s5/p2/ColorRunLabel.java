package book.dietmar_ratz.ch16.s5.p2;

import javax.swing.*;
import java.awt.*;

/**
 * Spezialisierte JLabel-Klasse, deren Objekte durch einen Thread
 * mit einer staendig wechselnden Zufallsanzeige versehen sind
 */
@SuppressWarnings("BusyWait")
public class ColorRunLabel extends JLabel implements Runnable {

    private boolean running = false;

    public ColorRunLabel(Color c) {
        setOpaque(true);
        setBackground(c);
        setFont(new Font("Arial", Font.BOLD, 50));
        setHorizontalAlignment(JLabel.CENTER);
    }

    public void start() {
        running = true;
        new Thread(this).start();
    }

    public void stop() {
        running = false;
    }

    public void run() {
        while (running) {
            setText("" + (int) (10 * Math.random()));
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                return;
            }
        }
    }
}
