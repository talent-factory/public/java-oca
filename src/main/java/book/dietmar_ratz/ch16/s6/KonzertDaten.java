package book.dietmar_ratz.ch16.s6;

class KonzertDaten {

    private int sitzPlatz = 0;

    int freierPlatz() {
        int n = sitzPlatz;
        // simuliere Datenbankabfragen
        try {
            Thread.sleep((int) (Math.random() * 100));
        } catch (InterruptedException ignored) {
        }
        return sitzPlatz = n + 1;
    }
}
