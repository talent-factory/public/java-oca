package book.dietmar_ratz.ch16.s4.p1;

public abstract class Figur {
    protected char x;
    protected int y;

    abstract public void setPosition(char x, int y);

    abstract public String getPosition();
}
