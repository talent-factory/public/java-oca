package book.dietmar_ratz.ch16.s4.p1;

public class FigurenThreads1 {

    public static void main(String[] args) {
        SchlechteFigur f = new SchlechteFigur();
        Schreiber s = new Schreiber(f);
        Leser l = new Leser(f);
        s.setDaemon(true);
        s.start();
        l.start();
    }
}
