package book.dietmar_ratz.ch16.s4.p1;

import book.dietmar_ratz.ch16.s2.p1.MachMal;

public class GuteFigur extends Figur {

    synchronized public void setPosition(char x, int y) {
        this.x = x;
        MachMal.eineSekundeLangGarNichts();
        this.y = y;
    }

    synchronized public String getPosition() {
        MachMal.eineSekundeLangGarNichts();
        return "(" + x + "," + y + ")";
    }
}
