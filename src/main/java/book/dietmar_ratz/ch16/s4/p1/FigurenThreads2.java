package book.dietmar_ratz.ch16.s4.p1;

public class FigurenThreads2 {

    public static void main(String[] args) {
        GuteFigur f = new GuteFigur();
        Schreiber s = new Schreiber(f);
        Leser l = new Leser(f);
        s.setDaemon(true);
        s.start();
        l.start();
    }
}
