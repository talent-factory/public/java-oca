package book.dietmar_ratz.ch16.s4.p2;

abstract class Wert {

    protected int wert;

    abstract public int get();

    abstract public void put(int w);
}

