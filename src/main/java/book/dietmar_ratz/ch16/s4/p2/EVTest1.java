package book.dietmar_ratz.ch16.s4.p2;

public class EVTest1 {

    public static void main(String[] args) {
        SchlechterWert w = new SchlechterWert();
        Erzeuger e = new Erzeuger(w);
        Verbraucher v = new Verbraucher(w);
        e.start();
        v.start();
    }
}
