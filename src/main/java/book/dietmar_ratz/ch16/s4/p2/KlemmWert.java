package book.dietmar_ratz.ch16.s4.p2;

class KlemmWert extends Wert {

    public synchronized int get() {
        try {
            wait();
        } catch (InterruptedException ignored) {
        }
        notify();
        System.out.println("Wert verbraucht!");
        return wert;
    }

    public synchronized void put(int w) {
        wert = w;
        System.out.println("Wert erzeugt!");
        notify();
        try {
            wait();
        } catch (InterruptedException ignored) {
        }
    }
}
