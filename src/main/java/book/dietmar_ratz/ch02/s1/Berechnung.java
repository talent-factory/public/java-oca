package book.dietmar_ratz.ch02.s1;

/**
 * Gibt die Summe zweier Zahlen auf dem Bildschirm aus.
 * <p>
 * Aufgabe 2.1, s.40
 */
public class Berechnung {

    public static void main(String[] args) {
        int i = 3 + 4;
        System.out.println(i);
    }

}
