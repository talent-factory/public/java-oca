package book.dietmar_ratz.ch02;

/**
 * Geben Sie das folgende Progreamm in Ihren Computer ein und bringen
 * Sie es zum Laufen.
 * <p>
 * Aufgabe 2.2, s.40
 */
public class Uebung {

    public static void main(String[] args) {
        System.out.println("Guten Tag!");
        System.out.println("Mein Name ist Puter, Komm-Puter.");
    }
}
