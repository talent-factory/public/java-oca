package _24optional.with;

import java.util.Optional;

public class Person {

    Optional<Car> car;

    public Optional<Car> getCar() {
        return car;
    }
}
