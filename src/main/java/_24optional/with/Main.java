package _24optional.with;

import java.util.Optional;

@SuppressWarnings("java:S106")
public class Main {

    public static void main(String[] args) {

        Optional<Person> person = Optional.empty();
        System.out.println(getCarInsuranceName(person));
    }
    private static String getCarInsuranceName(Optional<Person> person) {
         return person
                 .flatMap(Person::getCar)
                 .flatMap(Car::getInsurance)
                 .map(Insurance::getName)
                 .orElse("Not yet defined");

    }
}
