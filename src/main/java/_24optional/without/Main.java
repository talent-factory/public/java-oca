package _24optional.without;

@SuppressWarnings("java:S106")
public class Main {

    public static void main(String[] args) {

        Person person = new Person();
        System.out.println(getCarInsuranceName(person));
    }
    private static String getCarInsuranceName(Person person) {
         return person.getCar().getInsurance().getName();
    }
}
