package _22generics;

import lombok.Getter;

public class IntegerCache {

    @Getter
    private final Integer content;

    public IntegerCache(Integer content) {
        this.content = content;
    }
}
