package _22generics;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Comparator;

@Data
@AllArgsConstructor
public class Person implements Comparable<Person> {

    private String name;
    private String firstName;

    @Override
    public int compareTo(Person o) {

        if (!name.equals(o.name))
            return name.compareTo(o.name);

        return firstName.compareTo(o.firstName);
    }

    static class PersonSortByName implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.name.compareTo(o2.name);
        }
    }

    static class PersonSortByFirstName implements Comparator<Person> {

        @Override
        public int compare(Person o1, Person o2) {
            return o1.firstName.compareTo(o2.firstName);
        }
    }
}
