package _22generics;

import lombok.Getter;

/**
 * Generic cache storage.
 *
 * @param <T> type to be cached
 */
public record Cache<T>(@Getter T content) {

}
