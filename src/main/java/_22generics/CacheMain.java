package _22generics;

@SuppressWarnings("java:S106")
public class CacheMain {

    public static void main(String[] args) {
        Cache<Integer> genericInteger = new Cache<>(4711);
        System.out.println(genericInteger.content());

        Cache<String> genericString = new Cache<>("Senften");
        System.out.println(genericString.content());
    }
}
