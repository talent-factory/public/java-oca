package _22generics;

import lombok.Getter;

public record StringCache(@Getter String content) {

}
