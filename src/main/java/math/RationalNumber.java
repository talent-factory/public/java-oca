package math;

/**
 * Eine rationale Zahl ist eine reelle Zahl, die als Verhältnis zweier ganzer
 * Zahlen dargestellt werden kann. Um die Menge aller rationalen Zahlen zu
 * bezeichnen, wird das Formelzeichen Q (Unicode U+211A: ℚ) verwendet
 * (von „Quotient“, siehe Buchstabe mit Doppelstrich). Sie umfasst alle Zahlen,
 * die sich als Bruch darstellen lassen, der sowohl im Zähler als auch im Nenner
 * ganze Zahlen enthält. Die genaue mathematische Definition beruht auf
 * Äquivalenzklassen von Paaren ganzer Zahlen.
 *
 * @see <a href="https://de.wikipedia.org/wiki/Rationale_Zahl">Wikipedia</a>
 */
@SuppressWarnings("unused")
public class RationalNumber extends Number implements Comparable<RationalNumber> {

    /**
     * Zähler der rationalen Zahl
     */
    private long numerator;

    /**
     * Nenner der rationalen Zahl
     */
    private long denominator;

    /**
     * Erstellt eine rationale Zahl aus den beiden Werten {@code numerator} und
     * {@code denominator}.
     *
     * @param numerator   Zähler der rationalen Zahl
     * @param denominator Nenner der rationalen Zahl
     */
    public RationalNumber(long numerator, long denominator) {

        if (denominator == 0) {
            throw new ArithmeticException("Denominator must not be 0.");
        }

        if (denominator < 0) {
            numerator *= -1;
            denominator *= -1;
        }

        this.numerator = numerator;
        this.denominator = denominator;

        reduce();
    }

    /**
     * Erstellen einer rationalen Zahl, wobei der Nenner des Bruchs als {@code 1}
     * angenommen wird.
     *
     * @param value Zähler der rationalen Zahl
     */
    public RationalNumber(long value) {
        this(value, 1);
    }

    /**
     * Berechnet den grössten gemeinsamen Teiler der beiden Zahlen {@code m}
     * und {@code n}.
     *
     * @return grösster gemeinsamer Teiler der beiden Zahlen
     */
    private static long gcd(long m, long n) {
        m = Math.abs(m);
        n = Math.abs(n);
        return (n == 0) ? m : gcd(n, m % n);
    }

    /**
     * Berechnet das kleinste gemeinsame Vielfache. Das kleinste gemeinsame
     * Vielfache zweier ganzer Zahlen {@code m} und {@code n} ist die
     * kleinste positive natürliche Zahl, die sowohl Vielfaches von {@code m}
     * als auch Vielfaches von {@code n} ist.
     *
     * @return kleinstes gemeinsames Vielfaches der beiden Zahlen
     */
    private static long lcm(long m, long n) {
        m = Math.abs(m);
        n = Math.abs(n);
        return m * (n / gcd(m, n));
    }

    /**
     * Reduziert diese rationale Zahl, indem sowohl der Zähler als auch der
     * Nenner durch ihren größten gemeinsamen Teiler dividiert werden.
     */
    private void reduce() {
        if (numerator != 0) {
            long common = gcd(Math.abs(numerator), denominator);
            numerator = numerator / common;
            denominator = denominator / common;
        }
    }

    /**
     * Addition zweier rationaler Zahlen.
     *
     * @param other zu addierende, rationale Zahl
     * @return Resultat der Addition
     */
    public RationalNumber add(RationalNumber other) {
        // TODO Your code goes here
        return null;
    }

    /**
     * Berechnen der Differenz zweier rationaler Zahlen.
     *
     * @param other der abzuziehende Wert
     * @return Differenz der beiden relationalen Zahlen
     */
    public RationalNumber subtract(RationalNumber other) {
        // TODO Your code goes here
        return null;
    }

    /**
     * Multipliziert diese rationale Zahl mit der als Parameter übergebenen Zahl.
     *
     * @param other zu multiplizierende, rationale Zahl
     * @return Resultat der Multiplikation
     */
    public RationalNumber multiply(RationalNumber other) {
        // TODO Your code goes here
        return null;
    }

    /**
     * Potenzieren einer rationalen Zahl.
     *
     * @param exponent der rationalen Zahl
     * @return Resultat der Potenzrechnung
     */
    public RationalNumber pow(int exponent) {
        // TODO Your code goes here
        return null;
    }

    /**
     * Dividiert diese rationale Zahl durch die als Parameter übergebene Zahl
     * durch Multiplikation mit dem Kehrwert der zweiten rationalen Zahl.
     *
     * @param other zu dividierende, rationale Zahl
     * @return Resultat der Division
     */
    public RationalNumber divide(RationalNumber other) {
        return multiply(other.reciprocal());
    }


    /**
     * Berechnen des Absolutwertes {@code |x|}.
     *
     * @return Absoluter Wert der rationalen Zahl
     */
    public RationalNumber abs() {
        if (numerator >= 0) return this;
        else return negate();
    }

    /**
     * Berechnen der Negation {@code -x}.
     *
     * @return Wert für {@code -x}
     */
    public RationalNumber negate() {
        return new RationalNumber(-numerator, denominator);
    }

    /**
     * Gibt den Kehrwert dieser rationalen Zahl zurück.
     *
     * @return Kehrwert der rationalen Zahl
     */
    public RationalNumber reciprocal() {
        return new RationalNumber(denominator, numerator);
    }

    /**
     * Ausgabe der rationalen Zahl als Zeichenkette.
     *
     * @return Darstellung (als {@link String}) der rationalen Zahl
     */
    public String toString() {
        if (denominator == 1) return "" + numerator;
        else return numerator + "/" + denominator;
    }

    /**
     * Sind die beiden rationalen Zahlen identisch?
     *
     * @param other zu vergleichende rationale Zahl
     * @return liefert den Wert {@code true}, falls die beiden rationalen Zahlen
     * identisch sind
     */
    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other.getClass() != this.getClass()) return false;
        RationalNumber objToCompare = (RationalNumber) other;
        return compareTo(objToCompare) == 0;
    }

    /**
     * Vergleicht zwei rationale Zahlen.
     *
     * @param other zu vergleichende rationale Zahl
     * @return { -1, 0, +1 } if a &lt; b, a = b, or a &gt; b
     */
    @Override
    public int compareTo(RationalNumber other) {
        double left = this.numerator * other.denominator;
        double right = this.denominator * other.numerator;
        return Double.compare(left, right);
    }

    /**
     * Gibt den Wert der rationalen Zahl als {@code int} zurück.
     *
     * @return rationale Zahl als {@code int}
     */
    @Override
    public int intValue() {
        return (int) longValue();
    }

    /**
     * Gibt den Wert der rationalen Zahl als {@code long} zurück.
     *
     * @return rationale Zahl als {@code long}
     */
    @Override
    public long longValue() {
        return numerator / denominator;
    }

    /**
     * Gibt den Wert der rationalen Zahl als {@code float} zurück.
     *
     * @return rationale Zahl als {@code float}
     */
    @Override
    public float floatValue() {
        return (float) doubleValue();
    }

    /**
     * Gibt den Wert der rationalen Zahl als {@code double} zurück.
     *
     * @return rationale Zahl als {@code double}
     */
    @Override
    public double doubleValue() {
        return (double) numerator / denominator;
    }
}
