package _01hello;

/**
 * Dies ist die Dokumentation meiner Klasse...
 */
@SuppressWarnings("all")
public class HelloWorld {

    /**
     * Dies ist die Beschreibung meiner Methode. Es soll dem
     * Anwender meiner Funktion aufzeigen, wie diese verwendet
     * werden kann oder was diese macht.
     *
     * @param args Argumente, die beim Aufruf übergeben können
     */
    public static void main(String[] args) {
        System.out.println("Hello, World");
    }
}
