package _01hello;

import lombok.extern.log4j.Log4j2;


/**
 * Einfaches Beispiel, in welchem die Meldungen nicht (nur) auf der Konsole
 * ausgegeben werden, sondern in einem Klaren Format via Logging Framework.
 * <p>
 * Die Konfiguration hierzu ist in der Datei {@code resources/log4j2.properties}
 * zu finden.
 */
@Log4j2
public class Logging {

    public static void main(String[] args) {
        final String message = "Hello there!";

        log.trace(message);
        log.debug(message);
        log.info(message);
        log.warn(message);
        log.error(message);
        log.fatal(message);
    }
}
