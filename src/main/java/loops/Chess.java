package loops;

import static edu.stdlib.IOTools.*;

/**
 * Aufgabe 3.20 (s.102)
 */
public class Chess {

    public static void main(String[] args) {

        int n = readInt("Zahl eingeben: ");

        for (int row = 1; row <= n; row++) {
            for (int col = row; col < n + row; col++) {
                System.out.format("%3d", col);
            }
            System.out.println();
        }

    }
}
