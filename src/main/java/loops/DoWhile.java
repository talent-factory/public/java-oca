package loops;

import static edu.stdlib.IOTools.*;

import static java.lang.System.out;

public class DoWhile {

    public static void main(String[] args) {

        int value = readInt("Bitte eine ganze Zahl eingeben: ");

        /*
         * Ausgeben der Zahlen (eine Zahl pro Zeile) 0 .. value -1
         */

        int i = 0;

        // 1 .. n
        do {
            out.print(i++ + " ");
        } while (i < value);
        out.println("\nDone.");
    }

}
