package loops;

import static edu.stdlib.IOTools.*;

public class WhileLoop {

    public static void main(String[] args) {

        int value = readInt("Bitte eine ganze Zahl eingeben: ");

        /*
         * Ausgeben der Zahlen (eine Zahl pro Zeile) 0 .. value -1
         */

        int i = 0;

        // 0 .. n
        while (i < value) {
            System.out.print(i++ + " ");
        }
        System.out.println("\nDone.");
    }
}
