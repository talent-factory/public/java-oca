package loops;

import static edu.stdlib.IOTools.*;

public class ForLoop {

    public static void main(String[] args) {

        int value = readInt("Bitte eine ganze Zahl eingeben: ");

        /*
         * Ausgeben der Zahlen (eine Zahl pro Zeile) 0 .. value -1
         */
        for (int i = 0; i < value; i++) {
            System.out.print(i + "-");
        }

        System.out.println("\nDone.");
    }
}
