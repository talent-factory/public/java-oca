package classes;

import lombok.Data;

/**
 * Diese Klasse realisiert eine Adresse einer natürlichen Person
 */
@Data
public class Address {

    // Eigenschaften der Klasse 'Address'
    private String type;
    private String strasse;
    private int hausnummer;
    private int postleitzahl;
    private String wohnort;
}
