package classes;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

@ToString
public class Person {


    @Getter
    private String firstName;

    @Getter
    private String lastName;

    @Getter
    @Setter
    private Date birthDate;

    @Getter
    @Setter
    private Address home;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

}
