package classes;

public class TestMyClasses {

    public static void main(String[] args) {

        Address home = new Address();

        home.setType("private");
        home.setStrasse("Bernstrasse");
        home.setHausnummer(28);
        home.setPostleitzahl(3066);
        home.setWohnort("Stettlen");

        Person person = new Person("Markus", "Oswald");
        Person p2 = new Person("Markus", "Oswald");

        person.setHome(home);
        System.out.println(person);

        int x = 5;
        int y = 5;

        if (person.equals(p2)) {
            // ....
        }

    }
}
