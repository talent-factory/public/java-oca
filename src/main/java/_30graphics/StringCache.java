package _30graphics;

import lombok.Getter;

public class StringCache {

    @Getter
    private final String content;

    public StringCache(String content) {
        this.content = content;
    }
}
