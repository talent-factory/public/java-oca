package _30graphics.calculator;

import javax.swing.*;
import java.awt.*;

/**
 * Einfacher Taschenrechner, welcher dir Grundoperationen abdeckt.
 */
public class SimpleCalculator {

    // Beschriftung alle Knöpfe
    public static final String[][] LABELS = {
            {"7", "8", "9", "+"},
            {"4", "5", "6", "-"},
            {"1", "2", "3", "*"},
            {"0", ".", "/", "="}
    };

    // Verwendete Schriftart
    public static final Font FONT = new Font(Font.SANS_SERIF, Font.BOLD, 24);

    // Anzeigefeld des Rechners
    private JTextField display;

    /**
     * Starten des einfachen Rechners.
     *
     * @param args werden nicht verwendet
     */
    public static void main(String[] args) {
        SimpleCalculator calculator = new SimpleCalculator();
        SwingUtilities.invokeLater(calculator::createAndShowGui);
    }

    /**
     * Erstellen des GUI's mit all seinen Komponenten.
     */
    private void createAndShowGui() {
        display = new JTextField(10);
        display.setFont(FONT.deriveFont(Font.PLAIN));
        display.setHorizontalAlignment(SwingConstants.RIGHT);
        display.setText("0.0");
        JPanel buttonPanel = new JPanel(new GridLayout(LABELS.length, LABELS[0].length));

        for (String[] buttonText : LABELS) {
            for (String text : buttonText) {
                JButton button = new JButton(text);
                button.setFont(FONT);
                buttonPanel.add(button);
                button.addActionListener(event ->
                        updateDisplayField(event.getActionCommand()));
            }
        }

        JPanel mainPanel = new JPanel(new BorderLayout());
        mainPanel.add(display, BorderLayout.PAGE_START);
        mainPanel.add(buttonPanel, BorderLayout.CENTER);

        JFrame frame = new JFrame("SimpleCalculator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(mainPanel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * Aktualisieren der Anzeige.
     *
     * @param actionCommand Inhalt der gedrückten Taste
     */
    private void updateDisplayField(String actionCommand) {
        double value = Double.parseDouble(display.getText());
        char c = actionCommand.charAt(0);

        // TODO
        if (c >= '0' && c <= '9') { // Character.isDigit(c);
            value = value * 10 + (c - '0');
            display.setText("" + value);
        }
    }

}
