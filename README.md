## Beschreibung

Java ist die meistgenutzte Programmiersprache der Welt und gewinnt weiterhin 
Marktanteile. Die Popularität von Java garantiert zudem einen enormen Vorrat 
an unterstützendem Material und einen hohen Marktwert des erarbeiteten Wissens. 
Java-Kenntnisse sind auch ein idealer Einstieg in weitere populäre Sprachen. 

Nach erfolgreicher Beendigung können Sie die externe Prüfung als 
Oracle Certified Associate (OCA) ablegen und Ihren nächsten Karriereschritt 
tätigen.

### Aufbau der Ausbildung

- Arbeiten mit IntelliJ IDEA 
- Datentypen, Operatoren und Objekte
- Variablen, Typumwandlung, Arrays, Klassen Programmfluss und Fehlererkennung
- Modularisierung durch Klassen
- Kontrollstrukturen
- Exceptions Objektorientierte Programmierung
- Klassen, Eigenschaften und Methoden
- Klassenmethoden
- Innere Klassen
- Variablen- und Methodensichtbarkeit
- Vererbung
- Abstrakte Klassen und Schnittstellen Ein- und Ausgabe
- Streams
- Ausgaben auf den Bildschirm
- In Dateien schreiben
- Eingaben von der Tastatur
- Aus Dateien lesen Strings
- Umwandlung und Zerlegung Datum und Zeit Collections
- Listen und Mengen
- Methoden Lambda Ausdrücke Mit Java zeichnen Java Programme selbständig erstellen

---

### Installation

In diesem Moduel referenzieren wir ein Externes Projekt (IntroCS).

`$ git clone --recursive git@gitlab.com:talent-factory/public/java-oca.git`
